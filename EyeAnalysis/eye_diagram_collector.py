
import os
import sys
import time
IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
EYEPATH = IBERTPATH+"/EyeAnalysis/"
sys.path.append(IBERTPATH+'/PrivateModules/')
sys.path.append(IBERTPATH+'/Tools/')
sys.path.append(EYEPATH)
import eye_diagram_plotter as eye_plotter
from log_class import Log
from gt_mgt_conversion import MGTProps
from cmd_line_args_class import ArgGetter
from ibert_management_classes import Devices
from collections import OrderedDict
from yaml_functions import get_yaml

g_arg_tup_list = [
    ("-d", "--device"),
    ("-q", "--quad"),
    ("-c", "--channel"),
    ("-t", "--title"),
    ("-p", "--prescale"),
    ("-f", "--file"),
    ("-m", "--max_realign"),
]

g_arg_tup_list_bool = [
    ("-h", "--help"),
    ("-s", "--show"),
    ("--raw_only",),
]


# For input compatibility between python2 and python3
if sys.version_info[:2] <= (2, 7):
    safe_input = raw_input
else:
    safe_input = input

def setup_directories():
    """
    Create EyeAnalsysis and Results directories if they don't already exist.
    """
    for path_ext in ('', '/Results/'):
        if not os.path.exists(EYEPATH+path_ext):
            os.makedirs(EYEPATH+path_ext)

def setup_results_batch_dirs(batch_id):
    """
    Create Results subdirectories for this run, if they don't already exist. 
    """
    for path_ext in ('', '/Logs/', '/RawData/', '/ProcessedData/', '/EyeDiagrams/'):
        full_path = EYEPATH+"/Results/"+batch_id+path_ext
        if not os.path.exists(full_path):
            os.makedirs(full_path)

def get_timeout_value(prescale):
    """
    Get timeout value, in seconds, for the specified prescale value
    """
    return (prescale+1)*3


def show_usage():

    print("""
Usage: python eye_diagram_collector.py -d [device] -c [channel] (-q [quad])

                                            -h|--help
                                            -d|--device
                                            -q|--quad
                                            -c|--channel

Options:

 -h, --help                                  Display this message and exit
 -d, --device [name],                        Specify device
 -q, --quad [#]                              Specify quad number
 -c, --channel [#]                           Channel to collect eye diagram
                                             for. If quad number is provided,
                                             will be used as quad channel.
                                             Otherwise, will be used as GT
                                             channel.
 -f, --file  [file name]                     Identifier to use for output
                                             filenames 
 --raw_only                                  Only collect raw data.
 --show
 -p, --prescale [prescale value]
 -m, --max_realign [no of realignments]
    """)


def collect_eye_diagram(mgt_props, log=None, prescale=0, max_realign=40):
    """
    1 - set mgt_select, 0-79
    2 - set eye_go to 0
    3 - issue reset to eye_rstrtl
    4 - eye_data_rdy - check if 0. If it's not 0, redo step 2
    5 - set eye_go to 1
    6 - wait until eye_datardy is one
    7 - set eye_go to 0
    """
    return_data = {
        "Raw Eye Diagram Data" : "NA",
        "Realignments Performed" : "NA",
        "Total Time Taken" : "NA",
        "Maximum Realignments Reached" : "No"
    }

    initial_time = time.time()

    if log is None:
        log = Log()

    gt_id = mgt_props.gt_id
    gt_ch_int = mgt_props.gt_ch
    card = mgt_props.device

    devices = Devices()
    card_dev = devices[card]
    rx_mgt = devices[gt_id]

    log.write("-"*70 + "\n")
    log.write("Collection of eye diagram for rx: %s\n"%(mgt_props))
    log.write("-"*70 + "\n")
    log.flush()


    try:
        log.write("Beginning collection process.\n\n")
        log.flush(timestamped=True)
        set_prescale(rx_mgt, log, value=prescale)
        select_mgt_ch(card_dev, gt_ch_int, mgt_props.device_type, log)

        no_of_realignments = 0
        while no_of_realignments < max_realign:
            lower_eye_go(rx_mgt, log)
            issue_eye_reset(rx_mgt, log)
            raise_eye_go(rx_mgt, log)

            min_time = 1.5
            max_time = get_timeout_value(prescale)
            perform_realignment = False
            elapsed_time = wait_for_eye_data_ready(rx_mgt, log, timeout=max_time)

            if elapsed_time < min_time:
                log.write("Eye data ready in less than {:.2f} seconds.\n\n".format(float(min_time)))
                perform_realignment = True
            elif elapsed_time >= max_time:
                log.write("Timeout of {:.2f} seconds reached waiting for eye data ready.\n\n".format(float(max_time)))
                perform_realignment = True
            else:
                log.write("Eye data ready in {:.2f} seconds. Proceeding to obtain data from registers.\n\n".format(float(elapsed_time)))

            if perform_realignment and no_of_realignments < max_realign:
                log.write("Lowering eye_go, then performing realignment.\n")
                log.flush()
                lower_eye_go(rx_mgt, log)
                eye_realign(gt_id, log, mgt_type=mgt_props.mgt_type)
                log.write("Realignment finished. Reattempting collection process.\n\n")
                log.flush(timestamped=True)
                no_of_realignments += 1
            else:
                break

        eye_diagram_data = collect_eye_data(card_dev, log)

        total_time = time.time() - initial_time

        if no_of_realignments >= max_realign:
            log.write("WARNING: performed (%s) realignments without success, proceeding with bad data.\n"%(no_of_realignments))
        else:
            log.write("Finished collection process. Required %s realignments.\n"%(no_of_realignments))
        log.flush(timestamped=True)

        return_data["Raw Eye Diagram Data"] = eye_diagram_data
        return_data["Realignments Performed"] = no_of_realignments
        return_data["Total Time Taken"] = total_time
        return_data["Maximum Realignments Reached"] = (
            "Yes" if no_of_realignments >= max_realign else "No"
        )

    except RuntimeError as E:
        log.write("Due to runtime error (%s), eye diagram could not be collected.\n"%(repr(E)))
        log.flush(timestamped=True)

    return return_data


def set_prescale(rx_mgt, log, value=0x0):
    """
    Set prescale vale for receiver.
    """
    log.write("Setting prescale to %s\n"%(value))
    rx_mgt["eye_pre_scale"].write(value)
    log.write("Value after writing: %s\n"%(rx_mgt["eye_pre_scale"].read()))
    log.flush()


def select_mgt_ch(card_dev, gt_ch_int, dev_type, log):
    """
    Select MGT channel on the specified card.
    """
    # Step 1 - Select MGT Ch
    log.write("Setting mgtchselect to %s\n"%(gt_ch_int))
    log.flush()

    # Structure is different for RODs vs Hubs/HTMs
    if dev_type == "rod":
        card_dev["versions"]["mgt_select"]["mgtchselect"].write(gt_ch_int)
        log.write("Value after writing: %s\n"%(card_dev["versions"]["mgt_select"]["mgtchselect"].read()))
    else:
        card_dev["csr"]["mgt_select"]["mgtchselect"].write(gt_ch_int)
        log.write("Value after writing: %s\n"%(card_dev["csr"]["mgt_select"]["mgtchselect"].read()))
    log.flush()


def lower_eye_go(rx_mgt, log):
    """
    Lower eye_go bit on receiver
    """
    log.write("Lowering eye_go\n")
    log.flush()
    rx_mgt["eye_control"]["eye_go"].lower_bits()
    log.write("Value of eye_go after lowering: %s\n\n"%rx_mgt["eye_control"]["eye_go"].read())
    log.flush()


def raise_eye_go(rx_mgt, log):
    """
    Raise eye go bit on receiver
    """
    log.write("Raising eye_go.\n")
    log.flush()
    rx_mgt["eye_control"]["eye_go"].raise_bits()
    log.write(
        "Value of eye_go after raising: %s\n\n"%(
            rx_mgt["eye_control"]["eye_go"].read()
        )
    )
    log.flush()


def issue_eye_reset(rx_mgt, log, max_no_of_resets=5, wait_time=3):
    """
    Issue an eye reset to receiver, wait for eye_datardy bit to go low.

    Attempt reset multiple times if necessary.
    """
    log.write("Issuing eye diagram reset, waiting for eye_datardy low ...\n")
    log.flush()
    i = 0
    while i < max_no_of_resets:
        rx_mgt["eye_control"]["eye_rstrtl"].blip(wait_time=wait_time)
        log.flush()
        if not rx_mgt["eye_status"]["eye_datardy"].read():
            break
        i += 1

    if i >= max_no_of_resets: 
        raise RuntimeError("Eye_datardy not low after %s reset attempts.\n"%
                           max_no_of_resets)

    log.write("Eye_datardy is low after %s reset attempts.\n\n"%(i+1))
    log.flush()


def wait_for_eye_data_ready(rx_mgt, log, timeout=10.0):
    """
    Wait for eye_datardy bit to go high.

    Returns: Time elapsed waiting for eye data
    """
    start_time = time.time()
    elapsed_time = time.time() - start_time
    log.write("Waiting for eye_datardy high (timeout of {:.2f} seconds) ...\n".format(timeout))
    log.flush()
    while not rx_mgt["eye_status"]["eye_datardy"].read():
        time.sleep(1)
        elapsed_time = time.time() - start_time
        if elapsed_time >= timeout:
            break

    return elapsed_time


def eye_realign(gt_id, log, mgt_type=None, wait_time=5):
    """
    Perform eye realignment sequence. Wait for init_done to go high.

    Recently changed to "soft reset" method, but this is not suitable for all
    FW/Devices. Should revise to decide which method is used based on device
    (or we should have a uniform method across all in-use FW).
    """
    log.write("Performing realignment.\n")
    log.flush()

    devices = Devices()
    mgt = devices[gt_id]

    if mgt_type in "Ultrascale GTH" or "Ultrascale GTY":
        mgt["eye_control.eye_sofrs"].write(0x1)
        time.sleep(0.05)
        mgt["eye_control.eye_sofrs"].write(0x0)

    else:
        eye_ctrl = mgt["eye_control"]
        time.sleep(0.05)
        eye_ctrl["eye_drpaddrreg"].write(0x4f)
        time.sleep(0.05)
        eye_ctrl["eye_drpdireg"].write(0x8800)
        time.sleep(0.05)
        eye_ctrl["eye_drpwecmd"].write(0x1)
        time.sleep(0.05)
        eye_ctrl["eye_sofrs"].write(0x1)
        time.sleep(0.05)
        eye_ctrl["eye_drpwecmd"].write(0x0)
        time.sleep(0.05)
        eye_ctrl["eye_drpaddrreg"].write(0x4f)
        time.sleep(0.05)
        eye_ctrl["eye_drpdireg"].write(0x8000)
        time.sleep(0.05)
        eye_ctrl["eye_drpwecmd"].write(0x1)
        time.sleep(0.05)
        eye_ctrl["eye_sofrs"].write(0x0)
        time.sleep(0.05)
        eye_ctrl["eye_drpwecmd"].write(0x0)
        time.sleep(0.05)

    # Increased sleep time in hopes of waiting for init_done_high
    time.sleep(wait_time)
    if not mgt.get_init_done():
        log.write("Warning: init_done not high after waiting %s seconds following realignment (PMA reset)\n"%(wait_time))


def collect_eye_data(card_dev, log):
    """
    Collect raw eye diagram data from block registers.
    """
    log.write("Collecting eye diagram data from registers.\n")
    log.flush()

    # Since eye diagrams are stored in block memory, we have
    # to read them differently than a normal register
    eye_diagram1_uhal = card_dev["eye_diagram1"].get_uhal()
    eye_diagram2_uhal = card_dev["eye_diagram2"].get_uhal()
    eye_diagram3_uhal = card_dev["eye_diagram3"].get_uhal()

    eye_diagram1_list = eye_diagram1_uhal.readBlock(8192)
    eye_diagram2_list = eye_diagram2_uhal.readBlock(8192)
    eye_diagram3_list = eye_diagram3_uhal.readBlock(8192)

    card_dev.get_uhal().dispatch()

    eye_diagram_table_headers = (
        "Sample in Buffer",
        "Sample in Window",
        "TRIGGER",
        "state",
        "drpAddr",
        "samples",
        "errors",
        "horz",
        "vert",
        "go",
    )

    eye_diagram_data = []
    eye_diagram_data.append(eye_diagram_table_headers)

    i = 0
    while i < 8192:
        eye_diagram1_entry = bin(eye_diagram1_list[i])[2:].zfill(32)
        eye_diagram2_entry = bin(eye_diagram2_list[i])[2:].zfill(32)
        eye_diagram3_entry = bin(eye_diagram3_list[i])[2:].zfill(32)

        state    = int(eye_diagram1_entry[27:], 2)
        horzdata = int(eye_diagram1_entry[16:27], 2)
        vertdata = int(eye_diagram1_entry[5:16], 2)
        prescale = int(eye_diagram1_entry[:5], 2)

        samples  = int(eye_diagram2_entry[:16], 2)
        errors   = int(eye_diagram2_entry[16:], 2)

        drpaddr  = int(eye_diagram3_entry[22:], 2)
        unused   = int(eye_diagram3_entry[:22], 2)
        ## Old
        #sample   = int(eye_diagram1_entry[:16], 2)
        #error    = int(eye_diagram1_entry[16:], 2)
        #horzdata = int(eye_diagram2_entry[10:21], 2)
        #vertdata = int(eye_diagram2_entry[21:], 2)
        #state    = int(eye_diagram3_entry[17:22], 2)
        #drpaddr  = int(eye_diagram3_entry[22:], 2)
        eye_diagram_data.append(
            (hex(i        )[2:],
             hex(i        )[2:],
             hex(0        )[2:],
             hex(state    )[2:],
             hex(drpaddr  )[2:],
             hex(samples  )[2:],
             hex(errors   )[2:],
             hex(horzdata )[2:],
             hex(vertdata )[2:],
             hex(0        )[2:],
            )
        )

        i += 1

    return eye_diagram_data


def main(cmd_line_args=None):

    # Prepare dictionary of results to return
    eye_return_dict = OrderedDict(
        (
            ("Open Area", "NA"),
            ("Horizontal Opening", "NA"),
            ("Vertical Opening", "NA"),
            ("Errors Before", "NA"),
            ("Errors After", "NA"),
            ("Open Area Percentage", "NA"),
            ("Horizontal Opening Percentage", "NA"),
            ("Vertical Opening Percentage", "NA"),
            ("Raw Eye Diagram Data", "NA"),
            ("Realignments Performed", "NA"),
            ("Total Time Taken", "NA"),
            ("Maximum Realignments Reached", "NA"),
        )
    )

    setup_directories()

    # Handle command line arguments
    if cmd_line_args is None:
        cmd_line_args = ArgGetter(
            arg_tup_list = g_arg_tup_list,
            arg_tup_list_bool = g_arg_tup_list_bool,
        )

    if cmd_line_args["--help"]:
        show_usage()
        sys.exit()

    if cmd_line_args["--prescale"] is None:
        prescale = 0
    else:
        prescale = int(cmd_line_args["--prescale"])

    if cmd_line_args["--max_realign"] is None:
        max_realign = 40
    else:
        max_realign = int(cmd_line_args["--max_realign"])

    # Get Device/MGT info
    device = cmd_line_args["--device"]
    channel = cmd_line_args["--channel"]
    quad = cmd_line_args["--quad"]
    try:
        mgt_props = MGTProps(device=device, channel=channel, quad=quad)
    except ValueError:
        raise ValueError("Error while getting MGT properties. Double check that necessary and correct information was specified (may be missing channel, or invalid quad/channel specified)")

    # Handle results filenames
    cmd_line_filename = cmd_line_args["--file"]
    if cmd_line_filename is None:
        log = Log()
        raw_data_log = None
        proc_data_log = None
        eye_diagram_outfile = None
    else:
        file_id = "%s_%s_GTch%s_Q%sch%s"%(cmd_line_filename,
                                          mgt_props.device,
                                          mgt_props.gt_ch,
                                          mgt_props.quad,
                                          mgt_props.quad_channel)
        title = cmd_line_args["--title"]
        if title is not None:
            file_id += "_%s"%(title)
        setup_results_batch_dirs(cmd_line_filename)
        log = Log(EYEPATH+"/Results/%s/Logs/%s_log.txt"%(cmd_line_filename, file_id))
        raw_data_log = Log(EYEPATH+"/Results/%s/RawData/%s_raw.csv"%(cmd_line_filename, file_id))
        proc_data_log = Log(EYEPATH+"/Results/%s/ProcessedData/%s_processed.csv"%(cmd_line_filename, file_id))
        eye_diagram_outfile = EYEPATH+"/Results/%s/EyeDiagrams/%s_diagram.png"%(cmd_line_filename, file_id)

    # Get devices/MGT
    devices = Devices()
    rx_mgt = devices[mgt_props.gt_id]

    try: 
        # Handle raw eye diagram data collection, + errors before/after
        eye_return_dict["Errors Before"] = rx_mgt.get_error_count()
        eye_collection_data = collect_eye_diagram(mgt_props, log, prescale=prescale, max_realign=max_realign)
        eye_return_dict.update(eye_collection_data)
        raw_eye_data = eye_collection_data["Raw Eye Diagram Data"]
        eye_return_dict["Errors After"] = rx_mgt.get_error_count()

        # Log raw eye diagram data
        if raw_data_log is not None:
            for data_row in raw_eye_data:
                raw_data_log.write(",".join([str(data) for data in data_row]))
                raw_data_log.write("\n")
            raw_data_log.flush(print_log=False)

        # If collecting raw data only, nothing more to do
        if cmd_line_args["--raw_only"]:
            return eye_return_dict

        mgt_type = mgt_props.mgt_type
        mgt_quad = mgt_props.quad
        if (mgt_type == "7 Series GTH") and (mgt_quad == 110):
            mgt_type = "7 Series GTH - Q110"


        # Get processed eye data, get open areas
        proc_eye_data = eye_plotter.process_raw_eye_data(raw_eye_data, mgt_type=mgt_type)
        open_areas = eye_plotter.calculate_open_areas(proc_eye_data, mgt_type=mgt_type)
        eye_return_dict.update(open_areas)

        if proc_data_log is not None:
            for data_row in proc_eye_data:
                proc_data_log.write(",".join([str(data) for data in data_row]))
                proc_data_log.write("\n")
            proc_data_log.flush(print_log=False)

        device_config = get_yaml(IBERTPATH + '/device_config.yaml')
        device_connhub_id = device_config["device_connhub_mapping"][device]

        # If eye diagram not being produced, nothing more to do
        if not (cmd_line_args["--show"] or eye_diagram_outfile):
            return eye_return_dict

        # Get title for eye diagram
        full_title = "%s - %s"%(mgt_props.mgt_id, device_connhub_id)
        title = cmd_line_args["--title"]
        if title is not None:
            full_title += " - %s"%(title)

        # Make plot from processed data
        eye_plotter.output_full_plot(
            proc_eye_data,
            outfile=eye_diagram_outfile,
            display=cmd_line_args["--show"],
            title=full_title,
            mgt_type=mgt_type
        )

        log.write(
            "Open Area: %s (%s)\nHorizontal Opening: %s (%s)\nVertical Opening: %s (%s)\n"%(
                eye_return_dict["Open Area Percentage"],
                eye_return_dict["Open Area"],
                eye_return_dict["Horizontal Opening Percentage"],
                eye_return_dict["Horizontal Opening"],
                eye_return_dict["Vertical Opening Percentage"],
                eye_return_dict["Vertical Opening"],
            )
        )
        log.flush()

    except Exception as E:
        log.write("Exception occured: %s\nEye diagram collection may have failed\n"%(repr(E)))
        log.flush(timestamped=True)

    return eye_return_dict


if __name__ == "__main__":
    main()
