#!/usr/bin/env bash

USAGE="Usage: ./full_card_collection_test.sh -d [device] -s [start channel] -e [end channel] -f [file]\n"
USAGE+="\n"
USAGE+="-h|--help                   Display this message and exit\n"
USAGE+="-d|--device                 Device to use (hub1, htm3, etc)\n"
USAGE+="-s|--start                  Lower end of GT channels to test\n"
USAGE+="-e|--end                    Upper end of GT channels to test\n"
USAGE+="-f|--file                   Filename to use (not including extension)\n"

# Get command line arguments.
while [[ $# -gt 0 ]]
do
key="$1"

case $key in

    -d|--device)
    DEVICE="$2"
    shift
    shift
    ;;
    -s|--start)
    START="$2"
    shift
    shift
    ;;
    -e|--end)
    END="$2"
    shift
    shift
    ;;
    -t|--title)
    TITLE="$2"
    shift
    shift
    ;;
    -f|--file)
    FILENAME="$2"
    shift
    shift
    ;;
    -p|--prescale)
    PRESCALE="$2"
    shift
    shift
    ;;
    -h|--help)
    printf "$USAGE"
    exit 0
    ;;
    *)
    printf "Parameter $key not recognized.\n"
    printf "$usage"
    printf "Exiting ...\n"
    exit 1
    ;;
esac
done


# Check that necessary commmands are given
if [ -z "$START" ]
then
    printf "Parameter -s|--start not provided; Exiting ...\n"
    exit 1
fi
if [ -z "$END" ]
then
    echo "Parameter -e|--end not provided; Exiting ...\n"
    exit 1
fi
if [ -z "$FILENAME" ]
then
    echo "Parameter -f|--file not provided; Exiting ...\n"
    exit 1
fi
if [ -z "$DEVICE" ]
then
    echo "Parameter -d|--device not provided; Exiting ...\n"
    exit 1
fi
if [ -z "$PRESCALE" ]
then
    echo "Parameter -p|--prescale not provided; Exiting ...\n"
    exit 1
fi

## Get date in YYYYMMDD format
#now=$(date +'%Y%m%d')


for ((i=START;i<=END;i++)); do
    PYTHON_COMMAND="python eye_diagram_collector.py -d $DEVICE -c $i -f $FILENAME -p $PRESCALE"
    echo $PYTHON_COMMAND
    $PYTHON_COMMAND
    # Sleep for a short time, to make it easier to keyboard interrupt
    sleep 1
done

## ==> outputs 1 2 3 4 5 on separate lines
#
## Check that hub1SN is given
#if [ -z "$hub1SN" ]
#then
#    printf "Serial number of Hub1 not given. Required for all tests.\n"
#    printf "$usage"
#    printf "Exiting ...\n"
#    exit 1
#fi
#
## Get date in YYYYMMDD format
#now=$(date +'%Y%m%d')
#
#ping_1472byte_title="HubSN${hub1SN}x${hub2SN}_Ping_1472Byte_${now}.txt"
#
