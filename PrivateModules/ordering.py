
card_list = ["hub1", "rod1", "hub2", "rod2", "htm3", "htm4", "htm5", "htm6",
             "htm7", "htm8", "htm9", "htm10", "htm11", "htm12", "htm13",
             "htm14"]

mgt_order_offset = 10000
gt_order_offset = 0

def order_key_by_gt_ch(card, gt_ch):
    return card_list.index(card)*80 + int(gt_ch) + gt_order_offset

def order_key_by_mgt_quad(card, mgt_quad, quad_channel):
    return (card_list.index(card)*300 + int(mgt_quad) + int(quad_channel)
            + mgt_order_offset)

def hub_gt_ch_to_mgt_quad(gt_channel):

    gt_channel = int(gt_channel)
    if 0 <= gt_channel <= 39:
        mgt_quad_offset = 224
        gt_ch_offset = 0
    elif 40 <= gt_channel <= 79:
        mgt_quad_offset = 124
        gt_ch_offset = 40
    else:
        raise ValueError("GT channel value is %s, should be in range 0-79"%(gt_channel))

    mgt_quad = mgt_quad_offset + (gt_channel - gt_ch_offset)//4
    quad_channel = (gt_channel - gt_ch_offset)%4
    return mgt_quad, quad_channel

def hub_mgt_quad_to_gt_ch(mgt_quad, quad_channel):

    if 224 <= mgt_quad <= 233:
        mgt_quad_offset = 224
        gt_ch_offset = 0
    elif 124 <= mgt_quad <= 133:
        mgt_quad_offset = 124
        gt_ch_offset = 40
    else:
        raise ValueError("MGT quad value is %s, should be in range 124-133 or 224-233"%(mgt_quad))

    if not (0 <= quad_channel <= 3):
        raise ValueError("Quad channel value is %s, should be in range 0-3"%(quad_channel))

    mgt_quad = int(mgt_quad)
    quad_channel = int(quad_channel)

    return (mgt_quad-mgt_quad_offset)*4 + quad_channel + gt_ch_offset

