#!/usr/bin/env python
"""
Module defining LinkConfig and GTConfig classes, which are used to store/access
information about the routing of transceivers (as defined by the files in
<IBERTPATH>/LinkConfiguration/MGTDefinitions).

LinkConfig presents the routing info in the context of the links. I.e. given a
link, get the source and destination transceivers.

GTConfig presents the routing info in the context of the transceivers. I.e.
given a transceiver, get which links the transceiver transmits and/or receives
for.
"""
# Standard Library Modules
import sys
import os
from collections import OrderedDict
# For tab-autocomplete when running interactively
import rlcompleter
import readline
readline.parse_and_bind("tab: complete")

IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
LINKCONFIGPATH = IBERTPATH + '/LinkConfiguration/'
MGTDEFPATH = IBERTPATH + '/LinkConfiguration/MGTDefinitions/'
sys.path.append(IBERTPATH+'/PrivateModules/')

# Original Modules
from yaml_functions import get_yaml, is_yaml_none
from gt_mgt_conversion import MGTProps

class LinkConfig:
    """
    Class for storing/accessing link configuration information. Information is
    indexed by link name. The class GTConfig builds itself using an instance of
    this class.
    """

    def __init__(self, used_links_only=False, used_devs_only=False, config=None):
        """
        Arguments:
            in_use_only - Whether all defined links should be used, or only
                those specified in the link_list of a configuration file. If
                true, must provide config
            config - config to use if limiting to in-use links or devs only
        """
        if config is None:
            config = "/".join([IBERTPATH, 'PrivateModules', "template.yaml"])

        self.config = get_yaml(config)
        self.link_dict = self.get_link_dict()
        self.in_use_links = list(self.link_dict.keys())

        # Shorten link dictionary to only include used links
        if used_links_only:
            config = get_yaml(self.config)
            for link in config["link_list"]:
                if link not in self.link_dict:
                    raise KeyError("Invalid link name '%s'"%(link))
            self.in_use_links = config["link_list"]

        if used_devs_only:
            self.in_use_links = [link for link in self if
                                 self._link_dev_exists(link)]


    def _link_dev_exists(self, link):

        source_dict = self[link]["source"]
        if not is_yaml_none(source_dict):
            card = source_dict["card"]
            if is_yaml_none(self.config["device_connhub_mapping"][card]):
                return False

        dest_dict = self[link]["dest"]
        if not is_yaml_none(dest_dict):
            card = dest_dict["card"]
            if is_yaml_none(self.config["device_connhub_mapping"][card]):
                return False
        return True


    def __len__(self):
        return len(self.link_dict)


    def __getitem__(self, key):
        return self.link_dict[key]


    def __contains__(self, key):
        return key in self.in_use_links


    def __iter__(self):
        """
        Iterate over keys of link_dict. link_dict is ordered according to the
        file IBERTPATH/LinkConfiguration/link_ordering.yaml, but iteration
        occurs according to order of in_use_links.

        Thus, if a link list is provided by the user (i.e. in_use_only = True),
        then links will be iterated over in the order of the user's link list.
        """
        for link_name in self.in_use_links:
            yield link_name


    def get_link_dict(self):
        """
        Build the dictionary that forms the foundation of this class.
        """
        # Get all yaml files which define mgt links
        mgt_link_def_yaml_str = ""
        for fname in os.listdir(MGTDEFPATH):
            if '.yaml' in fname:
                with open(MGTDEFPATH + fname) as stream:
                    mgt_link_def_yaml_str += '\n' + stream.read() + '\n'

        link_dict = OrderedDict(get_yaml(mgt_link_def_yaml_str))

        # Reorder link dict according file 
        links_ordered = get_yaml(LINKCONFIGPATH+'link_ordering.yaml')
        for link in links_ordered:
            if link in link_dict:
                link_dict[link] = link_dict.pop(link)

        for link in list(link_dict.keys()):
            if link not in links_ordered:
                link_dict[link] = link_dict.pop(link)

        return link_dict


class GTConfig:
    """
    Class for accessing MGT attributes, indexed by card and GT channel.
    """
    def __init__(self, link_config):

        self.gt_dict = self.get_gt_dict(link_config)
        self.link_config = link_config

        self.length = 0
        for i in self:
            self.length += 1

        self.rx_length = 0
        self.tx_length = 0
        for gt in self:
            if self[gt]["tx_links"]:
                self.tx_length += 1
            if self[gt]["rx_links"]:
                self.rx_length += 1


    def __getitem__(self, key):

        if isinstance(key, tuple) or isinstance(key, list):
            # Convert tuples and lists to '.' separated key
            key = ".".join(key)

        # For a '.' separated key, traverse node tree
        if '.' in key:
            key_split = key.split('.')
            value = self
            for subkey in key_split:
                value = value[subkey]
            return value

        return self.gt_dict[key]


    def __iter__(self):
        """
        Yield 3 layers deep
        e.g. 'hub1.GT.ch1'
        """
        for key1 in self.gt_dict:
            for key2 in self.gt_dict[key1]:
                for key3 in self.gt_dict[key1][key2]:
                    yield ".".join((key1, key2, key3))


    def __len__(self):
        return self.length


    def get_gt_dict(self, link_config):
        """
        Build the dictionary that forms the foundation of this class.
        """

        gt_dict = OrderedDict()
        end_types = ("source", "dest")
        for link_name in link_config:
            for end_type in end_types:
                end_dict = link_config[link_name][end_type]
                if not is_yaml_none(end_dict):
                    card_name = end_dict["card"]
                    channel = end_dict["channel"].split(".")[-1]
                    mgt_props = MGTProps(device=card_name, channel=channel)
                    gt_id = mgt_props.gt_id
                    mgt_type = mgt_props.mgt_type

                    if card_name not in gt_dict:
                        gt_dict[card_name] = OrderedDict([("GT", OrderedDict())])
                    if channel not in gt_dict[card_name]["GT"]:
                        gt_dict[card_name]["GT"][channel] = OrderedDict(
                            (
                                ("tx_links", []),
                                ("rx_links", []),
                            )
                        )

                    gt_info_dict = gt_dict[card_name]["GT"][channel]
                    gt_info_dict["type"] = mgt_type
                    if end_type == "source":
                        gt_info_dict["tx_links"].append(link_name)
                    if end_type == "dest":
                        gt_info_dict["rx_links"].append(link_name)

        return gt_dict


    def get_tx_gts(self):
        for gt in self:
            if self[gt]["tx_links"]:
                yield gt


    def get_rx_gts(self):
        for gt in self:
            if self[gt]["rx_links"]:
                yield gt


    def get_no_of_rx_gts(self):
        return self.rx_length


    def get_no_of_tx_gts(self):
        return self.tx_length


    def _link_to_gt_generic(self, link_name, source_or_dest):
        """
        From a link name, get the corresponding card and gt channel at the
        source or destination of the link.
        """
        end_dict = self.link_config[link_name][source_or_dest]
        if is_yaml_none(end_dict):
            return None
        card_name = end_dict["card"]
        channel = end_dict["channel"]
        return ".".join((card_name, channel))


    def link_to_tx_gt(self, link_name):
        """
        From a link name, get the corresponding card and gt channel of the
        source/transmitter of the link.
        """
        return self._link_to_gt_generic(link_name, "source")


    def link_to_rx_gt(self, link_name):
        """
        From a link name, get the corresponding card and gt channel of the
        destination/receiver of the link.
        """
        return self._link_to_gt_generic(link_name, "dest")
