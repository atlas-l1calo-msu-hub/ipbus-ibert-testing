"""
Module for managing command line arguments provided to scripts.

It turns out that the argparse module, which is in the python standard library,
provides similar funcitonality, and more. For example, argparse contains the
ability to associate a help message with each argument, which can be used to
automatically produce a usage message for when '--help' argument is provided.

When time permits, the usage of this module throughout this project should be
replaced by the argparse module.
"""

import sys
import warnings

class ArgValue:
    """
    Class for handling command line argument values. Used in conjunction with
    ArgGetter class.
    """
    def __init__(self, boolean_arg=False):
        self.boolean_arg = boolean_arg
        if boolean_arg:
            self.value = False
        else:
            self.value = None


    def __str__(self):
        return str(self.value)

    def set_value(self, value=None):
        if self.boolean_arg:
            self.value = True
        else:
            self.value = value

    def get_value(self):
        return self.value

    def is_boolean(self):
        return self.boolean_arg


class ArgGetter:
    """
    Class for managing command line arguments.
    """

    def __init__(self, arg_tup_list=[], arg_tup_list_bool=[]):
        """
        Initializer for ArgGetter class.

        Args:
            arg_tup_list: List containing tuples of strings which are potential
                command line arguments (e.g. ("-l", "--log_name")), and which
                expect another argument following.

            arg_tup_list_bool: List containing tuples of strings which are
                potential command line arguments (e.g. ("-h", "--help")), and
                which do not expect another argument following (i.e. a boolean
                flag).

        Returns:
            None
        """
        self.arg_value_dict = {}
        for arg_tup in arg_tup_list:
            self.__add_param(arg_tup)
        for arg_tup in arg_tup_list_bool:
            self.__add_param(arg_tup, boolean_arg=True)

        self.get_args()

        return


    def __getitem__(self, key):
        """
        Item getter for ArgGetter class.

        Args:
            key: A command line argument, as specifed during object
                initialization.

        Returns:
            Value associated with command line argument.
        """

        return self.arg_value_dict[key].get_value()


    def __add_param(self, arg_name_tup, boolean_arg=False):
        """
        Args:
            arg_name_tup: A tuple/list containing string argument names
                specifying the same value (e.g ('-l', '--log'))
            boolean_arg: A boolean specifying whether the argument is boolean
                (e.g., a flag like --help or --verbose)
        Returns:
            None
        Raises:
            KeyError: When adding an arg name that has already been added
        """
        mutable_value = ArgValue(boolean_arg=boolean_arg)
        for arg_name in arg_name_tup:
            if arg_name not in self.arg_value_dict:
                self.arg_value_dict[arg_name] = mutable_value
            else:
                raise KeyError("Command line argument '%s' has already been used."%(arg_name))
        return


    def get_args(self):
        """
        Args:
            None
        Returns:
            None
        Raises:
            ValueError: When argument not recognized, argument provided more
                than once, or value following an argument expecting a value is
                itself another argument option.
            Exception: When value expected following an argument but none
                found.
        """

        cmd_line_args = sys.argv[1:]
        if len(cmd_line_args) >= 1: 
            i = 0
            while i < len(cmd_line_args):
                arg = cmd_line_args[i]
                if arg not in self.arg_value_dict:
                    raise ValueError("Argument %s not recognized."%arg)
                    i += 1
                    continue
                elif self[arg]:
                    raise ValueError("Argument %s provided more than once."
                                     %arg)

                if self.arg_value_dict[arg].is_boolean():
                    self.arg_value_dict[arg].set_value()
                else:
                    try:
                        i += 1
                        value = cmd_line_args[i]
                        self.arg_value_dict[arg].set_value(value)
                        if value in self.arg_value_dict:
                            raise ValueError("Value %s following %s is a command line option. You may be missing an argument!"%(value, arg))
                    except IndexError:
                        raise Exception("Expected value following %s but none found."%(arg))
                        self.arg_value_dict[arg].set_value()
                i += 1

        return
