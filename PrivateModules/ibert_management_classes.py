#!/usr/bin/env python
"""
Module defining the Devices and Manager classes.

The Devices class is used manage the various IPBus-accessible devices in the
shelf according to function and hardware (e.g., address the hub in slot 1 as
'hub1', regardless of which physical card is present) and perform IPBus
transactions.

The Manager class is used to collect all information needed to run an IPBus
IBERT test (user config, device config, link config) in one object.

As these classes both require the uhal module, they should stay defined in
their own module (or at least not be defined in a module alongside
classes/fucntions which do not require uhal), to prevent compatibility issues
when uhal is not present. For example, the PDF summar generation process does
not use uhal, and may need to be ran on machines which do not have uhal.

When ran interactively, this module creates the variables 'devices' and
'manager', instances of the Devices and Manager classes. These objects may be
used to perform operations on the various devices and MGTs in the shelf by hand.
"""
# Standard Library Modules
import sys
import os
from collections import OrderedDict
import copy
# For tab-autocomplete when running interactively
import rlcompleter
import readline
readline.parse_and_bind("tab: complete")

# Non-Standard Modules Not Part of Project
import uhal

IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
sys.path.append(IBERTPATH+'/PrivateModules/')

# Original Modules
from log_class import Log
import bit_operations_class
from gt_mgt_conversion import gt_to_mgt, MGTProps
from gt_mgt_conversion import MGTProps
from yaml_functions import get_yaml, is_yaml_none
from link_and_gt_conf_classes import LinkConfig, GTConfig


class Devices:
    """
    Class for managing IPBus-accessible devices. Main usage is indexing like a
    dictionary to get hw/ipbus Nodes (e.g. devices_obj["hub1"]).

    Valid keys correspond to the different logical devices in the 14SS/6SS
    ("hub1/2", "htm3-14", "rod1/2")
    """
    def __init__(self, config=None):

        # Default to top-level device config
        if config is None:
            config = IBERTPATH + "/device_config.yaml"

        conf = get_yaml(config)
        self.dev_dict = self.get_dev_dict(conf["device_connhub_mapping"])

        dev_ord = ["hub1", "hub2", "rod1", "rod2",
                   "htm3", "htm4", "htm5", "htm6", "htm7", "htm8",
                   "htm9", "htm10", "htm11", "htm12", "htm13", "htm14"]
        ord_func = lambda x : dev_ord.index(x) if x in dev_ord else -1

        self.full_dev_list = list(self.dev_dict.keys())
        self.full_dev_list.sort(key = ord_func)
        self.used_devs = self.get_used_devs(self.full_dev_list, self.dev_dict)
        self.length = len(self.used_devs)

    def __getitem__(self, key):
        if isinstance(key, tuple) or isinstance(key, list):
            # Convert tuples and lists to '.' separated key
            key = ".".join(key)

        if '.' in key:
            key_split = key.split('.')
            node = self
            for subkey in key_split:
                node = node[subkey]
            return node

        return self.dev_dict[key]


    def __contains__(self, item):
        return item in self.used_devs


    def __iter__(self):
        for dev_name in self.used_devs:
            yield dev_name

    def __len__(self):
        return self.length

    def get_dev_dict(self, device_connhub_mapping):
        """
        Gets dictionary mapping card names (hub1, hub2, etc) to HWNode objects.

        Arguments:
            device_connhub_mapping - dictionary where keys are card names, and
                items are the ConnHub id's corresponding to each card.

        Return:
            Dictionary where keys are card names, and items are HWNode objects
            corresponding to each card.
        """

        connhub_file_path = IBERTPATH + "/Xml/ConnHub.xml"
        manager = uhal.ConnectionManager("file://" + connhub_file_path)
        device_dict = {}

        for device_name, device_id in device_connhub_mapping.items():
            if not is_yaml_none(device_id):
                device_dict[device_name] = bit_operations_class.HWNode(
                    manager.getDevice(device_id)
                )
            else:
                device_dict[device_name] = None

        return device_dict

    def get_used_devs(self, full_dev_list, dev_dict):
        """
        Gets list of devices which are in-use.

        Arguments:
            full_dev_list - list of names of devices which are potentially in
                use.
            dev_dict - dictionary mapping from device names to HWNode objects.

        Return:
            list of devs which are in use.
        """

        used_devs = []
        for dev in full_dev_list:
            if not is_yaml_none(dev_dict[dev]):
                used_devs.append(dev)

        return used_devs


class Manager:
    """
    Class for creating/getting/storing all relevant information for
    reading/writing deivces/MGTs, determining link configuration, and logging
    information.
    """

    def __init__(self,
                 user_conf=None,
                 used_links_only=False,
                 used_devs_only=False,
                 dev_conf=None,
                 log=None,
                 verbosity=0):

        if user_conf is None:
            user_conf = IBERTPATH + '/PrivateModules/template.yaml'
        if dev_conf is None:
            dev_conf = IBERTPATH + '/device_config.yaml'

        self.conf = get_yaml(user_conf)
        self.conf.update(get_yaml(dev_conf))

        self.link_conf = LinkConfig(
            used_links_only=used_links_only,
            used_devs_only=used_devs_only,
            config = self.conf,
        )

        self.gt_conf = GTConfig(self.link_conf)
        self.devices = Devices(self.conf)

        if log is None:
            self.log = Log(verbosity=verbosity)
        elif type(log) == str:
            self.log = Log(log_name=log, verbosity = verbosity)
        elif isinstance(log, Log):
            self.log = log
        else:
            raise TypeError("Invalid type '%s' for 'log' parameter."%(type(log)))


if __name__ == "__main__":

    manager = Manager()
    devices = Devices()

