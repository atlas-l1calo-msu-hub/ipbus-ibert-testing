"""
Module defining various 'Node'-classes, which are used to perform IPBus
transactions with devices in a more python-like syntax than is present in the
nodes of the uhal module (e.g. using node['X']['Y'] instead of
node.getNode('X').getNode('Y')), and generally make them easier to manage (e.g.
including an automatic dispatch after reads/writes, methods for toggling
registers high/row).

Also defines the MGTNode class, which contains various methods to aid in the
management of MGTs specifically (e.g. methods used as shorthand for
reading/writing various registers and bitfields).
"""
import uhal
uhal.setLogLevelTo(uhal.LogLevel.WARNING)
import time
import warnings
import sys
import gt_mgt_conversion as gmc
    

class GenericNode:
    def __init__(self, uhal_obj, parent=None):
        """
        uhal_device : a uhal device object 
                      (i.e. uhal.ConnectionManager('filename').getDevice('devicename')
        """
        self.uhal_obj = uhal_obj
        self.parent = parent
        self._set_permission()
        self.child_node_dict = {}
        for node_id in self.get_child_nodes():
            self._add_node(node_id)


    def __getitem__(self, key):
        """
        Return : Node object of device with given name (key). Can traverse
            multiple layers of node-tree by suppling a list or tuple of keys,
            or a string of keys seperated by '.'
        """
        if isinstance(key, tuple) or isinstance(key, list):
            # Convert tuples and lists to '.' separated key
            key = ".".join(key)

        # For a '.' separated key, traverse node tree
        if '.' in key:
            key_split = key.split('.')
            node = self
            for subkey in key_split:
                node = node[subkey]
            return node

        elif key not in self.child_node_dict:
            raise KeyError("Uhal Node '%s' does not have child node '%s'."%(self, key))

        return self.child_node_dict[key]


    def __iter__(self):
        """
        Yield : iterable over nodes of Device
        """
        child_node_list = list(self.child_node_dict.values())
        # Should sort by address, if available, then by mask, if available,
        # then random, if neither available
        #child_node_list.sort(key = lambda node: node.get_uhal().getAddress())
        for child_node in child_node_list:
            yield child_node


    def __str__(self):
        """
        Return : Name of device
        """
        if self.parent:
            return str(self.parent) + "." + str(self.get_uhal())
        else:
            return str(self.get_uhal())

    def _set_permission(self):
        """
        """
        self.readable = True
        self.writable = True

    def _add_node(self, node_id):
        """
        Adds group with given name to dictionary of groups within device
        group_name : name of group (should match the id of a 'first-layer'
                                    node in XML file)
        """
        node_uhal = self.get_uhal().getNode(node_id)
        if "MGT" in node_uhal.getTags().split(','):
            self.child_node_dict[node_id] = MGTNode(node_uhal, parent=self)
        else:
            self.child_node_dict[node_id] = ChildNode(node_uhal, parent=self)
        return

        
    def get_child_nodes(self):
        """
        Return : list of id's of direct subnodes of the node corresponding to
                 device object (direct meaning one layer deep; i.e. not 
                 including any subnodes of subnodes)
        """
        child_node_list = []
        for node in self.get_uhal().getNodes():
            # filter out nested subnodes
            if '.' not in node:
                child_node_list.append(node)

        return child_node_list


    def get_uhal(self):
        """
        Return : uhal device associated with this object
        """
        return self.uhal_obj


    def dispatch(self):
        """
        Travel up node tree until HWinterface reached, then issue dispatch
        """
        current = self
        parent = self.parent
        while parent is not None:
            current = parent
            parent = parent.parent
        current.get_uhal().dispatch()
        return


class HWNode(GenericNode):
    """
    Node corresponding to a card/device.
    """
    def __init__(self, uhal_obj, parent=None):
        GenericNode.__init__(self, uhal_obj, parent)


class ChildNode(GenericNode):
    """
    Child node of a HWNode, or another ChildNode. 
    """
    def __init__(self, uhal_obj, parent):
        GenericNode.__init__(self, uhal_obj, parent)
        self._set_permission()


    def _set_permission(self):
        """
        Set read/write permission of Node. Read/write permission of this node
        (based on xml description) and of parent node are taken into
        consideration.
        """
        uhal_permission = self.get_uhal().getPermission()
        if uhal_permission == uhal._core.NodePermission.READWRITE:
            self.readable = True
            self.writable = True
        elif uhal_permission == uhal._core.NodePermission.READ:
            self.readable = True
            self.writable = False
        elif uhal_permission == uhal._core.NodePermission.WRITE:
            self.readable = False
            self.writable = True
        else:
            self.readable = False
            self.writable = False

        # If permission is not explicitly stated for a node, it is assumed to
        # be read/write. By taking logical and of r/w status with that of the
        # parent node, we preserve the r/w status of a register even when the
        # r/w status is not explicitly given for the bitfield.
        if self.parent is not None:
            self.readable = self.readable and self.parent.readable
            self.writable = self.writable and self.parent.writable


    def read(self):
        """
        Read value of register/bitfield.
        """
        if not self.readable:
            raise PermissionError("Cannot read '%s', does not have read permission."%self)

        try:
            value_read = self.get_uhal().read()
            self.dispatch()
            return value_read.value()

        except:
            raise UhalReadError("Error while reading from '%s'."%(self))


    def write(self, value, debug=False):
        """
        Write value to register.
        """
        if not self.writable:
            raise PermissionError("Cannot write to '%s', does not have write permission."%self)

        if debug:
            print("Writing value 0x%x to %s"%(value, self))

        try:
            self.get_uhal().write(value)
            self.dispatch()

        except Exception as e:
            raise UhalWriteError("Error while writing to '%s':"%(self) + str(e))
        
        if self.readable:
            read_value = self.read()
            if value != read_value:
                warnings.warn("Wrote value '%s' to '%s', but read value '%s' immediately afterward."%
                              (value, self, read_value)
            )

    
    def raise_bits(self):
        mask = self.get_uhal().getMask()
        # If mask is 0 (not be sensible to begin with), nothing to do
        if mask != 0:
            # Shift mask to the right until the rightmost bit is nonzero
            while mask % 2 == 0:
                mask = mask >> 1
        self.write(mask)


    def lower_bits(self):
        self.write(0)


    def blip(self, wait_time = 0.5):
        self.lower_bits()
        time.sleep(wait_time)
        self.raise_bits()
        time.sleep(wait_time)
        self.lower_bits()
        time.sleep(wait_time)


class MGTNode(ChildNode):
    """
    Special Node, representing an MGT.

    Contains useful methods for managing IBERT functionality of MGT.
    """

    def reset(self, wait_time = 0.5):
        return self["common_control.reset_master"].blip(wait_time = wait_time)


    def set_txprbssel(self, value):
        return self["tx_setup.txprbssel"].write(value)


    def get_txprbssel(self):
        return self["tx_setup.txprbssel"].read()


    def set_rxprbssel(self, value):
        return self["rx_setup.rxprbssel"].write(value)

    
    def get_rxprbssel(self):
        return self["rx_setup.rxprbssel"].read()


    def get_init_done(self):
        return self["common_status.init_done"].read()


    def inject_error(self, wait_time = 0.5):
        return self["tx_force_err.txprbsforceerr"].blip(wait_time = wait_time)


    def reset_errors(self, wait_time = 0.5):
        return self["rx_reset.rxprbscntreset"].blip(wait_time = wait_time)


    def get_rxprbslocked(self):
        return self["rx_status.rxprbslocked"].read()


    def get_error_flag(self):
        return self["rx_status.rxprbserr_flg"].read()


    def get_error_count(self):
        return self["rx_count_err"].read()


class UhalReadError(Exception):
    pass


class UhalWriteError(Exception):
    pass


class PermissionError(Exception):
    pass
