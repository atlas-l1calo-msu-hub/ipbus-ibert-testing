"""
Module defining all IPBus IBERT 'Actions', which form the backbone of an IPBus
IBERT test. The user config file for an IPBus IBERT test specifies several
actions to be performed (e.g. initialize links, check errors, etc.), which are
defined here as classes.

The mapping from the string used to identify an action in a user config file
and the actual class representing that action is defined by the global
dictionary variable 'g_action_map' at the end of the module.
"""

# Standard Library Modules
import sys
import os
from collections import OrderedDict
import copy
import time

IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
LINKCONFIGPATH = IBERTPATH + '/LinkConfiguration/'
MGTDEFPATH = IBERTPATH + '/LinkConfiguration/MGTDefinitions/'
sys.path.append(IBERTPATH+'/PrivateModules/')
sys.path.append(IBERTPATH+'/EyeAnalysis/')

# Original Modules
import i2c_monitor
import eye_diagram_collector as eye_collector
from gt_mgt_conversion import gt_to_mgt, MGTProps
from yaml_functions import is_yaml_none
from ibert_management_classes import Manager
from ibert_helper_functions import (get_bad_mgts, format_to_width,
    blip_register_group, get_all_gt_values, decode, decode_range, make_table)


class Action:
    """
    Base Class for other IPBus IBERT Action classes to inherit from.

    Acts as a template, defining methods all IBERT Actions should override.
    Also defines some functional methods, including logging of results as csv,
    converting results to a dictionary, etc.

    Attributes:
        results: A NxN array (implemented as double-list or double-tup) first
            row should be headers. First column should specify MGT/link/card,
            other columns describe a property of that MGT/link/card (some
            exceptions). MGTs should be used as first column whenever possible.
        manager: Manager object based on current shelf/user config and log.
        conf: dictionary containing user config and device config info
        gt_conf: GTConfig object based on current shelf routing config
        link_conf: LinkConfig object based on current shelf routing config
        devices: Devices object based on current cards used in shelf
        log: Log object to write to
        display_banner: whether to display a banner for the action (typically
            only true if ran as a standalone action)
        banner_name: name of action (to be used in banner)
        display_progress: whether to log message displaying action progress
            (typically only true if ran as a standalone action)
        verbosity: verbosity to use in log

    """
    banner_name = "Generic Action"
    def __init__(self, manager, display_banner=False, display_progress=False,
                 verbosity=None, quiet=False):
        """
        Args:
            manager: Manager object based on current shelf/user config and log.
            display_banner: whether to display a banner for the action
                (typically only true if ran as a standalone action)
            display_progress: whether to log message displaying action progress
                (typically only true if ran as a standalone action)
            verbosity: verbosity to use in log
            quiet: whether to log any output (overrides verbosity if true)
        """

        self.results = []
        self.gt_conf = manager.gt_conf
        self.link_conf = manager.link_conf
        self.devices = manager.devices
        self.conf = manager.conf
        # Copy log so changing verbosity does not affect output outside action
        self.log = copy.copy(manager.log)
        self.manager = manager
        self.display_progress = display_progress

        if verbosity is not None:
            self.log.verbosity = verbosity

        if quiet:
            self.log.verbosity = -1

        if display_banner:
            self._display_banner()

        self._perform_action()


    def _perform_action(self):
        """
        Perform the functionality associated with the action.
        
        Alse store results, if necessary). Meant to be overridden by
        subclasses.
        """
        pass


    def get_results(self):
        """
        Get results of action.

        Results should, with few exceptions, be stored as an NxN array
        (implemented as a double-list or double-tup).
        """
        return self.results


    def get_results_dict(self):
        """
        Get action results as a dictionary.

        where the keys are the entries of  the first column (link/MGT/device),
        and the items are the remainder of the corresponding row (as a list).
        """
        return dict([(x[0], x[1:]) for x in self.results])


    def get_results_dict_dual_layered(self):
        """
        Get results as a dual-dictionary

        The first layer of keys are the entries of the first column
        (link/MGT/device), and the second layer of keys are the entries of the
        first row (header entries). Effectively, a link/MGT/device and header
        entry specify a row and column in the results.
        """
        results_dict = OrderedDict()
        header = self.results[0]
        for row in self.results[1:]:
            gt_id = row[0]
            results_dict[gt_id] = {}
            gt_dict = results_dict[gt_id]
            for i, value in enumerate(row):
                gt_dict[header[i]] = value

        return results_dict


    def display_results(self, verbosity=None):
        """
        Display/log results in a meaningful and interpreted way.

        Will be overwritten by subclasses.

        Args:
            verbosity: verbosity to use in results (effect will vary between
                actions).
        """
        pass


    def get_results_csv(self):
        """
        Get action results in string-csv format.

        Returns:
            (str) results formatted as a csv
        """
        csv = ""
        for row in self.get_results():
            csv += ",".join([str(entry) for entry in row])
            csv += "\n"

        return csv


    def _display_banner(self):
        """
        Display/log a banner, signifying that the action is taking place.
        """

        try:
            title = self.banner_name
        except AttributeError:
            title = "Unknown Action"

        width = 80
        self.log.write(("="*width) + "\n")
        self.log.write("|{:^{width}}|\n".format(title, width=width-2))
        self.log.write(("="*width) + "\n")
        self.log.flush()


    def _display_bit_high_or_low(self, header_key, check_bit_high=True,
                                 ultrascale=False, series_7=False, included_mgts=None,
                                 mgt_text="MGTs"):
        """
        Check whether a bit is high or low in Action results for each MGT

        Results must have MGT IDs as the first column (aside from header) for
        this method to be used. Primarily meant to be used in status check
        actions (common, tx, and rx).

        Results will be written to the log (e.g. "WARNING - X bit is low on
        hub1.Q125.ch0")

        Args:
            header_key (str): what parameter to check for (should be a header
                entry from first row of results)
            check_bit_high (bool): True checks bit high, False checks bit low
            ultrascale (bool): whether to check bit for ultrascale MGTs
            series_7 (bool): whether to check bit for 7-series MGTs
            included_mgts (list): list of MGTs to include in check 
            mgt_text (str): string identifying which MGTs were checked (e.g.
                all Ultrascale, 7-Series, GTY, GTX, etc)

        Returns:
            A list of 'bad' mgts (ones which did not pass the bit check)
        """
        ideal_value = 0b1 if check_bit_high else 0b0
        bad_mgt_list = get_bad_mgts(
            self.get_results_dict_dual_layered(),
            header_key= header_key,
            ideal_value= ideal_value,
            mask_dict = {
                "Ultrascale GTH" : 0b1 if ultrascale else 0b0,
                "Ultrascale GTY" : 0b1 if ultrascale else 0b0,
                "7 Series GTX"   : 0b1 if series_7 else 0b0,
                "7 Series GTH"   : 0b1 if series_7 else 0b0,
            }
        )
        if included_mgts is not None:
            bad_mgt_list = [x for x in bad_mgt_list if x in included_mgts]

        bad_value_str = "low" if (ideal_value == 0b1) else "high"
        good_value_str = "high" if (bad_value_str == "low") else "low"

        if bad_mgt_list:
            self.log.write("WARNING - %s bit is %s on %s in-use %s:\n"%
                (header_key, bad_value_str, len(bad_mgt_list), mgt_text)
            )
            self.log.write(
                format_to_width(", ".join(bad_mgt_list), width=76, indent=4)
            )
            self.log.write("\n")
        else:
            self.log.write("GOOD - %s bit is %s on all in-use %s.\n"%
                (header_key, good_value_str, mgt_text)
            )

        self.log.flush()
        return bad_mgt_list


class ResetErrors(Action):
    """
    Action for resetting the error counters for in-use MGT receivers.

    Accomplishes this by raising and lowering the count reset bitfield on all
    receivers ('rx_reset.rxprbscntreset').
    """
    banner_name = "Reset Errors"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self, sleep_time=5):
        if self.display_progress:
            self.log.write("Resetting error counters ...\n")
            self.log.flush(timestamped=True)

        blip_register_group(
            manager = self.manager,
            gt_group = list(self.gt_conf.get_rx_gts()),
            register = "rx_reset.rxprbscntreset",
        )

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


class ResetErrorsAll(ResetErrors, Action):
    """
    Action for resetting the error counters for ALL present MGTs.

    This action behaves exactly like ResetErrors, only using all MGTs which are
    present, not just those which are receivers of links included in the link
    list.
    """
    banner_name = "Reset Errors All"
    def __init__(self, manager, *pos_args, **key_args):
        full_manager = Manager(
            user_conf=manager.conf,
            used_devs_only=True,
            dev_conf=manager.conf,
            log=manager.log
        )
        Action.__init__(self, full_manager, *pos_args, **key_args)


class GetErrors(Action):
    """
    Action for obtaining/report error counts for in-use MGT receivers.

    Both the error count and total bit count are stored in the results,
    although only the error count is used in the report. At the time of
    writing, the total bit count is not implemented in the latest FW on all
    devices. And for devices which do have the total bit counter implemented,
    the counter is too small to provide any meaningful information about the
    Bit Error Rate.
    """
    banner_name = "Get Errors"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self):
        if self.display_progress:
            self.log.write("Getting error data ...\n")
            self.log.flush(timestamped=True)

        self.results.append(["MGT", "Error Count", "Total Count"])

        for gt_id in self.gt_conf.get_rx_gts():
            rx_mgt = self.devices[gt_id]
            err_cnt = rx_mgt.get_error_count()
            tot_cnt = rx_mgt["rx_count_tot"].read()
            self.results.append([gt_id, err_cnt, tot_cnt])

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


    def display_results(self):
        self.log.write("RXs with Errors:\n", 0, match=True)
        self.log.write("RX Error Count Statuses:\n", 1)

        no_of_err_gts = 0
        for entry in self.get_results()[1:]:
            rx_gt = entry[0]
            err_cnt = entry[1]
            if err_cnt:
                no_of_err_gts += 1
                verbosity = 0
            else:
                verbosity = 1

            self.log.write("    RX: {}\n".format(rx_gt), verbosity)
            self.log.write("    RX Links: {}\n".format(
                           ", ".join(self.gt_conf[rx_gt]["rx_links"])),
                           verbosity
                          )
            self.log.write(("    Error Count: %1.2e\n"%err_cnt), verbosity)
            self.log.write("\n", verbosity)

        if no_of_err_gts:
            self.log.write("# of MGT RXs with errors: %s\n"%(no_of_err_gts))
        else:
            self.log.write("    None\n", 0, match=True)
        self.log.write("\n")
        self.log.flush()


class GetAllGTValues(Action):
    """
    Action to get/display all mgt register/bitfield values for all in-use mgts.

    The output of this action is extremely lengthy and verbose, and hence is
    only suitable for debugging, not regular human-readable reports.
    """
    banner_name = "Get All GT Values"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self):
        header = []
        for gt_id in self.gt_conf:
            gt_status = get_all_gt_values(self.manager, gt_id)
            header = ["MGT"] + list(gt_status.keys())
            self.results.append([gt_id] + list(gt_status.values()))
        self.results.insert(0, header)


    def display_results(self):
        header = self.results[0]
        for entry in self.results[1:]:
            self.log.write('-'*80 + '\n')
            self.log.write("MGT - %s / %s\n"%(entry[0], gt_to_mgt(entry[0])))
            for i, value in enumerate(header[1:]):
                self.log.write("    %s - %s\n"%(value, hex(entry[i+1])))
        self.log.write('-'*80 + '\n')
        self.log.flush()


class InjectErrors(Action):
    """
    Action for injecting errors using all in-use mgt transmitters.

    Error injection is performed by changing the tx prbs pattern from that
    specified in the user config and back again. During the time that the
    "wrong" pattern is used, errors will be received by the receiver.

    It should be noted that the pattern change depends only on the pattern
    specified in the user config, not on the actual register value determining
    the pattern transmitted.

    In the future, the pattern-change method should be removed in favor using
    the error injection bit ("tx_force_err.txprbsforceerr"). But at the time of
    writing, the error injection bit behaves unreliably (takes several toggles
    to inject an error sometimes).
    """
    banner_name = "Inject Errors"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self):
        if self.display_progress:
            self.log.write("Injecting errors ...\n")
            self.log.flush(timestamped=True)

        # Old method, by changing pattern
        init_prbs = self.conf["link_init_parameters"]["prbs-select"]
        if init_prbs.lower() != "prbs-7":
            err_prbs = "PRBS-7"
        else:
            err_prbs = "PRBS-15"

        for gt in self.gt_conf.get_tx_gts():
            tx_mgt = self.devices[gt]
            tx_type = self.gt_conf[gt]["type"]
            tx_mgt.set_txprbssel(decode("prbs-select", err_prbs, tx_type))

        time.sleep(0.5)

        for gt in self.gt_conf.get_tx_gts():
            tx_mgt = self.devices[gt]
            tx_type = self.gt_conf[gt]["type"]
            tx_mgt.set_txprbssel(decode("prbs-select", init_prbs, tx_type))

        ## New method, using forceerr bit
        #blip_register_group(
        #    self.manager,
        #    gt_group = list(self.gt_conf.get_tx_gts()),
        #    register = "tx_force_err.txprbsforceerr"
        #)

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


class Initialize(Action):
    """
    Action for initialling all in-use cards and MGTs with user config parameters.

    Writes user config settings to all relevant card/MGT registers, then resets
    all MGTs by toggling "common_control.reset_master" on and off.
    """
    banner_name = "Initialize"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self):
        if self.display_progress:
            self.log.write("Setting initialization parameters ...\n")
            self.log.flush(timestamped=True)

        self.set_device_init_values()
        self.set_mgt_init_values()

        self.log.write("Done!\n\n")
        self.log.flush(timestamped=True)

        self.log.write("Performing MGT Resets ... \n")
        self.log.flush(timestamped=True)

        ## Normal Method
        #mgt_group = [x for x in self.gt_conf]
        #blip_register_group(
        #    self.manager,
        #    mgt_group,
        #    "common_control.reset_master"
        #)

        # Special Method - Resets Quad Channel 0 MGTs last (to accomodate QPLL
        # issue for HTM Q109 and ROD Q110)
        is_quad_ch0 = lambda x : True if MGTProps(gt_id=x).quad_channel == 0 else False
        quad_ch0_mgt_group = [x for x in self.gt_conf if is_quad_ch0(x)]
        not_quad_ch0_mgt_group = [x for x in self.gt_conf if not is_quad_ch0(x)]
        blip_register_group(
            self.manager,
            not_quad_ch0_mgt_group,
            "common_control.reset_master"
        )
        blip_register_group(
            self.manager,
            quad_ch0_mgt_group,
            "common_control.reset_master"
        )

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


    def display_results(self):

        self.log.write("Used the following initialization parameters: \n")

        self.log.write("    Per-Card Parameters:\n")
        device_params = self.manager.conf["device_init_parameters"]
        for key, value in device_params.items():
            self.log.write("        '%s' : %s\n"%(key, value))
        self.log.write("\n")

        self.log.write("    Per-MGT Parameters:\n")
        link_params = self.manager.conf["link_init_parameters"]
        for key, value in link_params.items():
            self.log.write("        '%s' : %s\n"%(key, value))
        self.log.write("\n")
        self.log.flush()


    def set_device_init_values(self):
        """
        Writes initialization parameter values for each device.
        (i.e., parameters that can only be changed for the entire card)

        Initialization Parameters included so far:
            mgt equalizer
        """
        # Set parameters on a per-card/device basis
        mgt_equ = self.manager.conf["device_init_parameters"]["mgt equalizer"]
        for device in self.manager.devices:
            if device not in ("rod1", "rod2"):
                device_mgt = self.manager.devices[device]
                if not is_yaml_none(device_mgt):
                    if mgt_equ.lower() == "on":
                        device_mgt["csr"]["control"]["mgt_equ_en"].raise_bits()
                    elif mgt_equ.lower() == "off":
                        device_mgt["csr"]["control"]["mgt_equ_en"].lower_bits()
                    else:
                        raise ValueError("Invalid value for mgt equalizer: %s"%(mgt_equ))


    def set_mgt_init_values(self):
        # Set parameters on a per-link basis
        link_params = self.manager.conf["link_init_parameters"]
        prbs_val = link_params["prbs-select"]
        dfe_val = link_params["DFE"]
        pre_emph_val = link_params["pre-emphasis"]
        post_emph_val = link_params["post-emphasis"]
        swing_val = link_params["swing"]
        loopback_val = link_params["loopback"]

        for tx in self.manager.gt_conf.get_tx_gts():
            tx_mgt = self.manager.devices[tx]
            tx_type = self.manager.gt_conf[tx]["type"]

            txprbs_val = decode("prbs-select", prbs_val, tx_type)
            tx_pre_emph_val = decode("pre-emphasis", pre_emph_val, tx_type)
            tx_post_emph_val = decode("post-emphasis", post_emph_val, tx_type)
            tx_swing_val = decode("swing", swing_val, tx_type)
            tx_loopback_val = decode("loopback", loopback_val, tx_type)

            tx_mgt.set_txprbssel(txprbs_val) 
            tx_mgt["tx_setup"]["pre_emphasis"].write(tx_pre_emph_val)
            tx_mgt["tx_setup"]["post_emphasis"].write(tx_post_emph_val)
            tx_mgt["tx_setup"]["swing"].write(tx_swing_val)
            tx_mgt["common_control"]["loopback"].write(tx_loopback_val)

        for rx in self.manager.gt_conf.get_rx_gts():
            rx_mgt = self.manager.devices[rx]
            rx_type = self.manager.gt_conf[rx]["type"]

            rxprbs_val = decode("prbs-select", prbs_val, rx_type)
            rx_dfe_val = decode("DFE", dfe_val, rx_type)
            rx_loopback_val = decode("loopback", loopback_val, rx_type)

            rx_mgt.set_rxprbssel(rxprbs_val) 
            rx_mgt["rx_setup"]["dfe_on_off"].write(rx_dfe_val)
            rx_mgt["common_control"]["loopback"].write(rx_loopback_val)


class InitializeAll(Initialize, Action):
    """
    Same as Initialize, but iniitializes all present cards/MGTs (not just the
    MGTs that are part of links in the link list).
    """
    banner_name = "Initialize All"
    def __init__(self, manager, *pos_args, **key_args):
        full_manager = Manager(
            user_conf=manager.conf,
            used_devs_only=True,
            dev_conf=manager.conf,
            log=manager.log
        )
        Action.__init__(self, full_manager, *pos_args, **key_args)


class CollectEyeDiagrams(Action):
    """
    Action for collecting eye diagrams for all in-use MGT receivers.
    """
    banner_name = "Collect Eye Diagrams"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self):
        if self.display_progress:
            self.log.write("Collecting Eye Diagrams ...\n")
            self.log.flush(timestamped=True)

        # Get title/base-filename to use for eye diagram files
        eye_log_filename = None
        log_name = self.log.log_name
        if log_name:
            eye_log_filename = log_name.split("/")[-1].replace(".txt", "")

        # Prepare a dictionary of arguments to provide to eye
        # diagram collection script, in place of an ArgGetter object that would
        # normally pull from the command line. Not very elegant, but it was the
        # quickest way to get eye diagram collection up and running.
        arg_dict = {
            "-d" : None,
            "--device" : None,
            "-q" : None,
            "--quad" : None,
            "-c" : None,
            "--channel" : None,
            "-t" : None,
            "--title" : None,
            "-p" : None,
            "--prescale" : None,
            "-m" : None,
            "--max_realign" : None,
            "-f" : eye_log_filename,
            "--file" : eye_log_filename,
            "-h" : False,
            "--help" : False,
            "-s" : False,
            "--show" : False,
            "--raw_only" : False,
        }

        headers = ["Link Name",
                   "MGT RX",
                   "Open Area",
                   "Horiz Open",
                   "Vert Open",
                   "Realignments",
                   "Max Realigns",
                   "Errors B&A",
                   "Time (s)",
                  ]

        self.results.append(headers)

        no_of_rx_gts = self.manager.gt_conf.get_no_of_rx_gts()
        i = 0
        for rx_gt_id in self.manager.gt_conf.get_rx_gts():
            i += 1
            link_names = '-'.join(self.manager.gt_conf[rx_gt_id]["rx_links"])
            self.log.write("Collecting Eye Diagram for RX %s - Link %s [%s/%s] ... \n"%
                           (rx_gt_id, link_names, i, no_of_rx_gts))
            self.log.flush(timestamped=True)

            rx_props = MGTProps(gt_id=rx_gt_id)
            device = rx_props.device
            gt_ch = rx_props.gt_ch

            # Provide correct device and channel to eye diagram collector script
            arg_dict["-d"] = device
            arg_dict["--device"] = arg_dict["-d"]
            arg_dict["-c"] = gt_ch
            arg_dict["--channel"] = arg_dict["-c"]
            arg_dict["-t"] = link_names
            arg_dict["--title"] = arg_dict["-t"]

            eye_return_dict = eye_collector.main(arg_dict)
            
            # Write intermediate report of eye diagram results (these will also
            # be in a table at the end, but this way we still get something
            # even if the test stops early).
            self.log.write(
                "Open Area: %s (%s)\nErrors Before/After: %s/%s\nRealignments Performed: %s\n"%(
                    eye_return_dict["Open Area"],
                    eye_return_dict["Open Area Percentage"],
                    eye_return_dict["Errors Before"],
                    eye_return_dict["Errors After"],
                    eye_return_dict["Realignments Performed"],
                )
            )

            # Prepare some entries for table not in eye_return dict
            open_area_str = "%s (%s)"%(eye_return_dict["Open Area"], eye_return_dict["Open Area Percentage"])
            errors_before_after_str = "%1.1e/%1.1e"%(eye_return_dict["Errors Before"], eye_return_dict["Errors After"])
            try:
                total_time_taken_str = "%1.1e"%(float(eye_return_dict["Total Time Taken"]))
            except:
                total_time_taken_str = "NA"

            # Save results for this receiver
            self.results.append(
                [
                    link_names,
                    rx_gt_id,
                    open_area_str,
                    eye_return_dict["Horizontal Opening Percentage"],
                    eye_return_dict["Vertical Opening Percentage"],
                    eye_return_dict["Realignments Performed"],
                    eye_return_dict["Maximum Realignments Reached"],
                    errors_before_after_str,
                    total_time_taken_str,
                ]
            )
            self.log.flush(timestamped=True)

        self.log.write("Collection Finished!\n")
        self.log.flush(timestamped=True)


    def display_results(self):
        eye_diagram_table = make_table(
            self.get_results(),
            headers="firstrow",
        )
        self.log.write(eye_diagram_table)
        self.log.write("\n\n")
        self.log.flush()


class CheckTXStatus(Action):
    """
    Action for reporting/judging the status bits of in-use MGT transmitters.
    """
    banner_name = "Check TX Status"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self):
        if self.display_progress:
            self.log.write("Checking TX Status ...\n")
            self.log.flush(timestamped=True)

        headers = ["MGT",
                   "tx_status",
                   "txpmaresetdone",
                   "reset_tx_done",
                   "buffbypass_tx_done",
                   "buffbypass_tx_error",
                   "txdlyresetdone",
                   "txphaligndone",
                   "txphaligninitdone",
                   "txsyncdone",
                   "txsyncout",
                  ]

        self.results.append(headers)
        for mgt_id in self.manager.gt_conf:
            row = [mgt_id]
            row.append(self.manager.devices[mgt_id]["tx_status"].read())
            for header in headers[2:]:
                value = self.manager.devices[mgt_id]["tx_status"][header].read()
                row.append(value)
            self.results.append(row)

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


    def display_results(self):
        tx_list = list(self.manager.gt_conf.get_tx_gts())
        tx_list = [MGTProps(gt_id=x).mgt_id for x in tx_list]
        self._display_bit_high_or_low("txpmaresetdone", check_bit_high=True, ultrascale=True, included_mgts=tx_list, mgt_text="Ultrascale MGT transmitters")
        self._display_bit_high_or_low("reset_tx_done", check_bit_high=True, ultrascale=True, series_7=True, included_mgts=tx_list, mgt_text="MGT transmitters")
        self._display_bit_high_or_low("buffbypass_tx_done", check_bit_high=True, ultrascale=True, included_mgts=tx_list, mgt_text="Ultrascale MGT transmitters")
        self._display_bit_high_or_low("buffbypass_tx_error", check_bit_high=False, ultrascale=True, included_mgts=tx_list, mgt_text="Ultrascale MGT transmitters")
        #self._display_bit_high_or_low("txdlyresetdone", check_bit_high=True, ultrascale=True, included_mgts=tx_list, mgt_text="Ultrascale MGT transmitters")
        self._display_bit_high_or_low("txphaligndone", check_bit_high=True, ultrascale=True, included_mgts=tx_list, mgt_text="Ultrascale MGT transmitters")
        self._display_bit_high_or_low("txphaligninitdone", check_bit_high=True, ultrascale=True, included_mgts=tx_list, mgt_text="Ultrascale MGT transmitters")
        self._display_bit_high_or_low("txsyncdone", check_bit_high=True, ultrascale=True, included_mgts=tx_list, mgt_text="Ultrascale MGT transmitters")
        #self._display_bit_high_or_low("txsyncout", check_bit_high=True, ultrascale=True, included_mgts=tx_list)


class CheckRXStatus(Action):
    """
    Action for reporting/judging the status bits of in-use MGT receivers.
    """
    banner_name = "Check RX Status"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self):
        if self.display_progress:
            self.log.write("Checking RX Status ...\n")
            self.log.flush(timestamped=True)

        headers = ["MGT",
                   "mgt_cdr_locked",# This bitfield is in common_status, but would more appropriately be placed in rx_status
                   "rx_status",
                   "rxpmaresetdone",
                   "reset_rx_done",
                   "buffbypass_rx_done",
                   "buffbypass_rx_error",
                   "rxprbserr_flg",
                   "rxprbslocked",
                   "rxdlyresetdone",
                   "rxphaligndone",
                   "rxphalignerr",
                   "rxsyncdone",
                   "rxsyncout",
                  ]

        self.results.append(headers)
        for mgt_id in self.manager.gt_conf:
            row = [mgt_id]
            row.append(self.manager.devices[mgt_id]["common_status.mgt_cdr_locked"].read())
            row.append(self.manager.devices[mgt_id]["rx_status"].read())
            for header in headers[3:]:
                value = self.manager.devices[mgt_id]["rx_status"][header].read()
                row.append(value)
            self.results.append(row)

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


    def display_results(self):
        rx_list = list(self.manager.gt_conf.get_rx_gts())
        rx_list = [MGTProps(gt_id=x).mgt_id for x in rx_list]
        self._display_bit_high_or_low("mgt_cdr_locked", check_bit_high=True, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")
        self._display_bit_high_or_low("rxpmaresetdone", check_bit_high=True, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")
        self._display_bit_high_or_low("reset_rx_done", check_bit_high=True, ultrascale=True, series_7=True, included_mgts=rx_list, mgt_text="MGT receivers")
        self._display_bit_high_or_low("buffbypass_rx_done", check_bit_high=True, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")
        self._display_bit_high_or_low("buffbypass_rx_error", check_bit_high=False, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")
        self._display_bit_high_or_low("rxprbserr_flg", check_bit_high=False, ultrascale=True, series_7=True, included_mgts=rx_list, mgt_text="MGT receivers")
        self._display_bit_high_or_low("rxprbslocked", check_bit_high=True, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")
        #self._display_bit_high_or_low("rxdlyresetdone", check_bit_high=True, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")
        self._display_bit_high_or_low("rxphaligndone", check_bit_high=True, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")
        self._display_bit_high_or_low("rxphalignerr", check_bit_high=False, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")
        self._display_bit_high_or_low("rxsyncdone", check_bit_high=True, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")
        #self._display_bit_high_or_low("rxsyncout", check_bit_high=True, ultrascale=True, included_mgts=rx_list, mgt_text="Ultrascale MGT receivers")


class CheckRXLocked(CheckRXStatus, Action):
    """
    Action for reporting/judging rxprbslocked bit for each in-use MGT receiver.

    Inherits from CheckRXStatus and collects the same data, but only reports
    the results for rxprbslocked, and none of the other status bits.
    """
    banner_name = "Check RX Locked"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)

    def display_results(self):
        self._display_bit_high_or_low("rxprbslocked", check_bit_high=True, ultrascale=True, mgt_text="Ultrascale MGT receivers")


class CheckCommonStatus(Action):
    """
    Action for reporting/judging the status bits of in-use MGT transceivers.
    """
    banner_name = "Check Common Status"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)

    def _perform_action(self):
        if self.display_progress:
            self.log.write("Checking Common Status ...\n")
            self.log.flush(timestamped=True)

        headers = ["MGT",
                   "common_status",
                   "init_done",
                   "gtpowergood",
                   "init_retry_ctr",
                   #"mgt_cdr_locked",# This bitfield is in common_status, but would more appropriately be placed in rx_status
                   "qplllock",
                   "gt_crc_err",
                  ]

        self.results.append(headers)
        for mgt_id in self.manager.gt_conf:
            row = [mgt_id]
            row.append(self.manager.devices[mgt_id]["common_status"].read())
            for header in headers[2:]:
                value = self.manager.devices[mgt_id]["common_status"][header].read()
                row.append(value)
            self.results.append(row)

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


    def display_results(self):
        self._display_bit_high_or_low("init_done", check_bit_high=True, ultrascale=True, series_7=True, mgt_text="Ultrascale MGTs")
        self._display_bit_high_or_low("gtpowergood", check_bit_high=True, ultrascale=True, mgt_text="Ultrascale MGTs")
        #self._display_bit_high_or_low("init_retry_ctr", check_bit_high=True, ultrascale=True, mgt_text="Ultrascale MGTs")
        #self._display_bit_high_or_low("mgt_cdr_locked", check_bit_high=True, ultrascale=True, mgt_text="Ultrascale MGTs")
        self._display_bit_high_or_low("qplllock", check_bit_high=True, ultrascale=True, series_7=True, mgt_text="MGTs")


class CheckInitDone(CheckCommonStatus, Action):
    """
    Action for reporting/judging init_done bit for each in-use MGT transceiver.

    Inherits from CheckCommonStatus and collects the same data, but only reports
    the results for init_done, and none of the other status bits.
    """
    banner_name = "Check Init Done"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)

    def display_results(self):
        self._display_bit_high_or_low("init_done", check_bit_high=True, ultrascale=True, series_7=True)


class FindLinkRoutes(Action):
    """
    Action for determining/verifying the routing of MGT transceivers.

    Determines routes by injecting errors on each in-use tx, then checking for
    new errors on each in-use rx. If an rx has errors from the start, it will
    not be picked up as part of a link. That way, "broken" rxs will not appear
    to be linked to every transmitter. Error counters on all in-use rxs are
    reset after each tx error injection (and subsequent link checks).
    """
    banner_name = "Find Link Routes"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self):
        if self.display_progress:
            self.log.write("Checking Link Routes ...\n")
            self.log.flush(timestamped=True)

        no_of_rx_gts = self.gt_conf.get_no_of_rx_gts()
        i = 1
        length = self.gt_conf.get_no_of_tx_gts()
        for gt in self.gt_conf.get_tx_gts():
            self.log.write("Checking route for transmitter [%s/%s] ...\r"%
                           (i,length))
            self.log.flush(timestamped=True)
            i += 1

            time.sleep(0.1)
            # Get gts with initial errors (card_name and channel, no err_cnt)
            get_errors = GetErrors(self.manager)
            init_err_gts = [x[0] for x in get_errors.get_results()[1:] if x[1]]
            self.results.append([None]+init_err_gts)

            self.inject_errors_single(gt, 0.000001)
            #self.inject_errors_single(gt, 0.5)

            # Get gts with errors after injection
            get_errors = GetErrors(self.manager)
            final_err_gts = [x[0] for x in get_errors.get_results()[1:] if x[1]]
            # Filter out gts with init errors to get rx gts 'linked' to tx
            rx_gts = [x for x in final_err_gts if x not in init_err_gts]
            self.results.append([gt]+rx_gts)

            blip_register_group(
                self.manager,
                list(self.gt_conf.get_rx_gts()),
                "rx_reset.rxprbscntreset",
                0.000001
            )

        self.log.write("\n")
        self.log.flush()

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)

    def inject_errors_single(self, gt, wait_time=0.5):
        """
        Helper method for injecting errors using a single mgt transmitter.

        Errors are injected by changing the transmitted pattern, and by using
        the forceerr bit (safer to use both methods, and for some transceivers
        changing the pattern requires an MGT reset).
        """
        ## Old method, by changing pattern
        #init_prbs = self.conf["link_init_parameters"]["prbs-select"]
        #if init_prbs.lower() != "prbs-7":
        #    err_prbs = "PRBS-7"
        #else:
        #    err_prbs = "PRBS-15"
        #tx_mgt = self.devices[gt]
        #tx_type = self.gt_conf[gt]["type"]
        #tx_mgt.set_txprbssel(decode("prbs-select", err_prbs, tx_type))
        #time.sleep(wait_time)
        #tx_mgt.set_txprbssel(decode("prbs-select", init_prbs, tx_type))
        # New method, by using forceerr bit
        tx_mgt = self.devices[gt]
        # Error injection is finnicky, so we do it 10 times to be safe
        for i in range(20):
            tx_mgt.inject_error(wait_time=wait_time)


    def display_results(self):
        """
        Display link route information. Manager must only contain full links
        (i.e. only links which have both an implemented source and destination)
        """
        init_err_rx_set = set()
        for entry in self.get_results():
            tx_gt = entry[0]
            # Every other entry is the list of initial errors
            # For those entries, update the set of init error rxs, then move on
            if is_yaml_none(tx_gt):
                init_err_rx_set = set(entry[1:])
                continue
            tx_links = self.gt_conf[tx_gt]["tx_links"]
            ideal_rx_set = set([self.gt_conf.link_to_rx_gt(x) for x in tx_links
                                if not
                                is_yaml_none(self.gt_conf.link_to_rx_gt(x))])
            real_rx_set = set(entry[1:])
            # RXs which should be linked to the TX, but have init errors
            broken_rx_set = ideal_rx_set.intersection(init_err_rx_set)

            if ideal_rx_set != real_rx_set:
                self.log.write("[PROBLEM]   tx: %s\n"%(tx_gt), 1)
                self.log.write("       Exp rxs: %s\n"%(", ".join(ideal_rx_set)), 1)
                self.log.write("    Actual rxs: %s\n"%(", ".join(real_rx_set)), 1)
                self.log.write("Broken Exp rxs: %s\n"%(", ".join(broken_rx_set)), 1)
            else:
                self.log.write("[GOOD] tx: %s\n"%(tx_gt), 2)
                self.log.write("  Exp rxs: %s\n"%(ideal_rx_set), 2)
                self.log.write("   Actual: %s\n"%(real_rx_set), 2)

            self.log.write("\n", 1)
            self.log.flush()


class GetCardStatus(Action):
    """
    Action for reporting status of each in-use card (DNA #, firmware ver, etc)
    """
    banner_name = "Get Card Status"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _get_rod_table(self):
        """
        Helper function for getting table of ROD status (differs from Hubs and
        HTMs, so requires different table).
        """

        rod_table = []
        device_list = [x for x in self.devices if x in ("rod1", "rod2")]

        # If no rods, nothing to do
        if not device_list:
            return ""

        headers = (
            "Card",
            "Connhub ID",
            "DNA #",
            "FW Version",
            "Build Date",
            "Build Time",
            "Xml Version",
            "Module_ID",
        )
        rod_table.append(headers)
        self.results.append(headers)

        device_list = [x for x in self.devices if x in ("rod1", "rod2")]
        for card_name in device_list:
            card_hw = self.devices[card_name]
            connhub_id = str(card_hw)
            serial_no = str(card_hw["common_IdVersion.Module_ID.Serial"].read())

            result_entry = (
                card_name,
                connhub_id,
                serial_no,
                self._get_rod_fw_ver_full(card_name),
                self._get_rod_build_date(card_name),
                self._get_rod_build_time(card_name),
                self._get_rod_xml_ver(card_name),
                self._get_rod_module_id(card_name),
            )
            rod_table.append(result_entry)
            self.results.append(result_entry)

        card_status = make_table(
            rod_table,
            headers="firstrow",
        )

        return card_status


    def _get_rod_fw_ver_full(self, card_name):
        """
        Helper function for getting ROD firmware version, to be used in ROD
        status table.
        """
        com_id_hw = self.devices[card_name]["common_IdVersion"]
        fw_ver_hw = com_id_hw["FirmwareVersion"]["Version"]
        fw_maj_ver = str(fw_ver_hw["Major_Version"].read())
        fw_min_ver = str(fw_ver_hw["Minor_Version"].read())
        fw_patch = str(fw_ver_hw["Patch"].read())
        fw_ver_full = ".".join((fw_maj_ver, fw_min_ver, fw_patch))
        return fw_ver_full


    def _get_rod_build_date(self, card_name):
        """
        Helper function for getting ROD FW build date, to be used in ROD status
        table.
        """
        com_id_hw = self.devices[card_name]["common_IdVersion"]
        build_hw = com_id_hw["BuildTimeAndDate"]
        build_date_int = build_hw["BuildDate"].read()
        build_month = hex((build_date_int & 0x00ff000000) >> 6*4).split("0x")[-1]
        build_day   = hex((build_date_int & 0x0000ff0000) >> 4*4).split("0x")[-1]
        build_year  = hex((build_date_int & 0x000000ffff)).split("0x")[-1]
        build_date_str = "/".join((build_month, build_day, build_year))
        return build_date_str


    def _get_rod_build_time(self, card_name):
        """
        Helper function for getting ROD FW build time, to be used in ROD status
        table.
        """
        com_id_hw = self.devices[card_name]["common_IdVersion"]
        build_hw = com_id_hw["BuildTimeAndDate"]
        build_time_int = build_hw["BuildTime"].read()
        build_hr  = hex((build_time_int & 0x0000ff0000) >> 4*4).split("0x")[-1]
        build_min = hex((build_time_int & 0x000000ff00) >> 2*4).split("0x")[-1]
        build_sec = hex((build_time_int & 0x00000000ff)).split("0x")[-1]
        build_time_str = ":".join((build_hr, build_min, build_sec))
        return build_time_str


    def _get_rod_module_id(self, card_name):
        """
        Helper function for getting ROD module id, to be used in ROD status
        table.
        """
        com_id_hw = self.devices[card_name]["common_IdVersion"]
        common_module_hw = com_id_hw["Module_ID"]
        l1calo_no = str(common_module_hw["L1CaloNumber"].read())
        serial_no = str(common_module_hw["Serial"].read())
        issue = str(common_module_hw["Issue"].read())
        pbc_hw_update_no = str(common_module_hw["pcbUpdate"].read())
        module_id = ".".join(
            (l1calo_no, serial_no, issue, pbc_hw_update_no)
        )
        return module_id


    def _get_rod_xml_ver(self, card_name):
        """
        Helper function for getting ROD xml version, to be used in ROD status
        table.
        """
        com_id_hw = self.devices[card_name]["common_IdVersion"]
        xml_hw = com_id_hw["XmlVersion"]["Version"]
        xml_maj_ver = str(xml_hw["Major_Version"].read())
        xml_min_ver = str(xml_hw["Minor_Version"].read())
        xml_patch = str(xml_hw["Patch"].read())
        xml_ver_full = ".".join((xml_maj_ver, xml_min_ver, xml_patch))
        return xml_ver_full


    def _get_hub_and_htm_table(self):
        """
        Helper function for getting table of Hub/HTM card status (differs from
        RODs, so requires different table).
        """

        hub_and_htm_table = []
        device_list = [x for x in self.devices if x not in ("rod1", "rod2")]
        # If no hubs or htms, nothing to do
        if not device_list:
            return ""

        headers = (
            "Card",
            "Connhub ID",
            "Type",
            "DNA #",
            "Slot Addr",
            "Shelf Addr",
            "FW Release",
            "FW Ver",
            "Aurora",
            "Combined",
            "Rdout Ctl",
            "GBT Ver",
            "IBERT",
            "Support",
            "Build Date",
        )
        hub_and_htm_table.append(headers)
        self.results.append(headers)

        for card_name in device_list:
            card_hw = self.devices[card_name]
            connhub_id = str(card_hw)
            csr_hw = card_hw["csr"]
            version_hw = card_hw["versions"]

            ver_dot_rev = lambda x : ".".join([str((x >> 2*4) & 0xff),
                                               str(x & 0xff)])

            result_entry = (
                card_name,
                connhub_id,
                self._get_hub_htm_module_type(card_name),
                csr_hw["module"]["ser_number"].read(),
                hex(csr_hw["address"]["slot_addr"].read() & 0x7f), # Mask out parity bit
                hex(csr_hw["address"]["shelf_addr"].read()),
                self._get_hub_htm_fw_rel(card_name),
                self._get_hub_htm_fw_ver(card_name),
                ver_dot_rev(version_hw["aur_comb"]["aurora"].read()),
                ver_dot_rev(version_hw["aur_comb"]["combined"].read()),
                ver_dot_rev(version_hw["rdoutctrl_gbt"]["readoutctrl"].read()),
                ver_dot_rev(version_hw["rdoutctrl_gbt"]["gbt"].read()),
                ver_dot_rev(version_hw["ibert_support"]["ibert"].read()),
                ver_dot_rev(version_hw["ibert_support"]["support"].read()),
                self._get_hub_htm_build_date(card_name),
            )

            hub_and_htm_table.append(result_entry)
            self.results.append(result_entry)

        card_status = make_table(
            hub_and_htm_table,
            headers="firstrow",
        )

        return card_status


    def _get_hub_htm_module_type(self, card_name):
            csr_hw = self.devices[card_name]["csr"]
            module_type = csr_hw["module"]["module_type"].read()
            if module_type == 1:
                module_type = "Hub"
            elif module_type == 2:
                module_type = "HTM"
            else:
                module_type = "Unknown"
            return module_type


    def _get_hub_htm_fw_ver(self, card_name):
            csr_hw = self.devices[card_name]["csr"]
            fw_ver = str(csr_hw["module"]["fw_version"].read())
            fw_rev = str(csr_hw["module"]["fw_revision"].read())
            fw_ver_full = ".".join((fw_ver, fw_rev))
            return fw_ver_full


    def _get_hub_htm_fw_rel(self, card_name):
            version_hw = self.devices[card_name]["versions"]
            fw_release = version_hw["fw_release"].read()
            fw_release = ".".join([str(fw_release >> 4*4),
                                   str(fw_release & 0xffff)])
            return fw_release


    def _get_hub_htm_build_date(self, card_name):
            csr_hw = self.devices[card_name]["csr"]
            build_month = hex(csr_hw["date"]["month"].read()).split("0x")[-1]
            build_day = hex(csr_hw["date"]["day"].read()).split("0x")[-1]
            build_year = hex(csr_hw["date"]["year"].read()).split("0x")[-1]
            build_date_str = "/".join((build_month, build_day, build_year))
            return build_date_str


    def _perform_action(self):

        if self.display_progress:
            self.log.write("Checking Card Status ...\n")
            self.log.flush(timestamped=True)

        self.hub_and_htm_table = self._get_hub_and_htm_table()
        self.rod_table = self._get_rod_table()

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


    def display_results(self):

        self.log.write(self.hub_and_htm_table)
        self.log.write("\n\n")
        self.log.write(self.rod_table)
        self.log.write("\n\n")
        self.log.flush()


class GetOpticalPower(Action):
    """
    Action for reporting optical power of all present minipod txs and rxs.

    This action behaves somewhat uniquely because the optical powers must be
    read using I2C. Since we have no way of a priori way of knowing which cards
    have minipods (e.g. in a config file), this action tries to get optical
    power from minipods on all in-use cards, and abandons results for which an
    exception occurs while trying to read.

    This is a relatively sloppy way to handle the situation, and in the future
    the i2c_monitor may be modified raise a more specific exception in the case
    that no minipod device is present (if possible).
    """
    banner_name = "Get Optical Power"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)

    def _perform_action(self):

        if self.display_progress:
            self.log.write("Checking Optical Power ...\n")
            self.log.flush(timestamped=True)

        # Header row
        self.results.append(["Channel"]+list(range(0,12)))

        # Rxs
        for device in self.devices:
            row = [device + " rx"]
            card_uhal= self.devices[device].get_uhal()
            # Since we do not know which cards have minipods, a catch-all
            # try-except suite is used. We try to get the optical power for
            # each card. If there is none: we get an exception, don't save the
            # data in results, and move on to the next card.
            try: 
                rx_mpod = i2c_monitor.MiniPodMonitor(card_uhal, "recvr_mpod", 0)
                rx_mpod_data = rx_mpod.return_data()

                for channel in range(0,12):
                    opt_power = rx_mpod_data["OPTICAL_POW_CHAN_%s_uW"%(channel)]
                    row.append("{:.1f}".format(opt_power).ljust(6).strip())
                self.results.append(row)
            except:
                pass

        # Txs, handled nearly the same as RXs (in future, should have one
        # function that handles both cases)
        for device in self.devices:
            row = [device + " tx"]
            card_uhal= self.devices[device].get_uhal()
            try: 
                tx_mpod = i2c_monitor.MiniPodMonitor(card_uhal, "trans_mpod", 0)
                tx_mpod_data = tx_mpod.return_data()

                for channel in range(0,12):
                    opt_power = tx_mpod_data["OPTICAL_POW_CHAN_%s_uW"%(channel)]
                    row.append("{:.1f}".format(opt_power).ljust(6).strip())
                self.results.append(row)
            except:
                pass


        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


    def display_results(self):

        power_table = make_table(
            self.get_results(),
        )
        table_char_width = power_table.find('\n')
        first_col = power_table.find('+', 2)
        table_header = " "*first_col +  "+" + "-"*(table_char_width-2-first_col) + "+\n"
        table_header += " "*first_col + "|{title:^{width}s}|\n".format(
            title="Optical Power (uW)", width=table_char_width-2-first_col
        )
        power_table = table_header+power_table
        self.log.write(power_table)
        self.log.write("\n\n")
        self.log.flush()


class GetDCDCStatus(Action):
    """
    Action for reporting DCDC power supplies for Hub and ROD cards.

    DCDC power supplies are read using I2C. ROD power supplies are read using
    I2C through the ROD's own Hub (if both the Hub and ROD are present). 
    """
    banner_name = "Get DCDC Status"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)

    def _perform_action(self):

        if self.display_progress:
            self.log.write("Checking DCDC Status ...\n")
            self.log.flush(timestamped=True)

        headers = (
            "Card",
            "I2C Device",
            "V_OUT (V)",
            "I_OUT (A)",
        )

        self.results.append(headers)

        cards_tested_table_list = []
        card_name_to_device_type_dict = {
            "hub1" : "HUB",
            "hub2" : "HUB",
            #"rod1" : "ROD",
            #"rod2" : "ROD",
        }

        for hub, rod in (("hub1", "rod1"), ("hub2", "rod2")):
            if hub in self.devices:
                card_uhal= self.devices[hub].get_uhal()
                power_monit_hub = i2c_monitor.PowerModule(card_uhal, "hub_dcdc", 0, "HUB")
                data_dict = power_monit_hub.return_data()

                for i2c_device in data_dict:
                    v_out = data_dict[i2c_device]["V_OUT"]
                    i_out = data_dict[i2c_device]["I_OUT"]
                    result_entry = (
                        hub,
                        i2c_device,
                        v_out,
                        i_out,
                    )
                    self.results.append(result_entry)

                if rod in self.devices:
                    power_monit_rod = i2c_monitor.PowerModule(card_uhal, "hub_dcdc", 0, "ROD")
                    data_dict = power_monit_rod.return_data()
                    for i2c_device in data_dict:
                        v_out = data_dict[i2c_device]["V_OUT"]
                        i_out = data_dict[i2c_device]["I_OUT"]
                        result_entry = (
                            "%s/%s"%(hub, rod),
                            i2c_device,
                            v_out,
                            i_out,
                        )
                        self.results.append(result_entry)


        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)

    def display_results(self):
        card_status = make_table(
            self.get_results(),
            headers="firstrow",
        )
        self.log.write(card_status)
        self.log.write("\n\n")
        self.log.flush()


class GetGBTStatus(Action):
    """
    Action for reporting GBT status.

    Still unsure of the functionality/meaning of these registers. In future,
    may be able to improve output to tell when something is wrong.
    """
    banner_name = "Get GBT Status"
    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)

    def _get_gbt_rx_table(self):
        device_list = [x for x in self.devices if x in ("hub1", "hub2")]
        # If no devices, nothing to do
        if not device_list:
            return ""

        gbt_rx_table_data = []
        rx_headers = (
            "Card",
            "Bits Modified",
            "Words Received",
            "Bit Slip Rst Count",
            "Wide Bus Err Seen",
            "Data Err Seen",
            "Ready Lost Flag",
            "Ready",
            "Frame Clk Ready",
            "latOptGbtBankRx",
            "Is Data",
        )
        gbt_rx_table_data.append(rx_headers)

        for card_name in device_list:
            card_hw = self.devices[card_name]

            rx_entry = (
                card_name,
                hex(card_hw["versions.bitsmodified"].read()),
                hex(card_hw["versions.wordreceived"].read()),
                bin(card_hw["versions.gbtstatus.rbsrc"].read()),
                bin(card_hw["versions.gbtstatus.gbtrxedwbes"].read()),
                bin(card_hw["versions.gbtstatus.gbtrxdeeseen"].read()),
                bin(card_hw["versions.gbtstatus.gbtrxrelofl"].read()),
                bin(card_hw["versions.gbtstatus.gbtrxready"].read()),
                bin(card_hw["versions.gbtstatus.gbtrxfrclkrdy"].read()),
                bin(card_hw["versions.gbtstatus.gbtlatoptrx"].read()),
                bin(card_hw["versions.gbtstatus.gbtrxisdata"].read()),
            )
            gbt_rx_table_data.append(rx_entry)

        gbt_rx_table = make_table(
            gbt_rx_table_data,
            headers="firstrow",
        )

        return gbt_rx_table


    def _get_gbt_other_table(self):
        device_list = [x for x in self.devices if x in ("hub1", "hub2")]
        # If no devices, nothing to do
        if not device_list:
            return ""

        gbt_other_table_data = []
        other_headers = (
            "Card",
            "TX Align Computed",
            "TX Aligned",
            "MGT Ready",
            "latOptGbtBankTx",
            "TX Frame Clk Pll Locked",
        )
        gbt_other_table_data.append(other_headers)

        for card_name in device_list:
            card_hw = self.devices[card_name]

            other_entry =  (
                card_name,
                bin(card_hw["versions.gbtstatus.gbttxaligcomp"].read()),
                bin(card_hw["versions.gbtstatus.gbttxaligned"].read()),
                bin(card_hw["versions.gbtstatus.gbtmgtready"].read()),
                bin(card_hw["versions.gbtstatus.gbtlatopttx"].read()),
                bin(card_hw["versions.gbtstatus.gbttxfrclkpll"].read()),
            )
            gbt_other_table_data.append(other_entry)

        gbt_other_table = make_table(
            gbt_other_table_data,
            headers="firstrow",
        )

        return gbt_other_table


    def _perform_action(self):
        if self.display_progress:
            self.log.write("Checking GBT Status ...\n")
            self.log.flush(timestamped=True)

        # Table collection broken up into two, to keep table from being
        # unreasonably wide.
        self.gbt_rx_table = self._get_gbt_rx_table()
        self.gbt_other_table = self._get_gbt_other_table()

        if self.display_progress:
            self.log.write("Done!\n\n")
            self.log.flush(timestamped=True)


    def display_results(self):
        self.log.write("GBT RX Table:\n")
        self.log.write(self.gbt_rx_table)
        self.log.write("\n\n")
        self.log.write("GBT TX/Common Table:\n")
        self.log.write(self.gbt_other_table)
        self.log.write("\n\n")
        self.log.flush()


class FullReport(Action):
    """
    Action for obtaining full report of ALL links involving present cards.

    In particular, the route status, error counts, total bit count, tx status,
    common status, and rx status are checked for each link.

    Links are determined using the FindLinkRoutes action, but with a special
    manaager including ALL MGTs as transmitters and receivers. Therefore, even
    connections which are not defined as part of any link will found and
    included.

    In order to preserve the error count of links throughout and IPBus IBERT
    test, a FindLinkRoutes action is only ran the FIRST TIME that a FullReport
    action is run in a script. Subsequent FullReport actions will use the
    routing information from the first run.

    Attributes:
        full_link_routes_action: attribute for storing a full FindLinkRoutes
            action during the first run of the FullReport action. The routing
            information from the results will be used in all subsequent runs of
            the FullReport action.
        full_manager: manager including all MGTs, not just those of in-use
            links.
    """
    # Class variable for storing a full link routes action (FindLinkRoutes
    # action, with all transceivers used). Implemented this way so that only
    # the first FullReport needs to perform a FindLinkRoutes action
    full_link_routes_action = None
    full_manager = None
    banner_name = "Full Report"

    def __init__(self, *pos_args, **key_args):
        Action.__init__(self, *pos_args, **key_args)


    def _perform_action(self):

        if FullReport.full_manager is None:
            FullReport.full_manager = Manager(
                user_conf=self.conf,
                used_devs_only=True,
                dev_conf=self.conf,
                log=self.log
            )
        if FullReport.full_link_routes_action is None:
            FullReport.full_link_routes_action = FindLinkRoutes(
                FullReport.full_manager,
                display_progress=True
            )

        self.rx_err_cnt_action = GetErrors(
            FullReport.full_manager,
            display_progress=True
        )
        self.tx_status_action = CheckTXStatus(
            FullReport.full_manager,
            display_progress=True
        )
        self.rx_status_action = CheckRXStatus(
            FullReport.full_manager,
            display_progress=True
        )
        self.common_status_action = CheckCommonStatus(
            FullReport.full_manager,
            display_progress=True
        )

        final_report_data = self._get_final_report_data()
        route_status_dict = self._get_route_status()

        table = make_table(final_report_data, headers='keys')
        headers = list(final_report_data.keys())
        self.results.append(headers)
        no_of_rows = None
        for header in final_report_data:
            if no_of_rows is None:
                no_of_rows = len(final_report_data[header])
            elif no_of_rows != len(final_report_data[header]):
                raise RuntimeError("Final Report contains columns of differing length")

        self.results += [[final_report_data[header][i] for header in headers]
                        for i in range(no_of_rows)]

        self.log.write(table + "\n\n")
        self.log.write("# of Expected and Found Links: %s\n"%
                       len(route_status_dict["Good"]))
        self.log.write("# of Unexpected but Found Links: %s\n"%
                       len(route_status_dict["Unexpected"]))
        self.log.write("# of Expected but Not Found Links: %s\n\n"%
                       len(route_status_dict["Not Found"]))

        good_status_cnt = 0
        bad_status_cnt = 0
        for status in final_report_data["Statuses Good"]:
            if status == "Yes":
                good_status_cnt += 1
            else:
                bad_status_cnt += 1

        self.log.write("# of Links with Good TX/RX/Common Statuses: %s\n"%
                good_status_cnt)
        self.log.write("# of Links with Bad TX/RX/Common Statuses: %s\n\n"%
                bad_status_cnt)

        non_zero_err_cnt = 0
        for i, linked in enumerate(final_report_data["Linked"]):
            if linked == "Yes":
                if final_report_data["Errors"][i] != "0":
                    non_zero_err_cnt += 1

        self.log.write("# of (found) Links with Non-Zero Error Counts: %s\n"%
                non_zero_err_cnt)

        self.log.flush()

    def _get_final_report_data(self):
        """
        Get dictionary of data to be used to create report table.
        """

        link_data = OrderedDict(
            [
                ("Link Name", []),
                ("Linked", []),
                ("Source", []),
                ("Dest", []),
                ("Errors", []),
                ("Total", []),
                ("BER", []),
                #("Init Done (tx/rx)", []),
                #("Rx Locked", []),
                ("Tx Status", []),
                ("Rx Status", []),
                ("Common Status (tx/rx)", []),
                ("Statuses Good", [])
            ]
        )

        route_status_dict = self._get_route_status()
        full_tx_rx_name_list = (
            route_status_dict["Good"] 
            + route_status_dict["Unexpected"]
            + route_status_dict["Not Found"]
        )

        for tx, rx, name in full_tx_rx_name_list:

            tx_props = MGTProps(gt_id=tx)
            rx_props = MGTProps(gt_id=rx)
            link_status = self._get_link_status(tx, rx)
            pretty_status = self._get_link_status_pretty(link_status)

            link_data["Link Name"].append(name)
            link_found = (tx, rx, name) not in route_status_dict["Not Found"]
            link_data["Linked"].append("Yes" if link_found else "No")
            link_data["Source"].append(tx_props.mgt_id)
            link_data["Dest"].append(rx_props.mgt_id)
            link_data["Errors"].append(pretty_status["err_cnt"])
            link_data["Total"].append(pretty_status["tot_cnt"])
            link_data["BER"].append(pretty_status["ber"])
            #link_data["Init Done (tx, rx)"].append(
            #    ", ".join(
            #        (pretty_status["init_done_tx"],
            #         pretty_status["init_done_rx"])
            #    )
            #)
            #link_data["Rx Locked"].append(pretty_status["rx_locked"])
            link_data["Tx Status"].append(pretty_status["tx_status"])
            link_data["Rx Status"].append(pretty_status["rx_status"])
            link_data["Common Status (tx/rx)"].append(
                "/".join(
                    (pretty_status["common_status_tx"],
                     pretty_status["common_status_rx"])
                )
            )
            link_is_good = self._check_link_status_good(
                link_status,
                tx_props.mgt_type,
                rx_props.mgt_type,
            )
            link_data["Statuses Good"].append("Yes" if link_is_good else "No")

        return link_data


    def _get_link_status(self, tx, rx):
        """
        For a given tx and rx, return a dictionary specifying the values of
        various status bits for that tx and rx.
        """

        link_status = {
            "init_done_tx" : "NA",
            "init_done_rx" : "NA",
            "tx_status" : "NA",
            "rx_status" : "NA",
            "common_status_tx" : "NA",
            "common_status_rx" : "NA",
            "err_cnt" : "NA",
            "tot_cnt" : "NA",
            "ber" : "NA",
            "rx_locked" : "NA",
        }

        rx_err_cnt_dict = self.rx_err_cnt_action.get_results_dict_dual_layered()
        tx_status_dict = self.tx_status_action.get_results_dict_dual_layered()
        rx_status_dict = self.rx_status_action.get_results_dict_dual_layered()
        common_status_dict = self.common_status_action.get_results_dict_dual_layered()

        if tx is not None:
            link_status["init_done_tx"] = common_status_dict[tx]["init_done"]
            link_status["tx_status"] = tx_status_dict[tx]["tx_status"]
            link_status["common_status_tx"] = common_status_dict[tx]["common_status"]

        if rx is not None:
            err_cnt = rx_err_cnt_dict[rx]["Error Count"]
            tot_cnt = rx_err_cnt_dict[rx]["Total Count"]
            link_status["err_cnt"] = float(err_cnt)
            link_status["tot_cnt"] = float(tot_cnt)
            link_status["ber"] = self._calculate_ber(err_cnt, tot_cnt)
            link_status["init_done_rx"] = common_status_dict[rx]["init_done"]
            link_status["rx_locked"] = rx_status_dict[rx]["rxprbslocked"]
            link_status["rx_status"] = rx_status_dict[rx]["rx_status"]
            link_status["common_status_rx"] = common_status_dict[rx]["common_status"]

        return link_status


    def _get_link_status_pretty(self, link_status):
        """
        Given a status dict, as returned by _get_link_status, return a dict
        with the same keys, but 'pretty' values (1s and 0s changed to 'Yes' and
        'No', floats changed to scientific notation, etc).
        """
        pretty_status = {}
        for key in ("init_done_tx", "init_done_rx", "rx_locked"):
            val = link_status[key]
            val = bin(val) if val != "NA" else val
            pretty_status[key] = val

        for key in ("tx_status", "rx_status", "common_status_tx",
                    "common_status_rx"):
            val = link_status[key]
            val = "0x"+format(val, "08x") if val != "NA" else val
            pretty_status[key] = val

        for key in ("err_cnt", "tot_cnt"):
            val = link_status[key]
            if val != "NA":
                val = "{:1.2e}".format(val) if val else "0"
            pretty_status[key] = val

        for key in ("ber",):
            val = link_status[key] 
            pretty_status[key] = val

        return pretty_status


    def _check_link_status_good(self, link_status, tx_type, rx_type):
        """
        Given a link status dict, as returned by _get_link_status, check
        whether the status is good, considering the specified tx and rx mgt
        types.
        """

        good_status_dict = {
            "init_done" : {
                "mask" : {
                    "Ultrascale GTH" : 0b1,
                    "Ultrascale GTY" : 0b1,
                    "7 Series GTX" : 0b1,
                    "7 Series GTH" : 0b1,
                },
                "value" : 0b1,
            },
            "rx_locked" : {
                "mask" : {
                    "Ultrascale GTH" : 0b1,
                    "Ultrascale GTY" : 0b1,
                    "7 Series GTX" :   0b0,
                    "7 Series GTH" :   0b0,
                },
                "value" : 0b1,
            },
            "tx_status" : {
                "mask" : {
                    "Ultrascale GTH" : 0x1c001111,
                    "Ultrascale GTY" : 0x1c001111,
                    "7 Series GTX" :   0x00000010,
                    "7 Series GTH" :   0x00000010,
                },
                "value" : 0x1c000111,
            },
            # Need tx and rx versions for common_status because mgt_cdr_locked 
            # is in common_status, when it is actually only relevant for RXs
            "common_status_tx" : {
                "mask" : {
                    "Ultrascale GTH" : 0x00010011,
                    "Ultrascale GTY" : 0x00010011,
                    "7 Series GTX" :   0x00010001,
                    "7 Series GTH" :   0x00010001,
                },
                "value" : 0x00011011,
            },
            "common_status_rx" : {
                "mask" : {
                    "Ultrascale GTH" : 0x00011011,
                    "Ultrascale GTY" : 0x00011011,
                    "7 Series GTX" :   0x00010001,
                    "7 Series GTH" :   0x00010001,
                },
                "value" : 0x00011011,
            },
            "rx_status" : {
                "mask" : {
                    "Ultrascale GTH" : 0x1c111111,
                    "Ultrascale GTY" : 0x1c111111,
                    "7 Series GTX" :   0x00010010,
                    "7 Series GTH" :   0x00010010,
                },
                "value" : 0x14100111,
            },
        }
        good_status_dict["common_status_tx"] = good_status_dict["common_status_tx"]
        good_status_dict["common_status_rx"] = good_status_dict["common_status_rx"]
        good_status_dict["init_done_tx"] = good_status_dict["init_done"]
        good_status_dict["init_done_rx"] = good_status_dict["init_done"]

        link_status_good = True

        for key in ["init_done_tx", "common_status_tx", "tx_status"]:
            act_val = link_status[key]
            if act_val != "NA":
                good_val = good_status_dict[key]["value"]
                val_mask = good_status_dict[key]["mask"][tx_type]
                if (act_val & val_mask) != (good_val & val_mask):
                    link_status_good = False

        for key in ["init_done_rx", "common_status_rx", "rx_status", "rx_locked"]:
            act_val = link_status[key]
            if act_val != "NA":
                good_val = good_status_dict[key]["value"]
                val_mask = good_status_dict[key]["mask"][rx_type]
                if (act_val & val_mask) != (good_val & val_mask):
                    link_status_good = False

        #err_cnt = link_status["err_cnt"]
        #if (err_cnt != "NA") and (int(err_cnt) != 0):
        #    links_status_good = False

        return link_status_good


    def _calculate_ber(self, err_cnt, tot_cnt):
        """
        Given the error count and count of total bits received, calculate bit
        error rate. If error count is 0, treat is as 1 (to get more accurate
        reflection of the efficacy of the MGT transmitter/receiver).
        """
        try:
            # Always treat as if there was at least one error
            if err_cnt == 0:
                err_cnt += 1
            ber = float(err_cnt)/float(tot_cnt)
            return "{:1.2e}".format(ber)
        except ZeroDivisionError:
            return "NA"


    def _get_route_status(self):
        """
        Get dictionary specifying lists of links which are 'Good' (expected and
        found) , 'Unexpected' (found but not expected), or 'Not Found'
        (expected but not found).
        """

        # Deepcopy becuase we will be modifying dictionary
        routes_dict = copy.deepcopy(
            FullReport.full_link_routes_action.get_results_dict()
        )
        good_links = []
        unexp_links = []
        not_found_links = []

        for link in self.manager.link_conf:
            tx = self.manager.gt_conf.link_to_tx_gt(link)
            rx = self.manager.gt_conf.link_to_rx_gt(link)
            # Really, tx should be in routes dict, or else there is some disconnect regarding what link list was used
            if rx in routes_dict[tx]:
                # link is found
                routes_dict[tx].remove(rx) # This will cause problems if multiple links with same rx. Maybe I should add a validator for interfering links?
                good_links.append((tx, rx, link))
            else:
                # link not found
                not_found_links.append((tx, rx, link))

        # At this point, all expected link rxs should be removed
        for tx in routes_dict:
            if not is_yaml_none(tx):
                for rx in routes_dict[tx]:
                    unexp_links.append((tx, rx, "UNKNOWN"))

        # Sort entries of unexp_links by gt channel of tx, for easy parsing in results table
        unexp_links.sort(key=lambda x: self._sort_card_and_channel(x[0]))

        return {"Good" : good_links,
                "Unexpected" : unexp_links,
                "Not Found" : not_found_links
               }


    def display_results(self):
        pass


    def _sort_card_and_channel(self, gt_id):
        """
        Helper method to assist in ordering gt ids by card and channel.

        Used to help order unrecognized links nicely in the report table.
        """
        return MGTProps(gt_id=gt_id).get_order_index()


# Make mapping of strings to their associated actions
g_action_map = OrderedDict(
    [
        ("Initialize", Initialize),
        ("Initialize All", InitializeAll),
        ("Reset Errors", ResetErrors),
        ("Reset Errors All", ResetErrorsAll),
        ("Full Report", FullReport),
        ("Get Card Status", GetCardStatus),
        ("Get DCDC Status", GetDCDCStatus),
        ("Get GBT Status", GetGBTStatus),
        ("Check Init Done", CheckInitDone),
        ("Check RX Locked", CheckRXLocked),
        ("Check Common Status", CheckCommonStatus),
        ("Check RX Status", CheckRXStatus),
        ("Check TX Status", CheckTXStatus),
        ("Report Errors", GetErrors),
        ("Inject Errors", InjectErrors),
        ("Find Link Routes", FindLinkRoutes),
        ("Get GT Registers", GetAllGTValues),
        ("Collect Eye Diagrams", CollectEyeDiagrams),
        ("Get Optical Power", GetOpticalPower),
        ("Get All GT Values", GetAllGTValues),
    ]
)

