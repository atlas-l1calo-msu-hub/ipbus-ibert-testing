"""
Module defining MGTProps class, which is used to manage the various attributes
of an MGT. For example, given an MGT's card and GT channel, we may want to know
the MGT's quad number and channel, or the transceiver type.

The correspondence between GT channels and MGT Quad number and channel is
defined in this module.
"""

from yaml_functions import is_yaml_none

class MGTProps:
    """
    Class for managing properties of an MGT.

    Object initialized by providing some combination of properties which fully
    describe the MGT, and thereafter all other properties of the MGT will be
    derived.

    Args:
        device (str): card MGT is on (e.g. hub1, hub2, htm3, etc)
        channel (int): GT channel of MGT, OR quad channel (if quad is also to be supplied)
        quad (int): Quad number of MGT
        gt_id (str): format ["device.GT.ch#"]
        mgt_id (str): format ["device.Q#.ch#"]
    """
    def __init__(self, device=None, channel=None, quad=None, gt_id=None, mgt_id=None):

        # Determine which properties were supplied
        arg_mask = 0b00000
        if device is not None:
            arg_mask += 0b10000
        if channel is not None:
            arg_mask += 0b01000
        if quad is not None:
            arg_mask += 0b00100
        if gt_id is not None:
            arg_mask += 0b00010
        if mgt_id is not None:
            arg_mask += 0b00001

        # Make sure properties supplied fully describe an MGT
        if arg_mask not in (0b11000, 0b11100, 0b00010, 0b00001, 0):
            raise ValueError("Invalid combination of arguments supplied to MGTProps")

        # Derive gt_id and mgt_id from supplied properties
        if arg_mask == 0b11000:
            self.gt_id = "%s.GT.ch%s"%(device, channel)
            self.mgt_id = gt_to_mgt(self.gt_id)
        elif arg_mask == 0b11100:
            self.mgt_id = "%s.Q%s.ch%s"%(device, quad, channel)
            self.gt_id = mgt_to_gt(self.mgt_id)
        elif arg_mask == 0b00010:
            self.gt_id = gt_id
            self.mgt_id = gt_to_mgt(self.gt_id)
        elif arg_mask == 0b00001:
            self.mgt_id = mgt_id
            self.gt_id = mgt_to_gt(self.mgt_id)
        elif arg_mask == 0:
            self.mgt_id = "None"
            self.quad = "None"
            self.quad_channel = "None"
            self.gt_id = "None"
            self.gt_ch = "None"
            self.device = "None"
            self.device_type = "None"
            self.mgt_type = "None"
            return

        # Get all other properties from gt_id and mgt_id
        self._assign_from_gt_id()
        self._assign_from_mgt_id()
        self.mgt_type = get_transceiver_type(self.gt_id)

        # Get device type
        if self.device in ("hub1", "hub2"):
            self.device_type = "hub"
        elif self.device in ("htm3", "htm4", "htm5", "htm6", "htm7", "htm8", "htm9", "htm10", "htm11", "htm12", "htm13", "htm14"):
            self.device_type = "htm"
        elif self.device in ("rod1", "rod2"):
            self.device_type = "rod"
        else:
            self.device_type = "unknown"


    def _assign_from_mgt_id(self):
        mgt_id_split = self.mgt_id.split('.')
        self.device = mgt_id_split[0]
        self.quad = int(mgt_id_split[1].split('Q')[-1])
        self.quad_channel = int(mgt_id_split[-1].split('ch')[-1])


    def _assign_from_gt_id(self):
        gt_id_split = self.gt_id.split('.')
        self.device = gt_id_split[0]
        self.gt_ch = int(gt_id_split[-1].split('ch')[-1])

    def __str__(self):
        return "%s/%s"%(self.mgt_id, self.gt_id)

    def get_order_index(self):
        device_listing = ("hub1", "hub2", "rod1", "rod2", "htm3", "htm4",
            "htm5", "htm6", "htm7", "htm8", "htm9", "htm10", "htm11", "htm12",
            "htm13", "htm14"
        )
        device_offset = 100*device_listing.index(self.device)
        channel_offset = self.gt_ch
        return device_offset + channel_offset


def get_transceiver_type(gt_id):
    """
    Based on card and gt channel, return str identifying transciever type
    """
    gt_id_split = gt_id.split('.')
    card = gt_id_split[0]
    gt_ch = int(gt_id_split[-1].split('ch')[-1])

    if card in ( "htm3", "htm4", "htm5", "htm6", "htm7", "htm8", "htm9",
        "htm10", "htm11", "htm12", "htm13", "htm14", ):
        if 0 <= gt_ch <= 15:
            return "7 Series GTX"
        else:
            raise ValueError("Invalid channel number %s - Should be 0-15."%gt_ch)

    if card in ["hub1", "hub2"]:
        if 0 <= gt_ch <= 39:
            return "Ultrascale GTH"
        elif gt_ch <= 79:
            return "Ultrascale GTY"
        else:
            raise ValueError("Invalid channel number %s - Should be 0-79."%gt_ch)
    if card in ("rod1", "rod2"):
        if 0 <= gt_ch <= 79:
            return "7 Series GTH"
        else:
            raise ValueError("Invalid channel number %s - Should be 0-79."%gt_ch)


def gt_to_mgt(gt_id):
    if is_yaml_none(gt_id):
        return None

    gt_id = str(gt_id)
    if gt_id.count('.') != 2:
        raise ValueError("Invalid GT ID '%s'."%(gt_id) + 
                         "Should be of format '[card].GT.ch[#]'.")

    card, GT, gt_ch = gt_id.split('.')
    gt_ch = gt_ch.split('ch')[-1] # Remove 'ch', leave number

    if card in ("hub1", "hub2"):
        mgt_quad, quad_channel = hub_gt_to_mgt(gt_ch)
    elif card in ("htm3", "htm4", "htm5", "htm6", "htm7", "htm8", "htm9",
                  "htm10", "htm11", "htm12", "htm13", "htm14"):
        mgt_quad, quad_channel = htm_gt_to_mgt(gt_ch)
    elif card in ("rod1", "rod2"):
        mgt_quad, quad_channel = rod_gt_to_mgt(gt_ch)
    else:
        raise ValueError("Invalid card name '%s'."%(card))

    return ".".join((card, "Q%s"%(mgt_quad), "ch%s"%(quad_channel))) 


def htm_gt_to_mgt(gt_channel):
    gt_channel = int(gt_channel)
    if 0 <= gt_channel <= 15:
        mgt_quad_offset = 109
        gt_ch_offset = 0
    else:
        raise ValueError("HTM GT channel value is %s, should be in range 0-16"%(gt_channel))

    mgt_quad = mgt_quad_offset + (gt_channel - gt_ch_offset)//4
    quad_channel = (gt_channel - gt_ch_offset)%4
    return mgt_quad, quad_channel


def hub_gt_to_mgt(gt_channel):
    gt_channel = int(gt_channel)
    if 0 <= gt_channel <= 39:
        mgt_quad_offset = 224
        gt_ch_offset = 0
    elif 40 <= gt_channel <= 79:
        mgt_quad_offset = 124
        gt_ch_offset = 40
    else:
        raise ValueError("Hub GT channel value is %s, should be in range 0-79"%(gt_channel))

    mgt_quad = mgt_quad_offset + (gt_channel - gt_ch_offset)//4
    quad_channel = (gt_channel - gt_ch_offset)%4
    return mgt_quad, quad_channel


def rod_gt_to_mgt(gt_channel):
    gt_channel = int(gt_channel)
    if 0 <= gt_channel <= 39:
        mgt_quad_offset = 210
        gt_ch_offset = 0
    elif 40 <= gt_channel <= 79:
        mgt_quad_offset = 110
        gt_ch_offset = 40
    else:
        raise ValueError("Hub GT channel value is %s, should be in range 0-79"%(gt_channel))

    mgt_quad = mgt_quad_offset + (gt_channel - gt_ch_offset)//4
    quad_channel = (gt_channel - gt_ch_offset)%4
    return mgt_quad, quad_channel


def mgt_to_gt(mgt_id):
    if is_yaml_none(mgt_id):
        return None

    mgt_id = str(mgt_id)
    if mgt_id.count('.') != 2:
        raise ValueError(
            "Invalid MGT ID '%s'."%(mgt_id)
            + "Should be of format '[card].Q[#].ch[#]'."
        )

    card, mgt_quad, quad_channel = mgt_id.split('.')
    mgt_quad = mgt_quad.split("Q")[-1]
    quad_channel = quad_channel.split('ch')[-1] # Remove 'ch', leave number

    if card in ("hub1", "hub2"):
        gt_ch = hub_mgt_to_gt(mgt_quad, quad_channel)
    elif card in ("htm3", "htm4", "htm5", "htm6", "htm7", "htm8", "htm9",
                  "htm10", "htm11", "htm12", "htm13", "htm14"):
        gt_ch = htm_mgt_to_gt(mgt_quad, quad_channel)
    elif card in ("rod1", "rod2"):
        gt_ch = rod_mgt_to_gt(mgt_quad, quad_channel)
    else:
        raise ValueError("Invalid card name '%s'."%(card))

    return ".".join((card, "GT", "ch%s"%(gt_ch))) 


def htm_mgt_to_gt(mgt_quad, quad_channel):
    mgt_quad = int(mgt_quad)
    quad_channel = int(quad_channel)

    if 109 <= mgt_quad <= 112:
        mgt_quad_offset = 109
        gt_ch_offset = 0
    else:
        raise ValueError("MGT quad value is %s, should be in range 109-112"%(mgt_quad))

    if not (0 <= quad_channel <= 3):
        raise ValueError("Quad channel value is %s, should be in range 0-3"%(quad_channel))

    mgt_quad = int(mgt_quad)
    quad_channel = int(quad_channel)

    return (mgt_quad-mgt_quad_offset)*4 + quad_channel + gt_ch_offset


def hub_mgt_to_gt(mgt_quad, quad_channel):
    mgt_quad = int(mgt_quad)
    quad_channel = int(quad_channel)

    if 224 <= mgt_quad <= 233:
        mgt_quad_offset = 224
        gt_ch_offset = 0
    elif 124 <= mgt_quad <= 133:
        mgt_quad_offset = 124
        gt_ch_offset = 40
    else:
        raise ValueError("MGT quad value is %s, should be in range 124-133 or 224-233"%(mgt_quad))

    if not (0 <= quad_channel <= 3):
        raise ValueError("Quad channel value is %s, should be in range 0-3"%(quad_channel))

    mgt_quad = int(mgt_quad)
    quad_channel = int(quad_channel)

    return (mgt_quad-mgt_quad_offset)*4 + quad_channel + gt_ch_offset


def rod_mgt_to_gt(mgt_quad, quad_channel):
    mgt_quad = int(mgt_quad)
    quad_channel = int(quad_channel)

    if 210 <= mgt_quad <= 219:
        mgt_quad_offset = 210
        gt_ch_offset = 0
    elif 110 <= mgt_quad <= 119:
        mgt_quad_offset = 110
        gt_ch_offset = 40
    else:
        raise ValueError("MGT quad value is %s, should be in range 110-119 or 210-219"%(mgt_quad))

    if not (0 <= quad_channel <= 3):
        raise ValueError("Quad channel value is %s, should be in range 0-3"%(quad_channel))

    mgt_quad = int(mgt_quad)
    quad_channel = int(quad_channel)

    return (mgt_quad-mgt_quad_offset)*4 + quad_channel + gt_ch_offset

