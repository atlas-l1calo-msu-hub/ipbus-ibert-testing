"""
Module defining 'Log' class, which is used to help manage writing to both
stdout and a file at the same time.

The 'Log' class also contains features to manage verbosity settings and
timestamps in output.
"""

import time
import sys

class WriteEntry:
    def __init__(self, text, verbosity=0, match=False):
        """
        text: Text in write entry; i.e. the message to display
        verbosity: verbosity level of entry
        match: if True, only display message if verbosity is matched exactly
            (as opposed to greater than or equal to)
        """
        self.text = text
        self.verbosity = verbosity
        self.match = match

class Log:
    """
    Class for convenient logging information to a text file or to terminal
    output (or both), with automatic timestamping and verbosity.

    Attributes:
        log_name: name of log file to write to (example.txt)
        verbosity: verbosity of log output
        write_buffer: buffer to dump write entries to before writting to
            logfile.
        write_entries: list of write entries. Format of a write entry is a
            3-item tuple - text, verbosity, and match setting.
    """

    def __init__(self, log_name="", verbosity=0):

        self.log_name = log_name
        self.verbosity = verbosity
        self.write_buffer = ""
        self.write_entries = []


    def __copy__(self):
        """
        Method for returning a copy of Log instance.

        Simply creates another log with the same initial properties. This way,
        write_entries and write_buffer are not shared, and not interfered with.
        """
        return Log(self.log_name, self.verbosity)


    def write(self, text, verbosity=0, match=False):
        """
        Store text in write entries, to be cleared or flushed later.

        Args:
            text: text to store in buffer
            verbosity: verbosity level of written message
            match: if True, entry will only be displayed on flush if verbosity
                of log matches verbosity of entry exactly. If false, verbosity
                of log only need exceed verbosity of entry.
        """
        entry = WriteEntry(text, verbosity, match)
        self.write_entries.append(entry)

    def clear_write_entries(self):
        self.write_entries = []

    def clear_write_buffer(self):
        self.write_buffer = ""        

    def clear(self):
        self.clear_write_entries()
        self.clear_write_buffer()

    def flush(self, timestamped=False, verbosity=None, print_log=True,):
        """
        Output stored write entries to log (and stdout).

        Args:
            timestamped: whether to include a timestamp on the output text.
            verbosity: verbosity of output text (overrides verbosity of log
                instance)
            print_log: whether to output text to stdout as well as log file
        """

        if verbosity is None:
            verbosity = self.verbosity

        if not self.write_entries:
            self.clear()
            return

        for entry in self.write_entries:
            if entry.match:
                if verbosity == entry.verbosity:
                    self.write_buffer += entry.text
            elif verbosity >= entry.verbosity:
                self.write_buffer += entry.text

        if self.write_buffer == "":
            self.clear_write_entries()
            return

        if timestamped:
            write_str = ""
            timestamp_str = "[{}]: ".format(time.asctime()[4:])
            write_buffer_split = self.write_buffer.split("\n")
            write_str += timestamp_str
            indent = " "*len(timestamp_str)
            write_str += ("\n"+indent).join(self.write_buffer.split("\n"))
            # Remove lingering whitespace, but not newline
            write_str = write_str.strip(" ")
            self.write_buffer = write_str

        if self.log_name:
            log = open(self.log_name, "a+")
            log.write(self.write_buffer)
            log.close()

        if print_log == True:
            sys.stdout.write(self.write_buffer)
            sys.stdout.flush()

        self.clear()
