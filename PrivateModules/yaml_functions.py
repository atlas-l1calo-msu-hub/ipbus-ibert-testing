"""
Module defining functions used to help manage yaml configuration files.

The package 'ruamel_yaml' will be used if present, and is useful because of it's
dictionary order and comment preserving features.

If 'ruamel_yaml' package is not present, the standard library 'yaml' package
will be used instead. This will result in dictionaries in yaml files not being
stored in order (technically, the YAML specification states that dictionaries
in YAML are unordered), and comments will not be preserved.
"""

import os
IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")

# Will use ruamel_yaml if present, otherwise pyyaml
g_ruamel_yaml_present = True
try:
    from ruamel.yaml import YAML
    from ruamel.yaml.compat import StringIO
except ImportError:
    try:
        from ruamel_yaml import YAML
        from ruamel_yaml.compat import StringIO
    except ImportError:
        import yaml
        g_ruamel_yaml_present = False

if g_ruamel_yaml_present:
    g_yaml = YAML()
    g_yaml.preserve_quotes = True
    g_safe_yaml = YAML(typ='safe')


def is_yaml_none(item):
    """
    Check whether item is 'yaml'-None (when parsing yaml content, 'None' may be
    interpreted as the string, rather than Nonetype)
    """
    return ((item is None) or (item == 'None'))


def get_yaml(content, safe_load=True):
    """
    Get yaml-object (dictionary-like) corresponding to provided content.

    Arguments:
        content - yaml content to be obtained (yaml-str or filename-str)
    Return:
        yaml-object (dictionary-like) corresponding to provided content.
    """
    if g_ruamel_yaml_present:
        if safe_load:
            yaml_load = lambda x : g_safe_yaml.load(x)
        else:
            yaml_load = lambda x : g_yaml.load(x)
    else:
        if safe_load:
            yaml_load = lambda x : yaml.load(x, Loader=yaml.SafeLoader)
        else:
            yaml_load = lambda x : yaml.load(x, Loader=yaml.FullLoader)

    # Dictionary (yaml content already obtained - do nothing)
    if type(content) == dict:
        return content

    if os.path.isfile(content):
        with open(content, 'r') as stream:
            return yaml_load(stream)
    elif os.path.isfile(IBERTPATH + '/' + content):
        with open(IBERTPATH + '/' + content, 'r') as stream:
            return yaml_load(stream)
    else:
        yaml_dict = yaml_load(content)
        if not isinstance(yaml_dict, dict):
            raise ValueError("Provided content %s is not a file, or a yaml-dict-string"%content)
        return yaml_dict


def dump_yaml(yaml_content):
        if g_ruamel_yaml_present:
            yaml_iostr = StringIO()
            g_yaml.dump(yaml_content, yaml_iostr)
            return yaml_iostr.getvalue()
        else:
            return yaml.dump(yaml_content, sort_keys=False,
                             default_flow_style=False)

