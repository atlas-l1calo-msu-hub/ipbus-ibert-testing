"""
Module defining various helper functions to be used in the IPBus IBERT test.

The functions vary greatly in purpose. Some are for making tables, some are for
toggling registers high and low on several devices, some are for decoding
initialization values from a human-readable value to register-writeable value,
etc.
"""

import time
from gt_mgt_conversion import gt_to_mgt, MGTProps

def format_to_width(long_str, width, indent=0):
    """
    Format a long string to be spread across several lines, according to the
    width and indentation specified.
    """
    long_str_list = []
    while len(long_str) > width:
        cutoff = long_str[:width].rindex(" ")
        long_str_list.append(long_str[:cutoff].strip())
        long_str = long_str[cutoff:]
    long_str_list.append(long_str.strip())

    indent_str = " "*indent
    if long_str_list:
        return indent_str + ("\n"+indent_str).join(long_str_list) + "\n"
    else:
        return indent_str + long_str + "\n"


def blip_register_group(manager, gt_group, register, sleep_time=0.5):
    """
    Lower, raise, then lower a register or register.bitfield for a group of
    mgts (all together, to avoid long wait times for a large group of mgts).
    """
    for gt in gt_group:
        manager.devices[gt][register].lower_bits()
    time.sleep(sleep_time)
    for gt in gt_group:
        manager.devices[gt][register].raise_bits()
    time.sleep(sleep_time)
    for gt in gt_group:
        manager.devices[gt][register].lower_bits()
    time.sleep(sleep_time)


def get_all_gt_values(manager, gt_id):
    """
    For a given gt_id (along with a manager), return a dictionary of all the
    register/bitfield names and values for the GT channel.
    """
    gt_status_dict = OrderedDict()
    mgt = manager.devices[gt_id]
    for register in mgt:
        register_name = str(register).split('.')[-1]
        gt_status_dict[register_name] = register.read()
        for bitfield in register:
            bitfield_name = str(bitfield).split('.')[-1]
            gt_status_dict[bitfield_name] = bitfield.read()
    return gt_status_dict


def get_bad_mgts(results_dict, header_key, ideal_value=0b1, mask_dict=None):
    """
    Given a dual-layered results dict (as obtained via the
    get_results_dict_dual_layered method of an Action object), and the name of
    the header entry of the relevant parameter, get a list of mgts
    ('card.quad.quad_ch' format) which do not have the expected value.

    mask_dict is used for specifying which MGT types should have the parameter
    checked (not all MGT types may have the parameter actually implemented in
    the FW).
    """

    if mask_dict is None:
        mask_dict = {
            "Ultrascale GTH" : 0b1,
            "Ultrascale GTY" : 0b1,
            "7 Series GTX"   : 0b1,
            "7 Series GTH"   : 0b1,
        }

    bad_gt_list = []
    for gt_id in results_dict:
        if not gt_value_is_good(gt_id, results_dict, header_key, ideal_value,
                                mask_dict):
            bad_gt_list.append(gt_id)

    gt_ch_ordering = lambda x : MGTProps(gt_id=x).get_order_index()
    bad_gt_list.sort(key=gt_ch_ordering)
    bad_mgt_list = [gt_to_mgt(gt_id) for gt_id in bad_gt_list]

    return bad_mgt_list


def gt_value_is_good(gt_id, results_dict, header_key, ideal_value=0b1,
                     mask_dict=None):
    """
    Check whether a parameter's value is correct for a single MGT. Function to
    be used by get_bad_mgts function.
    """

    if mask_dict is None:
        mask_dict = {
            "Ultrascale GTH" : 0b1,
            "Ultrascale GTY" : 0b1,
            "7 Series GTX"   : 0b1,
            "7 Series GTH"   : 0b1,
        }

    value = int(results_dict[gt_id][header_key])
    mgt_type = MGTProps(gt_id=gt_id).mgt_type
    value_mask = mask_dict[mgt_type]
    if (value & value_mask) != (ideal_value & value_mask):
        return False
    else:
        return True


def decode(parameter, value, device_type):
    """
    Get bit pattern (to write to register) for a discrete value
    (e.g. prbs-select, DFE, etc.)

    Arguments:
        parameter - parameter to convert, as a string ('prbs-select', 'DFE', etc)
        value - desired value, as a string
        device_type - type of device to decode for ('Ultrascale GTH',
        'Ultrascale GTY', or '7 Series GTX')

    Return:
        Binary bit pattern to write to register
    """

    decode_dict = {
        "prbs-select" : {
            "PRBS-31" : {
                "Ultrascale GTH" : 0b101,
                "Ultrascale GTY" : 0b101,
                "7 Series GTX" : 0b100,
                "7 Series GTH" : 0b100,
            },

            "PRBS-23" : {
                "Ultrascale GTH" : 0b100,
                "Ultrascale GTY" : 0b100,
                "7 Series GTX" : 0b011,
                "7 Series GTH" : 0b011,
            },

            "PRBS-15" : {
                "Ultrascale GTH" : 0b011,
                "Ultrascale GTY" : 0b011,
                "7 Series GTX" : 0b010,
                "7 Series GTH" : 0b010,
            },

            # HTM doesn't have PRBS-9 Patterns
            #"PRBS-9" : {"Ultrascale GTH" : 0b010,
            #            "7 Series GTX" : "0xN",
            #           },

            "PRBS-7" : {
                "Ultrascale GTH" : 0b001,
                "Ultrascale GTY" : 0b001,
                "7 Series GTX" : 0b001,
                "7 Series GTH" : 0b001,
            },

            "OFF" : {
                "Ultrascale GTH" : 0b000,
                "Ultrascale GTY" : 0b000,
                "7 Series GTX" : 0b000,
                "7 Series GTH" : 0b000,
            },

        },

        "loopback" : {
            "Normal Operation" : {
                "Ultrascale GTH" : 0b000,
                "Ultrascale GTY" : 0b000,
                "7 Series GTX" : 0b000,
                "7 Series GTH" : 0b000,
            },

            "Near-End PCS" : {
                "Ultrascale GTH" : 0b001,
                "Ultrascale GTY" : 0b001,
                "7 Series GTX" : 0b001,
                "7 Series GTH" : 0b001,
            },

            "Near-End PMA" : {
                "Ultrascale GTH" : 0b010,
                "Ultrascale GTY" : 0b010,
                "7 Series GTX" : 0b010,
                "7 Series GTH" : 0b010,
            },

            "Far-End PMA" : {
                "Ultrascale GTH" : 0b100,
                "Ultrascale GTY" : 0b100,
                "7 Series GTX" : 0b100,
                "7 Series GTH" : 0b100,
            },

            "Far-End PCS" : {
                "Ultrascale GTH" : 0b110,
                "Ultrascale GTY" : 0b110,
                "7 Series GTX" : 0b110,
                "7 Series GTH" : 0b110,
            },

        },

        "DFE" : {
            "ON" : {
                "Ultrascale GTH" : 0b0,
                "Ultrascale GTY" : 0b0,
                "7 Series GTX" : 0b0,
                "7 Series GTH" : 0b0,
            },

            "OFF" : {
                "Ultrascale GTH" : 0b1,
                "Ultrascale GTY" : 0b1,
                "7 Series GTX" : 0b1,
                "7 Series GTH" : 0b1,
            },

        },

    }

    if parameter == "swing":

        if device_type == "7 Series GTX":
            value_range = [269, 336, 407, 474, 543, 609, 677, 741, 807, 
                           866, 924, 973, 1018, 1056, 1092, 1119]

        elif device_type == "7 Series GTH":
            value_range = [269, 336, 407, 474, 543, 609, 677, 741, 807,
                           866, 924, 973, 1018, 1056, 1092, 1119]

        elif device_type == "Ultrascale GTH":
            value_range = [170, 250, 320, 390, 460, 530, 600, 660, 730,
                           780, 840, 900, 950, 1000, 1040, 1080]
            
        elif device_type == "Ultrascale GTY":
            value_range = [191, 223, 254, 286, 315, 347, 378, 408, 439,
                           470, 499, 529, 556, 585, 613, 640, 669, 695,
                           720, 744, 766, 788, 809, 828, 846, 863, 878,
                           892, 903, 914, 924, 933]

        else:
            raise ValueError("Invalid device type: %s"%device_type)

        return decode_range(value, value_range, "int")

    if parameter == "pre-emphasis":

        if ((device_type == "7 Series GTX")
         or (device_type == "7 Series GTH")
         or (device_type == "Ultrascale GTH")
         or (device_type == "Ultrascale GTY")):

            value_range = [0, 0.22, 0.45, 0.68, 0.92, 1.16, 1.41, 1.67,
                           1.94, 2.21, 2.50, 2.79, 3.10, 3.41, 3.74, 4.08,
                           4.44, 4.81, 5.19, 5.60, 6.02, 6.02, 6.02, 6.02,
                           6.02, 6.02, 6.02, 6.02, 6.02, 6.02, 6.02, 6.02]

        else: 
            raise ValueError("Invalid device type: %s"%device_type)

        return decode_range(value, value_range, "float")
        
    if parameter == "post-emphasis":

        if ((device_type == "7 Series GTX")
         or (device_type == "7 Series GTH")
         or (device_type == "Ultrascale GTH")
         or (device_type == "Ultrascale GTY")):

            value_range = [0, 0.22, 0.45, 0.68, 0.92, 1.16, 1.41, 1.67, 1.94,
                           2.21, 2.50, 2.79, 3.10, 3.41, 3.74, 4.08, 4.44,
                           4.81, 5.19, 5.60, 6.02, 6.47, 6.94, 7.43, 7.96,
                           8.52, 9.12, 9.76, 10.46, 11.21, 12.04, 12.96]

        else: 
            raise ValueError("Invalid device type: %s"%device_type)

        return decode_range(value, value_range, "float")

    if parameter in decode_dict:
        if value in decode_dict[parameter]:
            return decode_dict[parameter][value][device_type]

        else:
            raise ValueError("Invalid value '%s' for parameter '%s'\n"%(value, parameter))

    else:
        raise ValueError("Parameter '%s' not recognized\n"%(parameter))
    

def decode_range(value, value_range, value_type):
    """
    Get bit pattern (to write to register) for a non-discrete value
    (e.g. pre/post-emphasis, swing)

    Arguments:
        value - desired value, as an int or float
        value_range - ordered list of writeable values
        value_type - type of 'value' argument ('int' or 'float')

    Return:
        Binary bit pattern to write to register
    """

    # For decoding pre/post emphasis, swing, etc
    # Things where the actual discrete values are not so meaningful
    if value_type == "int":
        value = int(value)
    elif value_type == "float":
        value = float(value)
    else:
        raise TypeError("Invaid decode value type '%s'"%(value_type))

    no_of_values = len(value_range)
    i = 1
    while i < no_of_values:
        if value < ((value_range[i] + value_range[i-1])/2):
            return i-1
        i += 1

    return i-1

def make_table(data, headers=None):
    """
    Given data

    Args:
        data: data to make table from. Should be an NxN double-list, or a double-dict (if headers=="keys")
        headers: optional. What to use as headers for the table. If "firstrow",
            first row is used (assumes data is NxN double-list). If "keys",
            uses keys of dictionary (assumes data is double-dict)
    """
    # If data is dictionary, convert to NxN matrix/2-Dlist
    if headers == "firstrow":
        data = [[str(entry) for entry in row] for row in data]
        headers = data[0]
    elif headers == "keys":
        headers = list(data.keys())
        data_table = []
        data_table.append(headers)
        # Assumes data dict is non-empty, well-formatted (can be converted to NxN matrix)
        for i in range(len(data[headers[0]])):
            data_table.append([data[header][i] for header in headers])
        data = [[str(entry) for entry in row] for row in data]
        data = data_table
    elif headers is None:
        data = [[str(entry) for entry in row] for row in data]

    table_width = len(data[0])
    max_widths = [0]*table_width
    for row in data:
        for i, entry in enumerate(row):
            if len(entry) > max_widths[i]:
                max_widths[i] = len(entry)
    # Get "| {} | {} | {} | ..."
    row_fmt_str = "| {} |\n".format(" | ".join(["{}"]*table_width))
    # Change {} to {:'max_width's}
    row_fmt_str = row_fmt_str.format(
        *["{{:{width}s}}".format(width=width) for width in max_widths]
    )
    row_divider_str = row_fmt_str.format(*[""]*table_width)
    row_divider_str = row_divider_str.replace(" ", "-")
    row_divider_str = row_divider_str.replace("|", "+")
    header_divider_str = row_divider_str.replace("-", "=")
    table_str = ""
    table_str += row_divider_str
    if headers is not None:
        table_str += row_fmt_str.format(*headers)
        table_str += header_divider_str
        data = data[1:]
    for row in data:
        table_str += row_fmt_str.format(*row)
        table_str += row_divider_str
    return table_str
