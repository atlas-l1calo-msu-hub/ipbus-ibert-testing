#!/usr/bin/env python
import os
import sys
import pdb
IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
LINKCONFIGPATH = IBERTPATH + "/LinkConfiguration/"
MGTDEFPATH = LINKCONFIGPATH + "/MGTDefinitions/"
SUMMARYPATH = IBERTPATH + "SummaryGeneration"
sys.path.append(IBERTPATH+'/PrivateModules/')

import numpy as np
from collections import OrderedDict
from copy import deepcopy
import itertools
import linecache

from pylatex import Document, Section, Subsection, Tabular, Math, TikZ, Axis, Plot, Figure, Matrix, Alignat, NoEscape, Itemize, Command
from pylatex import LongTable, MultiColumn
from pylatex.base_classes import Environment, Arguments
from pylatex import Package
from pylatex import Tabularx
from pylatex.utils import italic

import matplotlib as mpl
try:
    os.environ["DISPLAY"]
except KeyError:
    mpl.use("Agg")
import matplotlib.pyplot as plt

import datetime

from cmd_line_args_class import ArgGetter

from yaml_functions import get_yaml
from link_and_gt_conf_classes import LinkConfig, GTConfig
from ibert_helper_functions import get_bad_mgts
from gt_mgt_conversion import MGTProps

def show_usage():
    print("""
Usage: python generate_summary_pdf.py -i [input_fname] (-t [title]) (-m [message])

                                    -h|--help                    
                                    -i|--unput
                                    -t|--title
                                    -m|--message

Script for generating a PDF summary from the raw output csv of an IPBus IBERT
test. Sections are generated according to the actions present in a test. That
is, there are no requirements on what actions must be performed in a test, the
report will include or omit that section of the report based on whether or not
the data is found. In the case of duplicate actions, a report is typically only
generated for the last action performed. There are some exceptions, including
the FullReport section, which counts on there being at least 2 FullReport
actions.

Options:

-h, --help                          Display this message and exit
-i, --input                         Specify input file (csv)
-t, --title                         Optional title (if not provided, title will
                                    be determined from input filename)
-m, --message                       Optional message to include in summary
""")
    sys.exit()


def main(input_fname, title=None, message=None):
    # Setup
    geometry_options = {"right": "2cm", "left": "2cm", "top": "2cm", "bottom": "2cm", "landscape": True}
    doc = Document(geometry_options=geometry_options)

    if title is None:
        title = input_fname.split("/")[-1].replace("_raw.csv", "")

    # Making paths global, so I don't have to pass them as arguments.
    # Actually, may be best to pass title as an argument, and create path from that.

    RESULTSPATH = "/".join([IBERTPATH, "SummaryGeneration", "Results"])
    if not os.path.exists(RESULTSPATH):
        os.makedirs(RESULTSPATH)
    PROJECTPATH = "/".join([RESULTSPATH, title])
    FIGUREPATH = "/".join([PROJECTPATH, "Figures"])

    project_dir_paths = [PROJECTPATH, FIGUREPATH]
    for dir_path in project_dir_paths:
        if not os.path.isdir(dir_path):
            os.makedirs(dir_path)


    link_data = DataSection(input_fname, "Beginning of Link List",
                           "End of Link List", index=-1).get_results_dual_list()

    # Keep in mind this - should make sure I'm checking the right links
    full_link_list = [row[0] for row in link_data]
    test_conf = {"link_list": full_link_list}
    link_conf = LinkConfig(used_links_only=True, config=test_conf)
    gt_conf = GTConfig(link_conf)
    group_link_dict = get_group_link_dict(full_link_list)


    sections = []
    SectionIntro(doc, input_fname, title, message)
    sections.append(SectionCardStatus(doc, input_fname))
    SectionInitSettings(doc, input_fname)
    sections.append(SectionFullReport(doc, input_fname, group_link_dict, full_link_list))
    sections.append(SectionCommonStatusCheck(doc, input_fname, gt_conf))
    sections.append(SectionTXStatusCheck(doc, input_fname, gt_conf))
    sections.append(SectionRXStatusCheck(doc, input_fname, gt_conf))
    sections.append(SectionOpticalPower(doc, input_fname, FIGUREPATH))
    sections.append(SectionEyeDiagrams(doc, input_fname, link_conf, group_link_dict, full_link_list, FIGUREPATH))

    # Add Final Report
    SectionFinalSummary(doc, sections)

    DOCUMENTPATH = "/".join([PROJECTPATH, title])
    doc.generate_pdf(DOCUMENTPATH, clean_tex=False)
    return



def get_line(data_fname, line_no):
    """
    Get line from a file using linecache module. The purpose of this function
    is simply to use zero-based-indexing, which linecache does not do.
    """
    return linecache.getline(data_fname, line_no+1)


def process_csv_line(line):
    """
    Process line from a raw csv.

    Removes unnecessary whitespace, replaces '$' with ',' (method used to allow
    commas to be included in csv data).
    """
    return [entry.strip().replace("$", ",") for entry in line.strip().split(",")]


def get_csv_row(data_fname, row_no):
    """
    Get line from a csv file as a list
    """
    return process_csv_line(get_line(data_fname, row_no))


class DataSection:
    """
    Class for managing the acquisition of sections of data from the data input.

    Attributes:
        data_fname: filename of input data
        start_id: expected first column of first row of desired data section
        end_id: expected first column of last row of desired data section
        opt_ids: expected subsequent columns of first and last row of desired
            data section
        index: which data section to use from mathing sections in data input
            (-1 for last / most recent)
        data_found: whether desired data section was found in file
        start_indices: indices of start rows for each matching data section
        end_indices: indices of end rows for each matching data section
        results_storage_list: list of dictionaries which describe start/end
            index and dual-list/dual-dict representations of results for each
            matching data section from data input. Ordered according to order
            of appearance in input csv.
    """
    def __init__(self, data_fname, start_id, end_id, opt_ids=[], index=-1):
        self.data_fname = data_fname
        self.start_id = start_id
        self.end_id = end_id
        self.opt_ids = opt_ids
        self.index = index
        self.data_found = False
        self.results_storage_list = []
        self._setup_results_indices()


    def __bool__(self):
        return self.data_found

    def __iter__(self):
        for i in range(len(self.start_indices)):
            yield i
        

    def _setup_results_indices(self):
        self.start_indices = get_matching_row_indices(
            self.data_fname,
            self.start_id,
            *self.opt_ids
        )
        self.end_indices = get_matching_row_indices(
            self.data_fname,
            self.end_id,
            *self.opt_ids
        )

        # If action data is not found, nothing to do
        if not (self.start_indices and self.end_indices):
            return

        self.data_found = True

        for i in range(len(self.start_indices)):
            self.results_storage_list.append(
                {
                    "start index" : self.start_indices[i],
                    "end index"   : self.end_indices[i],
                    "dual list"   : None,
                    "dual dict"   : None,
                }
            )


    def get_results_dual_list(self, index = None):
        """
        Get data section results (not header/footer) as a double-list.
        """
        if index is None:
            index = self.index
        if self.results_storage_list[index]["dual list"] is not None:
            return self.results_storage_list[index]["dual list"]

        start_index = self.results_storage_list[index]["start index"]
        end_index = self.results_storage_list[index]["end index"]
        results_dual_list = []
        with open(self.data_fname) as raw_data:
            # From the full raw data, get just the relevant action data
            # Do not include the header and footer
            for line in itertools.islice(raw_data, start_index+1, end_index):
                row = process_csv_line(line)
                results_dual_list.append(row)
        self.results_storage_list[index]["dual list"] = results_dual_list
        return self.results_storage_list[index]["dual list"]


    def get_results_dual_dict(self, index = None):
        """
        Get data section results (not header/footer) as a double-dict.
        """
        if index is None:
            index = self.index

        if self.results_storage_list[index]["dual dict"] is not None:
            return self.results_storage_list[index]["dual dict"]

        results_dual_list = self.get_results_dual_list()
        if len(results_dual_list) < 2:
            raise ValueError("Cannot create results dual dict from results double list with fewer than 2 rows")

        header_row = results_dual_list[0]
        data_rows = results_dual_list[1:]

        results_dual_dict = OrderedDict()
        for row in data_rows:
            results_dual_dict[row[0]] = OrderedDict()
            for i, header in enumerate(header_row):
                results_dual_dict[row[0]][header] = row[i]
        self.results_storage_list[index]["dual dict"] = results_dual_dict
        return self.results_storage_list[index]["dual dict"]


    def get_header(self, index = None):
        """
        Get header (first row) of data section.
        """
        if index is None:
            index = self.index
        return get_csv_row(
            self.data_fname,
            self.results_storage_list[index]["start index"]
        )


    def get_footer(self, index = None):
        """
        Get header (last row) of data section.
        """
        if index is None:
            index = self.index
        return get_csv_row(
            self.data_fname,
            self.results_storage_list[index]["end index"]
        )


def get_matching_row_indices(data_fname, *entries):
    """
    Given an input data file, get the indices of all rows which match entries.

    Args:
        data_fname: filename of data input
        *entries: expected column entries of row. None functions as wildcard.
    """
    row_indices = []
    with open(data_fname) as data:
        for i, line in enumerate(data):
            row = process_csv_line(line)
            row_matches = True
            for j, entry in enumerate(entries):
                # Treat None as wildcard
                try:
                    if entry is None or entry == row[j]:
                        continue
                    else:
                        row_matches = False
                except IndexError:
                    row_matches = False
                    break
            if row_matches:
                row_indices.append(i)
    return row_indices


class ActionEntry(DataSection):
    """
    Subclass of DataSection, specifically for IPBus IBERT Action data sections.

    No meaningful changes in functionality, but fewer construction arguments to
    provide.
    """
    def __init__(self, data_fname, action_name, index=-1):
        DataSection.__init__(self, data_fname, "Beginning of Action Results",
                             "End of Action Results", [action_name], index=index)



def get_group_link_dict(link_list):
    """
    Given list of links, return dictionary organizing them into several lists,
    categorized by functionality.
    """
    sorted_link_dict = OrderedDict([ ("Other Links", []) ])

    relevant_definition_files = OrderedDict(
        [
            ("Hub+ROD Combined+Readout Data", ["hub_combined_data.yaml",
                                               "hub_readout_data.yaml",
                                               "ROD_readout.yaml"]),
            ("HTM FEX Data to Hub1", ["htm_FEX_data_to_hub1.yaml"]),
            ("HTM FEX Data to Hub2", ["htm_FEX_data_to_hub2.yaml"]),
            ("HTM FEX Data to ROD1", ["htm_FEX_data_to_ROD1.yaml"]),
            ("HTM FEX Data to ROD2", ["htm_FEX_data_to_ROD2.yaml"]),
            ("Optical Links", ["optical_links.yaml"]),
        ]
    )

    for pretty_name, filename_list in relevant_definition_files.items():
        relevant_link_definitions = {}
        for filename in filename_list:
            relevant_link_definitions.update(get_yaml(MGTDEFPATH+filename))
        relevant_links = [x for x in link_list if x in relevant_link_definitions]
        relevant_links = links_sorted_by_link_ordering(relevant_links)
        sorted_link_dict[pretty_name] = relevant_links

    for link in link_list:
        link_organized = False
        for sorted_link_list in sorted_link_dict.values():
            if link in sorted_link_list:
                link_organized = True
        if not link_organized:
            sorted_link_dict["Other Links"].append(link)

    return sorted_link_dict


def links_sorted_by_link_ordering(link_list):
    """
    Given a list of links, return that list of links, sorted by order in
    IBERTPATH/LinkConfiguration/link_ordering.yaml. Links not included in that
    file are moved to the end, but otherwise kept in order.
    """

    link_ordering = get_yaml(LINKCONFIGPATH+"/link_ordering.yaml")
    links_in_ordering = [x for x in link_list if x in link_ordering]
    links_not_in_ordering = [x for x in link_list if not x in link_ordering]

    links_in_ordering.sort(key=lambda x : link_ordering.index(x))
    return links_in_ordering + links_not_in_ordering


def convert_results_to_dual_dict(results_data):
    results_dict = OrderedDict()
    header = results_data[0]
    for row in results_data[1:]:
        gt_id = row[0]
        results_dict[gt_id] = {}
        gt_dict = results_dict[gt_id]
        for i, value in enumerate(row):
            gt_dict[header[i]] = value

    return results_dict


def convert_input_csv_to_double_list(filename):
    f = open(filename, "r")
    csv_str = f.read()
    csv_rows = csv_str.split("\n")
    return [[val.strip() for val in row.split(",")] for row in csv_rows]





def convert_to_datetime(day_month_year, hour_minute):
    day, month, year = [int(x) for x in day_month_year.split("/")]
    # Make date 
    year = year+2000 if year < 100 else year
    hour, minute = [int(x) for x in hour_minute.split(":")]
    return datetime.datetime(year=year, month=month, day=day, hour=hour,
                             minute=minute)


class SectionIntro:
    section_name = "Introduction"
    def __init__(self, doc, data_fname, title, message=None):
        preamble_title = "IBERT Results Summary: %s"%title
        doc.preamble.append(Command('title', preamble_title))
        doc.append(NoEscape(r'\maketitle'))

        start_test_data = DataSection(data_fname, "Start of IBERT test", "End of IBERT test", index=0)
        start_row = start_test_data.get_header()
        start_datetime = convert_to_datetime(start_row[1], start_row[2])
        doc.append(NoEscape(r'\noindent')) # Don't indent first paragraph
        doc.append("Summary for IBERT test starting on %s, at time %s.\n\n"%(
            start_datetime.strftime("%B %d, %Y"), start_datetime.strftime("%H:%M")
            )
        )

        if message is not None:
            doc.append("User message from PDF generation: %s\n\n"%(message))

        user_message_data = DataSection(data_fname, "User Message", "User Message", index=0)
        if user_message_data:
            user_message_row = user_message_data.get_header() 
            user_message = user_message_row[1]
            doc.append("User message from IBERT test run: %s\n\n"%(user_message))

        doc.append(NoEscape(r'\tableofcontents'))


class SectionInitSettings:
    section_name = "Initialization Settings Used"
    def __init__(self, doc, data_fname):
        self.doc = doc
        self.data_fname = data_fname
        self.data = DataSection(self.data_fname, "Beginning of Initialization Parameters", "End of Initialization Parameters")

        if not self.data:
            return

        with self.doc.create(Section(self.section_name)):
            self.doc.append("The following initialization settings were used for this IBERT test:\n")
            add_table(self.doc, self.data.get_results_dual_list())



class ActionSection:
    """
    Base class for creating section of the LaTeX document corresponding to an
    IPBus IBERT action.

    Attributes:
        doc: latex document to be written to
        data_fname: filename of input data
        action_name: name of action to create section for
        section_name: name of section to create
        index: index of action in data to use (-1 for last action)
        action: ActionEntry object for the action corresponding to this section
        problem_links: list of links or MGTs that are problematic in some way
            (used in final summary section)
    """
    section_name = "Dummy Section Name"
    action_name = "Dummy Action Name"

    def __init__(self, doc, data_fname, index=-1):
        self.doc = doc
        self.data_fname = data_fname
        self.problem_links = []
        self.action = ActionEntry(
            self.data_fname,
            self.action_name,
            index=index
        )

    def __bool__(self):
        """
        True if action found in data input, False if not.
        """
        return bool(self.action)



class SectionCardStatus(ActionSection):
    section_name = "Card Status"
    action_name = "Get Card Status"
    def __init__(self, doc, data_fname, index=-1):
        ActionSection.__init__(self, doc, data_fname, index=index)

        if not self.action:
            return

        with self.doc.create(Section("Cards Used")):
            card_status_hub_htm = self.get_card_status_hub_htm()
            if card_status_hub_htm:
                self.doc.append("The following Hub and HTM cards were used in this IBERT test:\n")
                self.doc.append(NoEscape(r"\footnotesize"))
                add_table(self.doc, card_status_hub_htm)
                self.doc.append(NoEscape(r"\normalsize"))
                self.doc.append("\n\n")

            card_status_rod = self.get_card_status_rod()
            if card_status_rod:
                self.doc.append("The following ROD cards were used in this IBERT test:\n")
                self.doc.append(NoEscape(r"\footnotesize"))
                add_table(self.doc, card_status_rod)
                self.doc.append(NoEscape(r"\normalsize"))
                self.doc.append("\n\n")


    def get_card_status_hub_htm(self):
        results_dual_list = self.action.get_results_dual_list()
        card_statuses = []
        # Add header for Hub/HTM section
        card_statuses += results_dual_list[0:1]
        hub_htm_tup = ("hub1", "hub2", "htm3", "htm4", "htm5", "htm6", "htm7",
                       "htm8", "htm9", "htm10", "htm11", "htm12", "htm13",
                       "htm14")
        card_statuses += [row for row in results_dual_list
                          if row and (row[0] in hub_htm_tup)]
        return card_statuses


    def get_card_status_rod(self):
        results_dual_list = self.action.get_results_dual_list()
        card_statuses = []
        # Add header for ROD section (skip first entry, header for hub/htms)
        card_statuses += [row for row in results_dual_list[1:]
                          if row and (row[0] == "Card")]
        rod_tup = ("rod1", "rod2")
        card_statuses += [row for row in results_dual_list
                          if row and (row[0] in rod_tup)]
        return card_statuses


class SectionFullReport(ActionSection):
    section_name = "Full Report Data"
    action_name = "Full Report"
    def __init__(self, doc, data_fname, group_link_dict, full_link_list, index=-1):
        ActionSection.__init__(self, doc, data_fname, index=index)
        self.group_link_dict = group_link_dict
        # If no full report data, nothing to do
        if not self.action:
            return

        self.present_link_list = [link for link in full_link_list
                                  if link in self.action.get_results_dual_dict()]

        self.short_full_report_data = self.get_short_full_report_data()

        start_footer = self.action.get_footer(index=0)
        start_dt = convert_to_datetime(start_footer[2], start_footer[3])
        end_header = self.action.get_header(index=-1)
        end_dt = convert_to_datetime(end_header[2], end_header[3])
        duration = end_dt - start_dt
        full_duration_seconds = float(duration.days*86400 + duration.seconds)

        with doc.create(Section(self.section_name)):
            doc.append("Final full report data collected for IBERT test, starting on date %s, at time %s. Time elapsed from end of first full report: %s days, %s hours. "%
                (
                    start_dt.strftime("%B %d, %Y"),
                    start_dt.strftime("%H:%M"),
                    "%s"%(duration.days),
                    "%.1f"%(float(duration.seconds/3600.0)),
                )
            )
            doc.append("The total number of bits sent over this time period is: at %.1f Gbps, %.1E; %.1f Gbps, %.1E; %.1f Gbps, %.1E."%
                (
                    4.8,
                    1e9*4.8*full_duration_seconds,
                    6.4,
                    1e9*6.4*full_duration_seconds,
                    10.8,
                    1e9*10.8*full_duration_seconds,
                )
            )
            self.output_tables()


    def output_tables(self):
        for pretty_name, link_list in self.group_link_dict.items():
            if link_list or (pretty_name != "Other Links"):
                relevant_data = [row for row in self.short_full_report_data
                                 if row[0] in link_list and row[0] in self.present_link_list]
                if relevant_data:
                    headers = self.short_full_report_data[0]
                    relevant_data.insert(0, headers)
                    color_list = get_full_report_colors(relevant_data)
                    add_longtable(self.doc, relevant_data, title=pretty_name, color_list=color_list)
                    for i, color in enumerate(color_list):
                        if color is not None:
                            self.problem_links.append(link_list[i])


    def get_short_full_report_data(self):
        """
        Convert results data from full report to include less entries (in order to
        make smaller, and fit the page width)
        """
        data = self.action.get_results_dual_list()
        header_row = data[0]
        rel_headers = ("Link Name", "Linked", "Source", "Dest", "Errors", "Statuses Good")
        rel_indices = [header_row.index(header) for header in rel_headers]
        return [[row[i] for i in rel_indices] for row in data]


def get_full_report_colors(data, first_row_is_header=True, headers=None):
    if first_row_is_header:
        headers = data[0]
        data = data[1:]
    colors_list = []
    severity_dict = {
        0 : None,
        1 : "yellow",
        2 : "orange",
        3 : "red",
    }

    for row in data:
        severity = 0
        if row[headers.index("Statuses Good")] != "Yes":
            severity = 1
        if row[headers.index("Errors")] != "0":
            severity = 2
        if row[headers.index("Linked")] != "Yes":
            severity = 3
        colors_list.append(severity_dict[severity])

    return colors_list


class SectionStatusCheckBase(ActionSection):
    section_name = "Dummy Status Check Section Name"
    action_name = "Dummy Status Check Action Name"

    param_list = []
    mgt_text = "Generic MGT(s)"

    check_ultrascale_dict = {
        "init_done"           : True ,
        "gtpowergood"         : True ,
        "qplllock"            : True,
        "mgt_cdr_locked"      : True ,
        "rxpmaresetdone"      : True ,
        "reset_rx_done"       : True ,
        "buffbypass_rx_done"  : True ,
        "buffbypass_rx_error" : True ,
        "rxprbserr_flg"       : True ,
        "rxprbslocked"        : True ,
        "rxphaligndone"       : True ,
        "rxphalignerr"        : True ,
        "rxsyncdone"          : True ,
        "init_done"           : True ,
        "gtpowergood"         : True ,
        "txpmaresetdone"      : True ,
        "reset_tx_done"       : True ,
        "buffbypass_tx_done"  : True ,
        "buffbypass_tx_error" : True ,
        "txdlyresetdone"      : True ,
        "txphaligndone"       : True ,
        "txphaligninitdone"   : True ,
        "txsyncdone"          : True ,
        "txsyncout"           : True ,
    }

    check_7_series_dict = {
        "init_done"           : True ,
        "gtpowergood"         : False,
        "qplllock"            : True ,
        "mgt_cdr_locked"      : False,
        "rxpmaresetdone"      : False,
        "reset_rx_done"       : True ,
        "buffbypass_rx_done"  : False,
        "buffbypass_rx_error" : False,
        "rxprbserr_flg"       : True ,
        "rxprbslocked"        : False,
        "rxphaligndone"       : False,
        "rxphalignerr"        : False,
        "rxsyncdone"          : False,
        "txpmaresetdone"      : False,
        "reset_tx_done"       : True ,
        "buffbypass_tx_done"  : False,
        "buffbypass_tx_error" : False,
        "txdlyresetdone"      : False,
        "txphaligndone"       : False,
        "txphaligninitdone"   : False,
        "txsyncdone"          : False,
        "txsyncout"           : False,
    }

    check_bit_high_dict = {
        "init_done"           : True ,
        "gtpowergood"         : True ,
        "qplllock"            : True ,
        "mgt_cdr_locked"      : True ,
        "rxpmaresetdone"      : True ,
        "reset_rx_done"       : True ,
        "buffbypass_rx_done"  : True ,
        "buffbypass_rx_error" : False,
        "rxprbserr_flg"       : False,
        "rxprbslocked"        : True ,
        "rxphaligndone"       : True ,
        "rxphalignerr"        : False,
        "rxsyncdone"          : True ,
        "init_done"           : True ,
        "gtpowergood"         : True ,
        "txpmaresetdone"      : True ,
        "reset_tx_done"       : True ,
        "buffbypass_tx_done"  : True ,
        "buffbypass_tx_error" : False,
        "txdlyresetdone"      : True ,
        "txphaligndone"       : True ,
        "txphaligninitdone"   : True ,
        "txsyncdone"          : True ,
        "txsyncout"           : True ,
    }

    def __init__(self, doc, data_fname, gt_conf, index=-1):
        ActionSection.__init__(self, doc, data_fname, index=index)
        self.gt_conf = gt_conf

        # If no action data, nothing to do
        if not self.action:
            return

        self._setup_mgt_list()


        with doc.create(Section(self.section_name)):
            self.check_statuses()


    def check_statuses(self):
        for parameter in self.param_list:
            check_ultrascale = self.check_ultrascale_dict[parameter]
            check_7_series = self.check_7_series_dict[parameter]
            check_bit_high = self.check_bit_high_dict[parameter]
            if check_ultrascale and check_7_series:
                mgt_text = self.mgt_text
            elif check_ultrascale:
                mgt_text = "Ultrascale " + self.mgt_text
            elif check_7_series:
                mgt_text = "7-Series " + self.mgt_text

            mgt_list = self.mgt_list
            bad_mgts = self.check_status_bit(
                parameter,
                check_bit_high=check_bit_high,
                ultrascale=check_ultrascale,
                series_7=check_7_series,
                included_mgts=mgt_list,
            )
            self.display_status_results(
                bad_mgts, parameter,
                check_bit_high=check_bit_high,
                mgt_text=mgt_text,
            )
            self.problem_links += bad_mgts


    def check_status_bit(self, header_key, check_bit_high=True,
                         ultrascale=False, series_7=False, included_mgts=None):
        """
        Method for checking whether a bit is high or low (used for status checks).
        """
        ideal_value = 0b1 if check_bit_high else 0b0
        bad_mgt_list = get_bad_mgts(
            self.action.get_results_dual_dict(),
            header_key = header_key,
            ideal_value= ideal_value,
            mask_dict = {
                "Ultrascale GTH" : 0b1 if ultrascale else 0b0,
                "Ultrascale GTY" : 0b1 if ultrascale else 0b0,
                "7 Series GTX"   : 0b1 if series_7 else 0b0,
                "7 Series GTH"   : 0b1 if series_7 else 0b0,
            }
        )
        if included_mgts is not None:
            bad_mgt_list = [x for x in bad_mgt_list if x in included_mgts]

        return bad_mgt_list


    def display_status_results(self, bad_mgts, parameter, check_bit_high=True,
                               mgt_text="MGTs"):

        bad_value_str = "low" if (check_bit_high) else "high"
        good_value_str = "high" if (bad_value_str == "low") else "low"

        if bad_mgts:
            self.doc.append("WARNING - %s bit is %s on %s in-use %s:\n"%
                (parameter, bad_value_str, len(bad_mgts), mgt_text)
            )
            with self.doc.create(Itemize()) as itemize:
                for mgt in bad_mgts:
                    itemize.add_item(mgt)

        else:
            self.doc.append("GOOD - %s bit is %s on all in-use %s.\n"%
                (parameter, good_value_str, mgt_text)
            )


    def _setup_mgt_list(self):
        self.mgt_list = [gt for gt in self.gt_conf]


class SectionCommonStatusCheck(SectionStatusCheckBase):
    section_name = "Common MGT Status Bit Checks"
    action_name = "Check Common Status"
    param_list = [
        "init_done",
        "gtpowergood",
        "qplllock",
    ]
    mgt_text = "MGT Transceiver(s)"


class SectionTXStatusCheck(SectionStatusCheckBase):
    section_name = "TX-Only MGT Status Bit Checks"
    action_name = "Check TX Status"
    param_list = [
        "txpmaresetdone",
        "reset_tx_done",
        "buffbypass_tx_done",
        "buffbypass_tx_error",
        "txphaligndone",
        "txphaligninitdone",
        "txsyncdone",
    ]
    mgt_text = "MGT Transmitter(s)"

    def _setup_mgt_list(self):
        self.mgt_list = [MGTProps(gt_id=gt).mgt_id
                         for gt in self.gt_conf.get_tx_gts()]


class SectionRXStatusCheck(SectionStatusCheckBase):
    section_name = "RX-Only MGT Status Bit Checks"
    action_name = "Check RX Status"
    param_list = [
        "mgt_cdr_locked",
        "rxpmaresetdone",
        "reset_rx_done",
        "buffbypass_rx_done",
        "buffbypass_rx_error",
        "rxprbserr_flg",
        "rxprbslocked",
        "rxphaligndone",
        "rxphalignerr",
        "rxsyncdone",
    ]
    mgt_text = "MGT Receiver(s)"

    def _setup_mgt_list(self):
        self.mgt_list = [MGTProps(gt_id=gt).mgt_id
                         for gt in self.gt_conf.get_rx_gts()]


class SectionOpticalPower(ActionSection):
    section_name = "Optical Power Summary Graphs"
    action_name = "Get Optical Power"
    def __init__(self, doc, data_fname, FIGUREPATH, index=-1):
        ActionSection.__init__(self, doc, data_fname, index=index)
        self.graph_names = []

        # If no data found, nothing to do
        if not self.action:
            return

        for end in ("tx", "rx"):
            fig = self.create_optical_summary_graph(tx_or_rx=end)
            graph_fname = "/".join([FIGUREPATH, "optical_power_graph_%s.png"%(end)])
            self.graph_names.append(graph_fname)
            fig.savefig(graph_fname)

        with self.doc.create(Section(self.section_name)):
            self.doc.append("The following graphs display the latest optical powers received.\n")
            for graph_fname in self.graph_names:
                with self.doc.create(Figure(position='!htbp')) as graph:
                    graph.add_image(graph_fname, width='9in')
        # To make sure all graphs are placed in this section
        self.doc.append(NoEscape(r"\clearpage"))


    def create_optical_summary_graph(self, tx_or_rx="rx", plot_zeros=True):
        fig, ax = plt.subplots(figsize=(15,5))
        # First row is 'Channels, 0, 1, ..., 11'
        for row in self.action.get_results_dual_list()[1:]:
            device = row[0]
            if tx_or_rx not in device:
                continue
            device = device.replace(tx_or_rx, "").strip()
            # Get optical powers as floats
            optical_powers = [float(power) for power in row[1:]]

            if not plot_zeros:
                # Replace 0.0's with None (so those points don't plot, rather than plot at 0)
                optical_powers = [x if x else None for x in optical_powers]

            ax.plot(optical_powers, marker='o', label=device)

        data_width = len(self.action.get_results_dual_list()[0]) - 1
        ax.set_xticks(np.arange(data_width))
        ax.set_xlabel("Channel")

        ax.set_ylim([0, 1500])
        ax.set_ylabel("Optical Power (uW)")

        ax.grid()
        ax.legend()

        ax.title.set_text("Optical Power %s Summary"%tx_or_rx.upper())
        plt.tight_layout()

        return fig


class SectionEyeDiagrams(ActionSection):
    section_name = "Eye Diagram Summary Graphs"
    action_name = "Collect Eye Diagrams"
    def __init__(self, doc, data_fname, link_conf, group_link_dict, full_link_list, FIGUREPATH, index=-1):
        ActionSection.__init__(self, doc, data_fname, index=index)
        self.link_conf = link_conf
        self.group_link_dict = group_link_dict
        self.graph_names = []

        # If no data found, nothing to do
        if not self.action:
            return

        self.present_link_list = [link for link in full_link_list
                                  if link in self.action.get_results_dual_dict()]

        eye_dia_dict = self.action.get_results_dual_dict()
        for pretty_name, link_list in group_link_dict.items():
            if link_list or (pretty_name != "Other Links"):

                relevant_link_list = [link for link in link_list if link in self.present_link_list]
                if relevant_link_list:
                    fig = self.create_eye_summary_graph(relevant_link_list, pretty_name)
                    graph_name = "/".join([FIGUREPATH, pretty_name + ".png"])
                    self.graph_names.append(graph_name)
                    fig.savefig(graph_name)

        with self.doc.create(Section(self.section_name)):
            self.doc.append("The following graphs summarize the eye diagram data collected for this IBERT test. Red markers indicate links for which the maximum number of realignments was performed, and hence the result is not from a 'clean' run of the collection process.\n")
            for graph_fname in self.graph_names:
                with self.doc.create(Figure(position='!htbp')) as graph:
                    graph.add_image(graph_fname, width='9in')

        # To make sure all graphs are placed in this section
        self.doc.append(NoEscape(r"\clearpage"))

        self.problem_links += self.get_bad_eye_links()


    def get_bad_eye_links(self):
        """
        Given the results dual dict from a CollectEyeDiagrams action, and a
        list of links (subset of the links in that action - which are the keys
        of that dual dict), return list of links which had "NA" for open area
        or vertical/horizontal opening, or which had the maximum number of
        realignments performed.
        """
        
        bad_links = self.get_NA_links()
        bad_links += [link for link in self.get_max_realign_links() if link not in bad_links]
        return bad_links

    def get_NA_links(self):
        bad_links = []
        for link in self.present_link_list:
            link_results = self.action.get_results_dual_dict()[link]
            open_area = link_results["Open Area"]
            horiz_open = link_results["Horiz Open"]
            vert_open = link_results["Vert Open"]
            # Process open areas, get relevant number as a float (opening %) or 'NA'
            open_area = open_area[open_area.find("(")+1:open_area.find(")")]
            open_area = open_area.replace("%", "").strip()
            horiz_open = horiz_open.replace("%", "").strip()
            vert_open = vert_open.replace("%", "").strip()
            if "NA" in (open_area, horiz_open, vert_open):
                bad_links.append(link)
        return bad_links

    def get_max_realign_links(self):
        bad_links = []
        for link in self.present_link_list:
            if self.check_max_realign(link):
                bad_links.append(link)
        return bad_links

    def check_max_realign(self, link):
        link_results = self.action.get_results_dual_dict()[link]
        # Need to watch out for missing 'Max Realigns' query, since not all
        # versions of the eye diagram collector have it
        try:
            max_realigns = link_results["Max Realigns"]
            if max_realigns == "Yes":
                return True
        except KeyError:
            pass
        return False


    def create_eye_summary_graph(self, link_list, title):
        """
        Given the results/data from a CollectEyeDiagrams action, create a
        matplotlib line graph giving the various opening statistics for each link.
        """
        eye_summary_data = OrderedDict()

        for link in link_list:
            link_results = self.action.get_results_dual_dict()[link]
            open_area = link_results["Open Area"]
            horiz_open = link_results["Horiz Open"]
            vert_open = link_results["Vert Open"]
            # Process open areas, get relevant number as a float (opening %) or 'NA'
            open_area = open_area[open_area.find("(")+1:open_area.find(")")]
            open_area = open_area.replace("%", "").strip()
            horiz_open = horiz_open.replace("%", "").strip()
            vert_open = vert_open.replace("%", "").strip()

            opening_dict = OrderedDict(
                [
                    ("Open Area", open_area),
                    ("Horiz Open", horiz_open),
                    ("Vert Open", vert_open),
                ]
            )
            for key, value in opening_dict.items():
                if value == "NA":
                    #value = float(0)
                    value = None
                else:
                    value = float(value)
                opening_dict[key] = value
            eye_summary_data[link] = opening_dict

        opening_areas = [eye_summary_data[x]["Open Area"] for x in link_list]
        horiz_openings = [eye_summary_data[x]["Horiz Open"] for x in link_list]
        vert_openings = [eye_summary_data[x]["Vert Open"] for x in link_list]

        # True-False list telling whether each link used max # of realignments
        realign_list = [True if self.check_max_realign(x) else False
                        for x in link_list]
        # Get lists of colors for data points - red if max realigns reached, normal otherwise
        opening_area_colors = ['red' if x else 'C0' for x in realign_list]
        horiz_opening_colors = ['red' if x else 'C1' for x in realign_list]
        vert_opening_colors = ['red' if x else 'C2' for x in realign_list]

        ## Creating Eye Diagram Summary Plot
        fig, ax = plt.subplots(figsize=(15,5))

        x_range = np.arange(len(link_list))


        # Plot main data points and lines (done seperately to allow conditional colors for markers)
        ax.plot(opening_areas, label="Open Area", color='C0', zorder=2)
        ax.scatter(x_range, opening_areas, marker='o', color=opening_area_colors, zorder=1)

        ax.plot(horiz_openings, label="Horizontal Opening", color='C1', zorder=2)
        ax.scatter(x_range, horiz_openings, marker='o', color=horiz_opening_colors, zorder=1)

        ax.plot(vert_openings, label="Vertical Opening", color='C2', zorder=2)
        ax.scatter(x_range, vert_openings, marker='o', color=vert_opening_colors, zorder=1)

        # Add red, vertical lines for missing data points
        for opening_list in (opening_areas, horiz_openings, vert_openings):
            for x, opening in zip(x_range, opening_list):
                if opening is None:
                    ax.axvline(x=x, color='red', zorder=0)

        # Setup x-axis 
        ax.set_xticks(x_range)
        ax.set_xticklabels(link_list, rotation=90)
        ax.set_xlabel("Link Name")

        # Setup y-axis
        ax.set_ylim([0, 100])
        ax.set_ylabel("Opening %")

        # So that grid appears behind data points
        ax.set_axisbelow(True)
        ax.grid()

        # Other cosmetics
        ax.legend()
        ax.title.set_text(title)

        plt.tight_layout()
        return fig


class SectionFinalSummary:
    def __init__(self, doc, sections):
        self.doc = doc
        self.sections = sections
        made_sections = [section for section in sections if section]

        with self.doc.create(Section("Final Summary")):

            problems = False
            for section in made_sections:
                problems = True if section.problem_links else False

            if problems:
                self.doc.append("WARNING - The following links/MGTs may be problematic:")
                with self.doc.create(Itemize()) as section_itemize:
                    for section in made_sections:
                        section_name = section.section_name
                        if section.problem_links:
                            section_itemize.add_item("In section '%s': "%section_name)
                            with self.doc.create(Itemize()) as link_itemize:
                                for link in section.problem_links:
                                    link_itemize.add_item(link)
            else:
                self.doc.append("No problems detected on any links.\n")

            self.doc.append("Note: links/MGTs for which no problems were detected may still not perform according to expectations. In particular, analog results (including eye opening data and optical power readings) should be checked by the reader.")


def add_tabularx(doc, data):
    table_str = "|l"*len(data[0]) + "|"
    with doc.create(Tabularx(table_str)) as data_table:
        #doc.append(NoEscape(r"\begin{adjustbox}{width=\textwidth}"))
        data_table.add_hline()
        for row in data:
            data_table.add_row(row)
            data_table.add_hline()
        #doc.append(NoEscape(r"\end{adjustbox}"))


def add_table(doc, data):
    table_str = "|l"*len(data[0]) + "|"
    with doc.create(Tabular(table_str)) as data_table:
        #doc.append(NoEscape(r"\begin{adjustbox}{width=\textwidth}"))
        data_table.add_hline()
        for row in data:
            data_table.add_row(row)
            data_table.add_hline()
        #doc.append(NoEscape(r"\end{adjustbox}"))


def add_longtable(doc, data, headers=None, title=None, color_list=None):
    if headers is None:
        headers=data[0]
        data = data[1:]

    if color_list is None:
        color_list = [None for row in data]

    width = len(data[0])
    longtable_str = "|l"*width + "|"
    with doc.create(LongTable(longtable_str)) as data_table:
        # Title/Header
        data_table.add_hline()
        if title is not None:
            data_table.add_row((MultiColumn(width, align='c', data=title),))
        data_table.add_hline()
        data_table.add_row(headers)
        data_table.add_hline()
        data_table.end_table_header()

        # Regular Footer
        data_table.add_hline()
        data_table.add_row((MultiColumn(width, align='r',
                            data='Continued on Next Page'),))
        data_table.add_hline()
        data_table.end_table_footer()

        # Last footer
        data_table.add_hline()
        data_table.add_row((MultiColumn(width, align='r',
                            data='End of Table'),))
        data_table.add_hline()
        data_table.end_table_last_footer()
        data_table.add_hline()

        data_table.add_hline
        for i, row in enumerate(data):
            data_table.add_row(row, color=color_list[i])
            data_table.add_hline()


class LandscapeEnvironment(Environment):
    """
    A class representing a Landscape LaTeX environment.

    This class represents a LaTeX environment named
    ``landscape``.
    """

    _latex_name = 'landscape'
    packages = [Package('pdflscape')]


class AdjustboxEnvironment(Environment):
    _latex_name = 'adjustbox'
    packages = [Package('adjustbox')]


class TabularxEnvironment(Environment):
    _latex_name = 'tabularx'
    packages = [Package('tabularx')]



# Main
if __name__ == '__main__':

    cmd_line_args = ArgGetter(
        arg_tup_list = [
            ("-i", "--input"),
            ("-t", "--title"),
            ("-m", "--message")
        ],
        arg_tup_list_bool = [
            ("-h", "--help"),
        ],
    )

    if cmd_line_args["--help"]:
        show_usage()

    input_fname = cmd_line_args["--input"]
    title = cmd_line_args["--title"]
    message = cmd_line_args["--message"]
    main(input_fname, title=title, message=message)
