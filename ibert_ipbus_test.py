#!/usr/bin/env python
# Standard Library Modules
import sys
import os
from collections import OrderedDict
import copy
import time

IBERTPATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(IBERTPATH + '/PrivateModules') 
sys.path.append(IBERTPATH + '/Tools')

# Original Modules
from gt_mgt_conversion import MGTProps
from cmd_line_args_class import ArgGetter
from log_class import Log
from yaml_functions import get_yaml, is_yaml_none
from ibert_management_classes import Manager
from ibert_action_classes import g_action_map
from ibert_helper_functions import format_to_width
import gui as ibert_gui

try:
    from Tkinter import *
    import ttk
    import tkFont
    import tkFileDialog as filedialog

except ImportError:
    from tkinter import *
    from tkinter import filedialog
    from tkinter import ttk
    import tkinter.font as tkFont

# For input compatibility between python2 and python3
if sys.version_info[:2] <= (2, 7):
    safe_input = raw_input
else:
    safe_input = input

version = "r6v7"

def setup_directories(setup_date_dir=False):
    if not os.path.exists(IBERTPATH+"/Results/"):
        os.makedirs(IBERTPATH+"/Results/")
    if setup_date_dir:
        date_dir_id = time.strftime("%d%b%Y")
        if not os.path.exists(IBERTPATH+"/Results/"+date_dir_id):
            os.makedirs(IBERTPATH+"/Results/"+date_dir_id)


def show_usage():

    print("""
Usage: python ibert_ipbus_test.py   
                                       -h|--help
                                       -c|--user_config_file
                                       -g|--gui
                                       -d|--device_config_file
                                       -l|--log_name
                                       -m|--message
                                       --hub1_SN
                                       --hub2_SN

Options:

 -h, --help                            Display this message and exit
 -c, --user_config_file [file name]    Specify test configuration file     
 -g, --gui                             Use GUI to manage user config settings
                                         (alternative to specifying file)
 -d, --device_config_file [file name]  Specify a device configuration file    
                                         (if not specified, defaults to
                                         <IBERTAREA>/device_config.yaml
 -l, --log_name [log name]             Log output to: 
                                         <IBERTAREA>/Results/[log name]
 -m, --message                         Message to include at beginning of log
 --hub1_SN [SN]                        For log header
 --hub2_SN [SN]                        For log header
    """)


def get_full_config(cmd_line_args):
    """
    Based on command line arguments, get a full config dictionary.

    Full config dictionary is a combination of the user config and device
    config.

    If --gui command line option is specified, GUI will be used to determine
    user config.
    """
    # Use GUI to get configuration
    if cmd_line_args["--gui"]:
        config =  get_full_config_gui_method(cmd_line_args)
    else:
        config =  get_full_config_file_method(cmd_line_args)

    device_config_file = cmd_line_args["--device_config_file"]
    if device_config_file is None:
        device_config_file = 'device_config.yaml'
    config.update(get_yaml(device_config_file))

    return config


def get_full_config_gui_method(cmd_line_args):
    """
    Function for creating GUI for determining full config.
    """
    user_config_file = cmd_line_args["--user_config_file"]
    if user_config_file:
        user_config_file = IBERTPATH + '/' + user_config_file

    device_config_file = cmd_line_args["--device_config_file"]
    if device_config_file:
        device_config_file = IBERTPATH + '/' + user_config_file

    root = Tk()
    root.title("IBERT IPBus Test Config GUI | %s"%(version))
    ibert_gui_obj = ibert_gui.IbertConfigGUI(
        root,
        in_test = True,
        config_file = user_config_file,
    )
    root.mainloop()
    start_test = ibert_gui_obj.start_test
    yaml_str = get_yaml(ibert_gui_obj.get_yaml_str())
    if not start_test:
        print("ALERT: GUI closed without starting test; Exiting ... \n")
        sys.exit()

    return yaml_str


def get_full_config_file_method(cmd_line_args):
    """
    Function for determining full config using specified files (not GUI)
    """
    user_config_file = cmd_line_args["--user_config_file"]
    device_config_file = cmd_line_args["--device_config_file"]
    if user_config_file is None:
        user_config_file = 'PrivateModules/template.yaml'
    if device_config_file is None:
        device_config_file = 'device_config.yaml'

    config = get_yaml(user_config_file)
    config.update(get_yaml(device_config_file))

    return config


def get_log(cmd_line_args):
    """
    Function for getting log name (w/ verbosity) based on cmd line arguments.
    """

    # Create log, printing to terminal output if no log filename provided
    log_name = cmd_line_args["--log_name"]
    verbosity = cmd_line_args["--verbosity"]
    if not verbosity:
        verbosity = 0
    else:
        verbosity = int(verbosity)

    if log_name:
        log = Log("Results/%s/"%(time.strftime("%d%b%Y")) + log_name, verbosity=verbosity)
    else:
        log = Log("", verbosity=verbosity)

    return log


def get_raw_log(log):
    """
    Function for creating a raw-csv log from main log.
    """
    raw_log_name = log.log_name
    if raw_log_name:
        raw_log_name = raw_log_name.replace(".txt", "")
        raw_log_name += "_raw.csv"

    return Log(raw_log_name)


def interrupt_dialog(manager=None, raw_log=None):
    """
    Dialog for when IPBus IBERT test is interrupted.

    Offers to perform a full report, then asks user whether they want to exit
    test.
    """
    while True:
        if manager is not None:
            response = safe_input("\nInterrupted; Perform Full Report? (Y/N): ")
            if response.lower() in ("y", "yes"):
                perform_actions(manager, ["Full Report"], raw_log)
            elif response.lower() not in ("n", "no"):
                print("Invalid Response '%s'"%(response))

        response = safe_input("\nExit program? (Y/N): ")
        if response.lower() in ("y", "yes"):
            end_date_str = time.strftime("%d/%m/%y")
            end_time_str = time.strftime("%H:%M")
            raw_log.write(",".join(("End of IBERT test", end_date_str, end_time_str)))
            raw_log.write("\n")
            raw_log.flush(print_log=False)
            sys.exit()
        elif response.lower() in ("n", "no"):
            break
        else:
            print("Invalid Response '%s'"%(response))


def perform_actions(manager, action_list, raw_log=None):
    """
    Perform a set of actions, and log results.

    Args:
        manager: Manager object for IPBus IBERT test
        action_list: list of actions (string identifiers from user config) to
            perform
        raw_log: (optional) raw_log to write csv results to
    """

    action_storage = []
    if is_yaml_none(action_list):
        return  action_storage

    for action_name in action_list:
        try: 
            start_date_str = time.strftime("%d/%m/%y")
            start_time_str = time.strftime("%H:%M")
            action = g_action_map[action_name](
                manager, display_banner=True, display_progress=True
            )
            action.display_results()
            action_storage.append(action)
            end_date_str = time.strftime("%d/%m/%y")
            end_time_str = time.strftime("%H:%M")
            if raw_log is not None:
                raw_log.write("Beginning of Action Results,%s,%s,%s\n"%(
                    action_name, start_date_str, start_time_str)
                )
                raw_log.write(action.get_results_csv())
                raw_log.write("End of Action Results,%s,%s,%s\n"%(
                    action_name, end_date_str, end_time_str)
                )
                #raw_log.write("Beginning of Action Alerts,%s\n"%action_name)
                #raw_log.write("End of Action Alerts,%s\n"%action_name)
                raw_log.flush(print_log=False)

        except KeyboardInterrupt:
            # Be sure that the action start/end is closed
            end_date_str = time.strftime("%d/%m/%y")
            end_time_str = time.strftime("%H:%M")
            raw_log.write("End of Action Results,%s,%s,%s\n"%(
                action_name, end_date_str, end_time_str)
            )
            interrupt_dialog(manager, raw_log)

    return action_storage


def get_init_params_report(config):
    """
    Based on config, return a str report of initialization parameters.
    """
    full_init_param_dict = copy.copy(config["device_init_parameters"])
    full_init_param_dict.update(config["link_init_parameters"])

    report = """Initialization Parameters:
    PRBS-Select         : {prbs-select}
    Loopback            : {loopback}
    DFE On/Off          : {DFE}
    MGT Equalizer       : {mgt equalizer}
    Post-Emphasis* (dB) : {post-emphasis}
    Pre-Emphasis* (dB)  : {pre-emphasis}
    Swing* (mV)         : {swing}
    (* indicates parameters which are rounded, and may differ slightly from the
    actual value used, which may also differ between MGT types)

""".format(**full_init_param_dict)

    return report 


def get_action_report(config):
    """
    Based on config, return str report of the actions to be performed in test.
    """

    one_time_list = config["single_action_list"]
    if one_time_list:
        one_time_list_pretty = "\n    ".join(one_time_list)
    else:
        one_time_list_pretty = "None"

    periodic_list = config["periodic_action_list"]
    if periodic_list:
        periodic_list_pretty = "\n    ".join(periodic_list)
    else:
        periodic_list_pretty = "None"

    report = "One-time Actions:\n    %s\n\nPeriodic Actions:\n    %s\n\n"
    report = report%(one_time_list_pretty, periodic_list_pretty)

    if periodic_list:
        periodic_timing_dict = config["periodic_action_timing"]

        periodic_timing_str = """Periodic Action Timing
    Start Time (s)  : {start time}
    Period (s)      : {period}
    # of Iterations : {no of iterations}

""".format(**periodic_timing_dict)

        report += periodic_timing_str

    return report


def get_link_report(config, manager):
    """
    Based on config, return str of links to be used in test.
    """
    link_report_dict = {
        "no_of_devices" : len(manager.devices),
        "no_of_links" : len(config["link_list"]),
        "no_of_gts" : len(manager.gt_conf),
        "no_of_txs" : manager.gt_conf.get_no_of_tx_gts(),
        "no_of_rxs" : manager.gt_conf.get_no_of_rx_gts(),
    }

    report = """Link Configuration Info:
    # of Cards/Devices   : {no_of_devices}
    # of Links Specified : {no_of_links}
    Links Include        : {no_of_gts} MGTs ({no_of_txs} TXs, {no_of_rxs} RXs)

""".format(**link_report_dict)
    return report


def write_link_list_raw_csv(raw_log, config, link_conf):
    """
    Based on config, write link list section to raw log
    """
    raw_log.write("Beginning of Link List\n")
    for link in config["link_list"]:
        if not is_yaml_none(link_conf[link]["source"]):
            source_gt_id = "{card}.{channel}".format(**link_conf[link]["source"])
            source_mgt_id = MGTProps(gt_id=source_gt_id).mgt_id
        else:
            source_gt_id = "None"
            source_mgt_id = "None"
        if not is_yaml_none(link_conf[link]["dest"]):
            dest_gt_id = "{card}.{channel}".format(**link_conf[link]["dest"])
            dest_mgt_id = MGTProps(gt_id=dest_gt_id).mgt_id
        else:
            dest_gt_id = "None"
            dest_mgt_id = "None"
        raw_log.write("%s,%s,%s\n"%(link, source_mgt_id, dest_mgt_id))
    raw_log.write("End of Link List\n")
    raw_log.flush(print_log=False)
    return


def write_init_params_raw_csv(raw_log, config):
    """
    Based on config, write initialization parameters section to raw log
    """
    raw_log.write("Beginning of Initialization Parameters\n")
    full_init_param_dict = copy.copy(config["device_init_parameters"])
    full_init_param_dict.update(config["link_init_parameters"])
    raw_log.write("""PRBS-Select,{prbs-select}
Loopback,{loopback}
DFE On/Off,{DFE}
MGT Equalizer,{mgt equalizer}
Post-Emphasis(dB),{post-emphasis}
Pre-Emphasis(dB),{pre-emphasis}
Swing(mV),{swing}
""".format(**full_init_param_dict))
    raw_log.write("End of Initialization Parameters\n")
    raw_log.flush(print_log=False)
    return

def write_test_start_raw(raw_log):
    # Get start timing
    start_date_str = time.strftime("%d/%m/%y")
    start_time_str = time.strftime("%H:%M")

    # Write start info to raw log
    raw_log.write(",".join(("Start of IBERT test", start_date_str, start_time_str)))
    raw_log.write("\n")
    raw_log.flush(print_log=False)

def write_test_end_raw(raw_log):
    # Write end info to raw log
    end_date_str = time.strftime("%d/%m/%y")
    end_time_str = time.strftime("%H:%M")
    raw_log.write(",".join(("End of IBERT test", end_date_str, end_time_str)))
    raw_log.write("\n")
    raw_log.flush(print_log=False)


def main():
    # Get cmd line arguments
    cmd_line_args = ArgGetter(
        arg_tup_list = [
            ("-l", "--log_name"),
            ("-c", "--user_config_file"),
            ("-d", "--device_config_file"),
            ("-v", "--verbosity"),
            ("-m", "--message"),
            ("--hub1_SN",),
            ("--hub2_SN",),
        ],
        arg_tup_list_bool = [
            ("-h", "--help"),
            ("-g", "--gui"),
            ("-f", "--full"),
        ],
    )

    # Setup results directories
    setup_date_dir = True if cmd_line_args["--log_name"] is not None else False
    setup_directories(setup_date_dir)

    # If user requests help, show usage and exit
    if cmd_line_args["--help"]:
        show_usage()
        sys.exit()

    # Setup logs
    log = get_log(cmd_line_args)
    raw_log = get_raw_log(log)

    config = get_full_config(cmd_line_args)

    manager = Manager(
        user_conf=config,
        used_links_only = not cmd_line_args["--full"],
        used_devs_only = not cmd_line_args["--full"],
        dev_conf=config,
        log=log
    )

    write_test_start_raw(raw_log)

    # Write link list and initialization parameters to raw log
    write_link_list_raw_csv(raw_log, config, manager.link_conf)
    write_init_params_raw_csv(raw_log, config)

    # Opening Header
    width = 80
    log.write(("="*width) + "\n")
    log.write("|{:^{width}}|\n".format("IBERT Test - %s"%(version), width=width-2))
    if cmd_line_args["-l"]:
        log.write("|{:^{width}}|\n".format(cmd_line_args["-l"], width=width-2))
    if cmd_line_args["--hub1_SN"] or cmd_line_args["--hub2_SN"]:
        hub_sn_str = "Hub1 SN: %s, Hub2 SN: %s"%(
            cmd_line_args["--hub1_SN"], cmd_line_args["--hub2_SN"]
        )
        log.write("|{:^{width}}|\n".format(hub_sn_str, width=width-2))
    log.write("|{:^{width}}|\n".format(time.asctime(), width=width-2))
    log.write("|{:{width}}|\n".format("", width=width-2))
    log.write(("="*width) + "\n\n")
    log.flush()

    # Opening message
    if cmd_line_args["--message"] is not None:
        message = cmd_line_args["--message"]
        message_trimmed = format_to_width(message, width=80)
        log.write("User message:\n\n%s\n\n"%message_trimmed)
        log.flush()
        # Replace commas with $ in csv file, for safety (can always convert back)
        raw_log.write("User Message,%s\n"%(message.replace(",", "$")))
        raw_log.flush(print_log=False)


    # Write init params, actions, links (user config info) to log
    log.write(get_init_params_report(config))
    log.write(get_action_report(config))
    log.write(get_link_report(config, manager))
    log.flush()

    # For storing performed actions (in case we want to use them later)
    action_storage_list = []

    # One-time actions
    action_storage_list += perform_actions(
        manager,
        config["single_action_list"],
        raw_log
    )

    # Periodic Actions - Initial Wait
    periodic_timing = config["periodic_action_timing"] 
    # If there are no periodic actions, don't bother with the timing 
    if not config["periodic_action_list"]:
        write_test_end_raw(raw_log)
        return
    if periodic_timing["no of iterations"] < 1:
        write_test_end_raw(raw_log)
        return
    try:
        time.sleep(int(periodic_timing["start time"]))
    except KeyboardInterrupt:
        interrupt_dialog(manager, raw_log)

    # Periodic Actions - Actual do-loop
    i=0
    i += 1
    action_storage_list += perform_actions(
        manager,
        config["periodic_action_list"],
        raw_log
    )
    while i < int(periodic_timing["no of iterations"]):
        try:
            time.sleep(int(periodic_timing["period"]))
        except KeyboardInterrupt:
            interrupt_dialog(manager, raw_log)
        i += 1
        action_storage_list += perform_actions(
            manager,
            config["periodic_action_list"],
            raw_log
        )

    write_test_end_raw(raw_log)
    return
    

if __name__ == "__main__":
    main()

