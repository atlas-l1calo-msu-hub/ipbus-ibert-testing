import sys
import os
from tabulate import tabulate
import os
IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
sys.path.append(IBERTPATH + "/PrivateModules/")
from ibert_management_classes import Devices
from log_class import Log

log_name = ""
my_log = Log(log_name)
devices = Devices()

for dev in ("hub1", "hub2"):
    dev_hw = devices[dev]
    if dev_hw is not None and dev_hw != "None":
        my_log.write("Writing 0b1 to csr->control->rod_pwr_en on %s (%s)\n"%(dev, dev_hw))
        my_log.flush()
        dev_hw["csr"]["control"]["rod_pwr_en"].write(0b1)
        my_log.write("Value read back after writing 0b1: %s\n"%(dev_hw["csr"]["control"]["rod_pwr_en"].read()))
        my_log.flush()
    else:
        my_log.write("Skipping %s, no device specified in configuration\n")
        my_log.flush()

my_log.write("Rod power-ups complete\n")
my_log.flush()
