#!/usr/bin/python
import os
import sys
import re
import pdb
import traceback
IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
LINKCONFPATH = IBERTPATH + '/LinkConfiguration/'
MGTDEFPATH = LINKCONFPATH + '/MGTDefinitions/'
PRIVATEMODPATH = IBERTPATH + '/PrivateModules/'
XMLPATH = IBERTPATH + '/Xml/'
sys.path.append(IBERTPATH+'/PrivateModules/')
from yaml_functions import get_yaml, dump_yaml

try:
    from Tkinter import *
    import ttk
    import tkFont
    import tkFileDialog as filedialog

except ImportError:
    from tkinter import *
    from tkinter import filedialog
    from tkinter import ttk
    import tkinter.font as tkFont

version = "r6v4"

class IbertVariables:
    def __init__(self):
        """
        Initialize object-wide variables to be used in user configuration, as
        well as some 'helper' variables that will not be used in the
        configuration, but will need to be known for certain GUI functions.
        """

        self.prbs_select = StringVar(self.root)
        self.dfe = StringVar(self.root)
        self.mgt_equ = StringVar(self.root)
        self.pre_emphasis = StringVar(self.root)
        self.post_emphasis = StringVar(self.root)
        self.swing = StringVar(self.root)
        self.perform_initialization = BooleanVar(self.root)

        self.period_length = StringVar(self.root)
        self.period_start_time = StringVar(self.root)    
        self.no_of_periods = StringVar(self.root)

        self.single_action_list = Variable(self.root)
        self.periodic_action_list = Variable(self.root)
        self.link_list = Variable(self.root)

        self.device_name_list = ["Hub1",
                                 "Hub2",
                                 "ROD1",
                                 "ROD2",
                                 "HTM3",
                                 "HTM4",
                                 "HTM5",
                                 "HTM6",
                                 "HTM7",
                                 "HTM8",
                                 "HTM9",
                                 "HTM10",
                                 "HTM11",
                                 "HTM12",
                                 "HTM13",
                                 "HTM14",
                                 ]

        self.device_connhub_mapping = {}
        for device_name in self.device_name_list:
            device_name_lower = device_name.lower()
            self.device_connhub_mapping[device_name_lower] = StringVar(self.root)

class IbertConfigGUI:
    """
    Class for creating and IBERT Config GUI
    """

    def __init__(self, frame, in_test = False, config_file=None):

        self.root = frame

        self.in_test = in_test
        self.start_test = False

        # Setup Fonts (use smaller fonts for python 2.7, which appears to have
        # bigger text)
        if sys.version_info >= (3, 0, 0):
            #self.font = tkFont.Font(family="System", size=16)
            #self.small_font = tkFont.Font(family="System", size=12)
            self.font = tkFont.Font(family="System", size=12)
            self.small_font = tkFont.Font(family="System", size=10)
        else:
            #self.font = tkFont.Font(family="System", size=12)
            #self.small_font = tkFont.Font(family="System", size=10)
            self.font = tkFont.Font(family="System", size=9)
            self.small_font = tkFont.Font(family="System", size=7)

        self.yaml_code = get_yaml(IBERTPATH+'/device_config.yaml') 

        if config_file:
            self.import_configuration(config_file)
        
        self.initialize_variables()
        self.initialize_frames()

        self.connhub_gui = ConnHubGUI(self.connhub_frame, self)
        self.config_file_gui = ConfigFileGUI(self.config_file_frame, self)
        self.control_gui = ControlGUI(self.control_frame, self)

    #def sort_yaml_code(self):
    #    for key in ("hub1", "hub2", "rod1", "rod2", "htm3", "htm4", "htm5",
    #                "htm6", "htm7", "htm8", "htm9", "htm10", "htm11", "htm12",
    #                "htm13", "htm14"):
    #        if key in self.yaml_code:
    #            self.yaml_code.move_to_end(key)
            

    def initialize_variables(self):
        """
        Initialize object-wide variables to be used in user configuration, as
        well as some 'helper' variables that will not be used in the
        configuration, but will need to be known for certain GUI functions.
        """

        self.device_name_list = ["Hub1",
                                 "Hub2",
                                 "ROD1",
                                 "ROD2",
                                 "HTM3",
                                 "HTM4",
                                 "HTM5",
                                 "HTM6",
                                 "HTM7",
                                 "HTM8",
                                 "HTM9",
                                 "HTM10",
                                 "HTM11",
                                 "HTM12",
                                 "HTM13",
                                 "HTM14",
                                 ]

        self.device_connhub_mapping = {}
        for device_name in self.device_name_list:
            device_name_lower = device_name.lower()
            self.device_connhub_mapping[device_name_lower] = StringVar(self.root)
            # Set default value of "None"
            device_id = self.yaml_code["device_connhub_mapping"][device_name_lower]
            self.device_connhub_mapping[device_name_lower].set(device_id)



    def initialize_frames(self):
        """
        Initialize frames used to organize different portions of GUI
        """

        full_config_frame = Frame(self.root)

        # Create Frame for import/exporting configurations
        self.config_file_frame = LabelFrame(
            #self.root,
            full_config_frame,
            text="Import/Export Configurations",
            padx=5, pady=5
        )
        # Frame for Exiting, Starting test ...
        self.control_frame = LabelFrame(
            #self.root,
            full_config_frame,
            text="Control",
            padx=5, pady=5
        )
        # Grid frames in first column (self.root)
        full_config_frame.grid(row=0, column=0, rowspan=3)
        self.config_file_frame.grid(row=0, column=0, sticky="new")
        self.control_frame.grid(row=2, column=0, sticky="new")

        # ConnHub Frame
        self.connhub_frame = LabelFrame(
            self.root,
            text="Connection Hub",
            padx=5, pady=5
        )
        self.connhub_frame.grid(row=0, column=2, rowspan=3, sticky="ns")

        ToggleVisibilityButton(full_config_frame)
        ToggleVisibilityButton(self.connhub_frame)


    def update_yaml_code(self):
        """
        Update yaml code to have configuration settings as determined by the
        GUI.
        """

        connhub_str = "device_connhub_mapping"
        for device_name in self.device_name_list:
            device_name_lower = device_name.lower()
            device_id = self.device_connhub_mapping[device_name_lower].get()
            self.yaml_code[connhub_str][device_name_lower] = device_id


    def import_configuration(self, config_filename):
        """
        Import variables from user configuration file with given filename
        """
        # Get YAML user configuration
        user_config = get_yaml(config_filename)

        # Get the configuration parameters
        for device_name, device_id in self.device_connhub_mapping.items():
            connhub_str = "device_connhub_mapping"
            new_device_id_str = user_config[connhub_str][device_name]
            device_id.set(new_device_id_str)


    def get_device_connhub_mapping(self):
        """
        Return dictionary of connhub mapping
        """

        device_connhub_mapping = {}
        for key, value in self.device_connhub_mapping.items():
            device_connhub_mapping[key] = value.get()

        return device_connhub_mapping


    def get_full_user_config_dict(self):
        """
        Get dictionary of user configuration settings (python-interpretable)
        """

        full_user_config = {
             "device_connhub_mapping" : self.get_device_connhub_mapping(),
        }

        return full_user_config


    def get_yaml_str(self):
        """
        Output user configuration yaml, but as a string (rather than writing to
        file).
        DEPRECATED with addition of get_yaml_tempfile method
        """
        self.update_yaml_code()
        return dump_yaml(self.yaml_code)


    def slow_destroy(self):
        self.config_file_frame.destroy()
        self.init_frame.destroy()
        self.single_action_frame.destroy()
        self.periodic_action_frame.destroy()
        self.connhub_frame.destroy()
        self.control_frame.destroy()
        self.link_list_frame.destroy()
        self.root.quit()


class ToggleVisibilityButton:
    """
    Class for adding a 'Toggle Visibility' button below a widget. Should only
    be created AFTER the widget has been gridded.
    """

    def __init__(self, widget):

        tog_vis_button = Button(
            #widget.winfo_toplevel(),
            widget.master,
            text="Toggle Visibility",
            command=self.toggle_visibility,
        )
        tog_vis_button.grid(
            row=widget.grid_info()['row']+widget.grid_info()['rowspan'],
            column=widget.grid_info()['column'],
        )

        self.visible = True
        self.widget = widget

    def toggle_visibility(self):
        """
        Toggle visibility of widget
        """

        if self.visible:
            self.widget.grid_remove()
        else:
            self.widget.grid()

        self.visible = not self.visible


class ScrollableListbox:
    """
    Class for creating and managing Scrollable Listboxes as a single object,
    rather than managing a listbox and a scrollbar object.
    """
        
    def __init__(self, parent_frame, **args):

        # Listbox and Scrollbar will be placed in a single, local frame
        self.local_frame = Frame(parent_frame)

        # All 
        self.listbox = Listbox(self.local_frame, **args)
        self.listbox.grid(row=0, column=0, sticky="we")


        self.listbox_scrollbar = Scrollbar(self.local_frame)
        self.listbox_scrollbar.grid(row=0, column=1, sticky="wns", padx=(0,5))

        self.listbox.config(yscrollcommand = self.listbox_scrollbar.set)
        self.listbox_scrollbar.config(command = self.listbox.yview)


    def get(self, *pos_args, **kwd_args):
        return self.listbox.get(*pos_args, **kwd_args)


    def delete(self, *pos_args, **kwd_args):
        return self.listbox.delete(*pos_args, **kwd_args)


    def insert(self, *pos_args, **kwd_args):
        return self.listbox.insert(*pos_args, **kwd_args)


    def curselection(self, *pos_args, **kwd_args):
        return self.listbox.curselection(*pos_args, **kwd_args)


    def grid(self, *pos_args, **kwd_args):
        """
        Grid the local frame, which contains the listbox and scrolbar widgets
        """
        return self.local_frame.grid(*pos_args, **kwd_args)


class ConfigFileGUI:

    def __init__(self, frame, parent):
        """
        For this GUI class I chose to keep reference many of the variables by
        their parent, rather than by self, since there are a lot of them and
        the variables across multiple categories.
        """

        self.frame = frame
        self.font = parent.font
        self.parent = parent
        self.config_setup()

    def config_setup(self):
        """
        Setup portion of GUI used to manage importing and exporting of
        configurations.
        """

        import_button = Button(self.frame,
                               text = "Import Configuration",
                               command = self.get_and_import_configuration,
                               font = self.font)


        export_button = Button(self.frame,
                               text = "Export Configuration",
                               command = self.export_configuration,
                               font = self.font)
        
        import_button.grid(row=0, column=0)
        export_button.grid(row=0, column=1)


    def get_and_import_configuration(self):
        """
        Import a user configuration file.
        """

        filename = StringVar(self.frame)
        open_file(filename, initialdir=IBERTPATH)

        # Import configuration if filename is not empty (user cancelled)
        if filename.get():
            self.parent.import_configuration(filename.get())


    def export_configuration(self):
        """
        Export a user configuration file.
        """

        filename = StringVar(self.frame)
        save_file(filename)

        # # Export configuration if filename is not empty (user cancelled)
        if filename.get():
            self.parent.update_yaml_code()
            with open(filename.get(), 'w') as stream:
                stream.write(dump_yaml(self.parent.yaml_code))


class ControlGUI:

    def __init__(self, frame, parent):

        self.frame = frame
        self.root = parent.root
        self.font = parent.font
        self.in_test = parent.in_test
        self.parent = parent

        self.control_setup()



    def control_setup(self):
        """
        Setup portion of the GUI used to manage test flow/control (e.g. exiting
        test, starting test, etc.)
        """

        exit_button = Button(
            self.frame,
            text = "Exit Test",
            command = lambda : self.exit_gui(start_test=False),
            font = self.font
        )

        if self.in_test:
            start_button = Button(
                self.frame,
                text = "Start Test",
                command = lambda : self.exit_gui(start_test=True),
                font = self.font
            )

        exit_button.grid(row=0, column=0)
        if self.in_test:
            start_button.grid(row=0, column=1)


    def exit_gui(self, start_test = False):
        """
        Exit the GUI
        """

        self.parent.start_test = start_test
        self.parent.root.quit()


class ConnHubGUI:

    def __init__(self, frame, parent):
        self.frame = frame
        self.font = parent.font
        self.parent = parent

        self.device_connhub_mapping = parent.device_connhub_mapping
        self.device_name_list = parent.device_name_list
        self.connhub_setup()


    def connhub_setup(self):
        """
        Setup portion of GUI to be used to manage ConnHub devices
        """


        connhub_file = open(XMLPATH + "/ConnHub.xml", "r")
        connhub_text = connhub_file.read()
        device_id_list = re.findall(r'id="(.*?)"', connhub_text)
        device_id_list.append("None")
        device_id_tuple = tuple(device_id_list)

        row = 0
        for device_name in self.device_name_list:
            self.add_device(device_name, device_id_tuple, row=row)
            row += 1


    def add_device(self, device_name, device_id_tuple, row=0, column=0):
        """
        For use in connhub setup function.
        Add widgets for device id selection. Components are:
        Label: Display device name
        Combobox: Allow user to input device id
        """

        device_label = Label(self.frame,
                             text=device_name,
                             font=self.font)

        device_id = self.device_connhub_mapping[device_name.lower()]

        device_id_combobox = SearchableCombobox(self.frame,
                                                textvariable=device_id,
                                                values=device_id_tuple,
                                                width=15)

        device_label.grid(row=row, column=column, sticky=W)
        device_id_combobox.grid(row=row, column=column+1, sticky=W)


class SearchableCombobox(ttk.Combobox):
    """
    Class for creating and managing 'searchable' Comboboxes. Here 'searchable'
    means that the list of options available to select reduces to only those
    for which the current entry is a substring
    """

    def __init__(self, frame, textvariable, values, **kwd_args):

        self.all_values = values

        try:
            super().__init__(frame, textvariable=textvariable, **kwd_args)
        except: # Catch differences between python2.7 and python3
            ttk.Combobox.__init__(self, frame, textvariable=textvariable, **kwd_args)

        self['values'] = self.all_values 

        # Trace changes in combobox, update list
        textvariable.trace_variable(
            mode = ('wu'), # Trace on 'write' or 'unset'
            callback = lambda *args : self.update_combobox(textvariable)
        )


    def update_combobox(self, value):
        """
        Update combobox so that only options are those for which
        link_value is a substring
        """

        value_str = value.get().lower()

        if value_str == '':
            data = self.all_values 
        else:
            data = []
            for item in self.all_values:
                if item: # Make sure item is not None
                    if value_str in item.lower():
                        data.append(item)                

        self['values'] = data


def delete_multiple_select(listbox):
    """
    Delete selected items from listbox. Works also for multiple select mode
    listboxes.
    """
    selected_indices = listbox.curselection()

    # Delete items by index from greatest to least, so that removing one item
    # does not change the position of the others to be deleted
    for index in selected_indices[::-1]:
        listbox.delete(index)


def add_non_empty_item(listbox, item):
    
    """
    Add item to listbox if item is not empty. If an item in the listbox is
    selected, add item after selection (after the first item, for a multiple
    selection).
    """

    selected_indices = listbox.curselection()
    item_str = item.get()

    if item_str:

        if selected_indices:
            listbox.insert(selected_indices[0], item_str)

        else:
            listbox.insert("end", item_str)


def open_file(filename, initialdir = "../UserConfig"):
    """
    Allow user to set filename to that of an existing file (the file is not
    actually opened in this function)
    """

    filetypes = (("yaml Files", "*.yaml"), ("all files", "*.*"))

    filename_temp = filedialog.askopenfilename(initialdir = initialdir,
                                               title = "Select a File",
                                               filetypes = filetypes)

    filename.set(filename_temp)


def save_file(filename, initialdir=None):
    """
    Allow user to set filename to that of an existing or new file (the file
    is not actually saved in this function)
    """

    if initialdir is None:
        initialdir = IBERTPATH

    filetypes = (("yaml Files", "*.yaml"), ("all files", "*.*"))

    filename_temp = filedialog.asksaveasfilename(
        initialdir = initialdir,
        title = "Select a File",
        filetypes = filetypes,
        defaultextension = filetypes,
    )

    filename.set(filename_temp)
    


def main():

    # Create top level widget - the main window
    root = Tk()
    root.title("IBERT IPBus Device GUI - %s"%version)
    #root.geometry("900x900+1000+1000")
    # First part (NNNxMMM) determines window size - If I do this, frame won't expand to fit widgets
    # Second part (+NNN+MMM) determines y and x offset position of window 

    # Establish Confiugration Parameters/Variables
    ibert_window = IbertConfigGUI(root)

    root.mainloop()

if __name__ == "__main__":

    main()
    #pdb.set_trace()
