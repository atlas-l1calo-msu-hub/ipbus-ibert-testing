#!/usr/bin/python

import os
import sys
import re
import pdb
import traceback
import time
IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
LINKCONFPATH = IBERTPATH + '/LinkConfiguration/'
MGTDEFPATH = LINKCONFPATH + '/MGTDefinitions/'
PRIVATEMODPATH = IBERTPATH + '/PrivateModules/'
XMLPATH = IBERTPATH + '/Xml/'

sys.path.append(IBERTPATH+'/PrivateModules/')
from yaml_functions import get_yaml, dump_yaml

try:
    from ruamel_yaml.scalarstring import DoubleQuotedScalarString
except ImportError:
    try:
        from ruamel.yaml.scalarstring import DoubleQuotedScalarString
    except ImportError:
        pass

try:
    from Tkinter import *
    import ttk
    import tkFont
    import tkFileDialog as filedialog

except ImportError:
    from tkinter import *
    from tkinter import filedialog
    from tkinter import ttk
    import tkinter.font as tkFont

sys.path.append(IBERTPATH + '/PrivateModules') 
from ibert_action_classes import g_action_map 


# For tab-autocomplete when running interactively
import rlcompleter
import readline
readline.parse_and_bind("tab: complete")

version = "r6v4"

class IbertVariables:
    def __init__(self):
        """
        Initialize object-wide variables to be used in user configuration, as
        well as some 'helper' variables that will not be used in the
        configuration, but will need to be known for certain GUI functions.
        """

        self.prbs_select = StringVar(self.root)
        self.dfe = StringVar(self.root)
        self.mgt_equ = StringVar(self.root)
        self.pre_emphasis = StringVar(self.root)
        self.post_emphasis = StringVar(self.root)
        self.swing = StringVar(self.root)
        self.loopback = StringVar(self.root)

        self.period_length = StringVar(self.root)
        self.period_start_time = StringVar(self.root)    
        self.no_of_iterations = StringVar(self.root)

        self.single_action_list = Variable(self.root)
        self.periodic_action_list = Variable(self.root)
        self.link_list = Variable(self.root)

        self.device_name_list = ["Hub1",
                                 "Hub2",
                                 "ROD1",
                                 "ROD2",
                                 "HTM3",
                                 "HTM4",
                                 "HTM5",
                                 "HTM6",
                                 "HTM7",
                                 "HTM8",
                                 "HTM9",
                                 "HTM10",
                                 "HTM11",
                                 "HTM12",
                                 "HTM13",
                                 "HTM14",
                                 ]


class IbertConfigGUI:
    """
    Class for creating and IBERT Config GUI
    """

    def __init__(self, frame, in_test = False, config_file=None):


        self.root = frame

        self.in_test = in_test
        self.start_test = False

        # Setup Fonts (use smaller fonts for python 2.7, which appears to have
        # bigger text)
        if sys.version_info >= (3, 0, 0):
            #self.font = tkFont.Font(family="System", size=16)
            #self.small_font = tkFont.Font(family="System", size=12)
            self.font = tkFont.Font(family="System", size=12)
            self.small_font = tkFont.Font(family="System", size=10)
        else:
            #self.font = tkFont.Font(family="System", size=12)
            #self.small_font = tkFont.Font(family="System", size=10)
            self.font = tkFont.Font(family="System", size=9)
            self.small_font = tkFont.Font(family="System", size=7)

        self.initialize_variables()
        self.initialize_frames()

        # Get a default configuration to use
        self.yaml_code = get_yaml(PRIVATEMODPATH + '/template.yaml')

        if config_file:
            self.import_configuration(config_file)
        
        # Get all yaml files which define mgt links
        self.yaml_def_file_list = [
            fname for fname in os.listdir(MGTDEFPATH) if ".yaml" in fname
        ]

        # Combine all yaml mgt definition files into one
        mgt_link_def_yaml_str = ""
        for yaml_file in self.yaml_def_file_list:
            with open(MGTDEFPATH+yaml_file) as stream:
                mgt_link_def_yaml_str += '\n' + stream.read() + '\n'
        self.mgt_link_definitions = get_yaml(mgt_link_def_yaml_str)
        # Get mgt link ordering
        self.link_order_list = get_yaml(LINKCONFPATH + '/link_ordering.yaml')

        # Handle links not present in the link ordering, but present in some
        # yaml definition file
        links_ordered = []
        links_not_ordered = []
        for link_name in dict(self.mgt_link_definitions).keys():
            if link_name in self.link_order_list:
                links_ordered.append(link_name)
            else:
                links_not_ordered.append(link_name)

        self.all_link_list = sorted(
            links_ordered,
            key = lambda x : self.link_order_list.index(x)
        )
        for link_name in links_not_ordered:
            self.all_link_list.append(link_name)



        self.init_options_gui = InitOptionsGUI(self.init_frame, self)
        self.single_action_gui = ActionListGUI(self.single_action_frame, self)
        self.periodic_action_gui = ActionListGUI(self.periodic_action_frame,
                                                 self, periodic=True)
        self.config_file_gui = ConfigFileGUI(self.config_file_frame, self)
        self.control_gui = ControlGUI(self.control_frame, self)
        self.link_list_gui = LinkListGUI(self.link_list_frame, self)


    def initialize_variables(self):
        """
        Initialize object-wide variables to be used in user configuration, as
        well as some 'helper' variables that will not be used in the
        configuration, but will need to be known for certain GUI functions.
        """

        # Create tkinter variables to store ibert user config settings
        self.prbs_select = StringVar(self.root)
        self.dfe = StringVar(self.root)
        self.mgt_equ = StringVar(self.root)
        self.pre_emphasis = StringVar(self.root)
        self.post_emphasis = StringVar(self.root)
        self.swing = StringVar(self.root)
        self.loopback = StringVar(self.root)

        self.single_action_list = Variable(self.root)

        self.periodic_action_list = Variable(self.root)
        self.period_length = StringVar(self.root)
        self.period_start_time = StringVar(self.root)    
        self.no_of_iterations = StringVar(self.root)

        self.link_list = Variable(self.root)

        self.device_name_list = ["Hub1",
                                 "Hub2",
                                 "ROD1",
                                 "ROD2",
                                 "HTM3",
                                 "HTM4",
                                 "HTM5",
                                 "HTM6",
                                 "HTM7",
                                 "HTM8",
                                 "HTM9",
                                 "HTM10",
                                 "HTM11",
                                 "HTM12",
                                 "HTM13",
                                 "HTM14",
                                 ]


        # Set default values
        self.prbs_select.set("PRBS-31")
        self.dfe.set("OFF")
        self.mgt_equ.set("OFF")
        self.pre_emphasis.set("0")
        self.post_emphasis.set("0")
        self.swing.set("1018")
        self.loopback.set("Normal Operation")


        self.period_length.set("3600")
        self.period_start_time.set("5")
        self.no_of_iterations.set("0")

        self.single_action_list.set(tuple())

        self.periodic_action_list.set(tuple())

        self.link_list.set(tuple())



    def initialize_frames(self):
        """
        Initialize frames used to organize different portions of GUI
        """

        full_config_frame = Frame(self.root)

        # Create Frame for import/exporting configurations
        self.config_file_frame = LabelFrame(
            #self.root,
            full_config_frame,
            text="Import/Export Configurations",
            padx=5, pady=5
        )
        # Create Frame for initialization values
        self.init_frame = LabelFrame(
            #self.root,
            full_config_frame,
            text="Initialization Settings",
            padx=5, pady=5
        )
        # Frame for Exiting, Starting test ...
        self.control_frame = LabelFrame(
            #self.root,
            full_config_frame,
            text="Control",
            padx=5, pady=5
        )
        # Grid frames in first column (self.root)
        full_config_frame.grid(row=0, column=0, rowspan=3)
        self.config_file_frame.grid(row=0, column=0, sticky="new")
        self.init_frame.grid(row=1, column=0, sticky="new")
        self.control_frame.grid(row=2, column=0, sticky="new")

        full_action_frame = Frame(self.root)
        # Create Frame for Actions
        self.single_action_frame = LabelFrame(
            full_action_frame,
            text="One-Time Actions",
            padx=5, pady=5
        )
        # Create Frame for Periodic Actions
        self.periodic_action_frame = LabelFrame(
            full_action_frame,
            text="Periodic Actions",
            padx=5, pady=5
        )
        full_action_frame.grid(row=0, column=1, rowspan=3)
        self.single_action_frame.grid(row=0)
        self.periodic_action_frame.grid(row=1)

        # Frame for managing link list 
        self.link_list_frame = LabelFrame(
            self.root,
            text="Link List",
            padx=5, pady=5
        )
        self.link_list_frame.grid(row=0, column=3, rowspan=3, sticky="ns")

        ToggleVisibilityButton(full_config_frame)
        ToggleVisibilityButton(full_action_frame)
        ToggleVisibilityButton(self.link_list_frame)

        """
        link_list_hide_button = Button(self.root,
            text="Toggle Visibility",
            command=lambda : self.link_list_frame.grid_remove()
        )
        link_list_hide_button.grid(row=4, column=3)
        """



    def update_yaml_code(self):
        """
        Update yaml code to have configuration settings as determined by the
        GUI.
        """


        link_str = "link_init_parameters"
        self.yaml_code[link_str]["prbs-select"] = self.prbs_select.get()
        self.yaml_code[link_str]["DFE"] = self.dfe.get()
        self.yaml_code[link_str]["pre-emphasis"] = self.pre_emphasis.get()
        self.yaml_code[link_str]["post-emphasis"] = self.post_emphasis.get()
        self.yaml_code[link_str]["swing"] = self.swing.get()
        self.yaml_code[link_str]["loopback"] = self.loopback.get()

        device_str = "device_init_parameters"
        self.yaml_code[device_str]["mgt equalizer"] = self.mgt_equ.get()

        # We need to empty the action list, then add new items, in order to
        # preserve the comments
        # Empty old action_list actions
        initial_length = len(self.yaml_code["single_action_list"])
        for i in range(0, initial_length):
            del(self.yaml_code["single_action_list"][0])
        # Insert new actions 
        for action in self.get_single_action_list():
            try:
                action = DoubleQuotedScalarString(action)
            except NameError:
                pass
            self.yaml_code["single_action_list"].append(action)

        # We need to empty the action list, then add new items, in order to
        # preserve the comments
        # Empty old action_list actions
        initial_length = len(self.yaml_code["periodic_action_list"])
        for i in range(0, initial_length):
            del(self.yaml_code["periodic_action_list"][0])
        # Insert new actions 
        for action in self.get_periodic_action_list():
            try:
                action = DoubleQuotedScalarString(action)
            except NameError:
                pass
            self.yaml_code["periodic_action_list"].append(action)

        # We need to empty the link list, then add new items, in order to
        # preserve the comments
        # Empty old action_list actions
        initial_length = len(self.yaml_code["link_list"])
        for i in range(0, initial_length):
            del(self.yaml_code["link_list"][0])
        # Insert new actions 
        for link in self.link_list.get():
            try:
                link = DoubleQuotedScalarString(link)
            except NameError:
                pass
            self.yaml_code["link_list"].append(link)

        timing_str = "periodic_action_timing"
        self.yaml_code[timing_str]["start time"] = self.period_start_time.get()
        self.yaml_code[timing_str]["period"] = self.period_length.get()
        self.yaml_code[timing_str]["no of iterations"] = self.no_of_iterations.get()
        

    def import_configuration(self, config_filename):
        """
        Import variables from user configuration file with given filename
        """
        # Get YAML user configuration
        user_config = get_yaml(config_filename, safe_load=False)

        # Get the configuration parameters


        link_init_params = user_config["link_init_parameters"]
        device_init_params = user_config["device_init_parameters"]
        periodic_timing = user_config["periodic_action_timing"]

        self.prbs_select.set(link_init_params["prbs-select"])
        self.dfe.set(link_init_params["DFE"])
        self.pre_emphasis.set(link_init_params["pre-emphasis"])
        self.post_emphasis.set(link_init_params["post-emphasis"])
        self.swing.set(link_init_params["swing"])
        self.loopback.set(link_init_params["loopback"])

        self.mgt_equ.set(device_init_params["mgt equalizer"])

        self.period_start_time.set(int(periodic_timing["start time"]))
        self.period_length.set(int(periodic_timing["period"]))
        self.no_of_iterations.set(int(periodic_timing["no of iterations"]))

        # Need to convert to tuples, since in tkinter for python2.7 setting
        # a variable as a list will result in a string, whereas setting as
        # a tuple will keep it as a tuple (possibly a result of difference
        # between lists and arrays in other languages)
        self.single_action_list.set(
            tuple(user_config["single_action_list"])
        )
        self.periodic_action_list.set(
            tuple(user_config["periodic_action_list"])
        )
        self.link_list.set(tuple(user_config["link_list"]))


    def get_periodic_action_timing(self):
        """
        Get periodic action timing as a dictionary
        """


        periodic_action_timing = {
            "start time" : self.period_start_time.get(),
            "period" : self.period_length.get(),
            "no of iterations" : self.no_of_iterations.get(),
        }

        return periodic_action_timing


    def get_link_init_params(self):
        """
        Get link initialization parameters as a dictionary
        """

        link_initialization_parameters = {
            "prbs-select"   : self.prbs_select.get(),
            "DFE"           : self.dfe.get(),
            "pre-emphasis"  : self.pre_emphasis.get(),
            "post-emphasis" : self.post_emphasis.get(),
            "swing"         : self.swing.get(),
            "loopback"      : self.loopback.get(),
        }
        
        return link_initialization_parameters


    def get_device_init_params(self):
        """
        Get device initialization parameters as a dictionary
        """

        device_init_parameters = {
            "mgt equalizer" : self.mgt_equ.get(),
        }

        return device_init_parameters


    def get_single_action_list(self):
        """
        Get single action list as a list
        """

        return list(self.single_action_list.get())


    def get_periodic_action_list(self):
        """
        Get periodic action list as a list
        """

        return list(self.periodic_action_list.get())


    def get_link_list(self):
        """
        Get link list as a list
        """


        return list(self.link_list.get())


    def get_full_user_config_dict(self):
        """
        Get dictionary of user configuration settings (python-interpretable)
        """

        full_user_config = {
             "device_init_parameters" : self.get_link_init_params(),
             "link_init_parameters" : self.get_device_init_params(),
             "single_action_list" : self.get_single_action_list(),
             "periodic_action_list" : self.get_periodic_action_list(),
             "periodic_action_timing" : self.get_periodic_action_timing(),
             "link_list" : self.get_link_list(),

        }

        return full_user_config


    def get_yaml_str(self):
        """
        Output user configuration yaml, but as a string (rather than writing to
        file).
        DEPRECATED with addition of get_yaml_tempfile method
        """

        self.update_yaml_code()
        return dump_yaml(self.yaml_code)


    def slow_destroy(self):
        self.config_file_frame.destroy()
        self.init_frame.destroy()
        self.single_action_frame.destroy()
        self.periodic_action_frame.destroy()
        self.control_frame.destroy()
        self.link_list_frame.destroy()
        self.root.quit()
        #self.root.destroy()

class ToggleVisibilityButton:
    """
    Class for adding a 'Toggle Visibility' button below a widget. Should only
    be created AFTER the widget has been gridded.
    """

    def __init__(self, widget):

        tog_vis_button = Button(
            #widget.winfo_toplevel(),
            widget.master,
            text="Toggle Visibility",
            command=self.toggle_visibility,
        )
        tog_vis_button.grid(
            row=widget.grid_info()['row']+widget.grid_info()['rowspan'],
            column=widget.grid_info()['column'],
        )

        self.visible = True
        self.widget = widget

    def toggle_visibility(self):
        """
        Toggle visibility of widget
        """

        if self.visible:
            self.widget.grid_remove()
        else:
            self.widget.grid()

        self.visible = not self.visible


class ScrollableListbox:
    """
    Class for creating and managing Scrollable Listboxes as a single object,
    rather than managing a listbox and a scrollbar object.
    """
        
    def __init__(self, parent_frame, **args):

        # Listbox and Scrollbar will be placed in a single, local frame
        self.local_frame = Frame(parent_frame)

        # All 
        self.listbox = Listbox(self.local_frame, **args)
        self.listbox.grid(row=0, column=0, sticky="we")


        self.listbox_scrollbar = Scrollbar(self.local_frame)
        self.listbox_scrollbar.grid(row=0, column=1, sticky="wns", padx=(0,5))

        self.listbox.config(yscrollcommand = self.listbox_scrollbar.set)
        self.listbox_scrollbar.config(command = self.listbox.yview)


    def get(self, *pos_args, **kwd_args):
        return self.listbox.get(*pos_args, **kwd_args)


    def delete(self, *pos_args, **kwd_args):
        return self.listbox.delete(*pos_args, **kwd_args)


    def insert(self, *pos_args, **kwd_args):
        return self.listbox.insert(*pos_args, **kwd_args)


    def curselection(self, *pos_args, **kwd_args):
        return self.listbox.curselection(*pos_args, **kwd_args)


    def grid(self, *pos_args, **kwd_args):
        """
        Grid the local frame, which contains the listbox and scrolbar widgets
        """
        return self.local_frame.grid(*pos_args, **kwd_args)


class ConfigFileGUI:

    def __init__(self, frame, parent):
        """
        For this GUI class I chose to keep reference many of the variables by
        their parent, rather than by self, since there are a lot of them and
        the variables across multiple categories.
        """

        self.frame = frame
        self.font = parent.font
        self.parent = parent
        self.config_setup()

    def config_setup(self):
        """
        Setup portion of GUI used to manage importing and exporting of
        configurations.
        """

        import_button = Button(self.frame,
                               text = "Import Configuration",
                               command = self.get_and_import_configuration,
                               font = self.font)


        export_button = Button(self.frame,
                               text = "Export Configuration",
                               command = self.export_configuration,
                               font = self.font)
        
        import_button.grid(row=0, column=0)
        export_button.grid(row=0, column=1)


    def get_and_import_configuration(self):
        """
        Import a user configuration file.
        """

        filename = StringVar(self.frame)
        open_file(filename)

        # Import configuration if filename is not empty (user cancelled)
        if filename.get():
            self.parent.import_configuration(filename.get())


    def export_configuration(self):
        """
        Export a user configuration file.
        """

        filename = StringVar(self.frame)
        save_file(filename)

        # # Export configuration if filename is not empty (user cancelled)
        if filename.get():

            #pdb.set_trace()
            self.parent.update_yaml_code()
            with open(filename.get(), 'w') as stream:
                stream.write(dump_yaml(self.parent.yaml_code))

class ControlGUI:

    def __init__(self, frame, parent):

        self.frame = frame
        self.root = parent.root
        self.font = parent.font
        self.in_test = parent.in_test
        self.parent = parent

        self.control_setup()


    def control_setup(self):
        """
        Setup portion of the GUI used to manage test flow/control (e.g. exiting
        test, starting test, etc.)
        """

        exit_button = Button(
            self.frame,
            text = "Exit Test",
            command = lambda : self.exit_gui(start_test=False),
            font = self.font
        )

        if self.in_test:
            start_button = Button(
                self.frame,
                text = "Start Test",
                command = lambda : self.exit_gui(start_test=True),
                font = self.font
            )

        exit_button.grid(row=0, column=0)
        if self.in_test:
            start_button.grid(row=0, column=1)


    def exit_gui(self, start_test = False):
        """
        Exit the GUI
        """

        self.parent.start_test = start_test
        self.parent.root.quit()
        self.parent.slow_destroy()
        # Due to segmentation fault of unknown cause on hubdev with python2
        # (but not on laptop with python3), minimizing GUI is the best I can
        # do, rather than destroying window
        self.parent.root.wm_state('iconic')
        self.parent.root.quit()
        #self.parent.root.destroy()


class InitOptionsGUI:

    def __init__(self, frame, parent):
        self.frame = frame 
        self.font = parent.font

        self.prbs_select = parent.prbs_select
        self.dfe = parent.dfe
        self.mgt_equ = parent.mgt_equ
        self.pre_emphasis = parent.pre_emphasis
        self.post_emphasis = parent.post_emphasis
        self.swing = parent.swing
        self.loopback = parent.loopback

        self.prbs_select_setup()
        self.dfe_setup()
        self.mgt_equ_setup()
        self.pre_emph_setup()
        self.post_emph_setup()
        self.swing_setup()
        self.loopback_setup()

    def prbs_select_setup(self):

        prbs_options = ["PRBS-31", "PRBS-23", "PRBS-15", "PRBS-7", "OFF"]

        prbs_dropdown = OptionMenu(self.frame,
                                   self.prbs_select,
                                   *prbs_options)
        prbs_dropdown.configure(width=10)
        prbs_dropdown['font'] = self.font

        prbs_label = Label(self.frame,
                           text="PRBS-Select:",
                           font=self.font)

        prbs_label.grid(row=0, column=0, sticky=E)
        prbs_dropdown.grid(row=0, column=1, columnspan=2, sticky=W+E)


    # On/off for DFE
    def dfe_setup(self):

        dfe_rb_off = Radiobutton(self.frame,
                                 text="OFF",
                                 value="OFF",
                                 variable=self.dfe,
                                 font=self.font)

        dfe_rb_on = Radiobutton(self.frame,
                                text="ON",
                                value="ON",
                                variable=self.dfe,
                                font=self.font)

        dfe_label_str = StringVar()
        dfe_label_str.set("DFE")
        dfe_label = Label(self.frame,
                          text= "DFE:",
                          font=self.font)

        dfe_label.grid(row=1, column=0, sticky=E)
        dfe_rb_on.grid(row=1, column=1, sticky=W)
        dfe_rb_off.grid(row=1, column=2, sticky=W)


    # On/off for MGT Equalizer
    def mgt_equ_setup(self):

        mgt_equ_rb_off = Radiobutton(self.frame,
                                     text="OFF",
                                     value="OFF",
                                     variable=self.mgt_equ,
                                     font=self.font)

        mgt_equ_rb_on = Radiobutton(self.frame,
                                    text="ON",
                                    value="ON",
                                    variable=self.mgt_equ,
                                    font=self.font)

        mgt_equ_label = Label(self.frame,
                              text="MGT Equalizer:",
                              font=self.font)

        mgt_equ_label.grid(row=4, column=0, sticky=E)
        mgt_equ_rb_off.grid(row=4, column=2, sticky=W)
        mgt_equ_rb_on.grid(row=4, column=1, sticky=W)


    # Input for pre-emphasis
    def pre_emph_setup(self):

        pre_emphasis_spinbox = Spinbox(self.frame, from_=0, to=1080,
                                       textvariable=self.pre_emphasis,
                                       width=5,
                                       font=self.font)

        pre_emph_label = Label(self.frame,
                               text="Pre-Emphasis:",
                               font=self.font)

        pre_emph_label.grid(row=5, column=0, sticky=E)
        pre_emphasis_spinbox.grid(row=5, column=1, sticky=W, columnspan=3)


    # Input for post-emphasis
    def post_emph_setup(self):

        post_emphasis_spinbox = Spinbox(self.frame, from_=0, to=1080,
                                        textvariable=self.post_emphasis,
                                        width=5,
                                        font=self.font)

        post_emph_label = Label(self.frame,
                                text="Post-Emphasis:",
                                font=self.font)

        post_emph_label.grid(row=6, column=0, sticky=E)
        post_emphasis_spinbox.grid(row=6, column=1, sticky=W, columnspan=3)


    # Input for swing
    def swing_setup(self):

        swing_spinbox = Spinbox(self.frame, from_=0, to=1080,
                                textvariable=self.swing,
                                width=5,
                                font=self.font)

        swing_label = Label(self.frame,
                            text="Swing:",
                            font=self.font)

        swing_label.grid(row=7, column=0, sticky=E)
        swing_spinbox.grid(row=7, column=1, sticky=W, columnspan=3)

    def loopback_setup(self):
        loopback_options = ["Normal Operation", "Near-End PCS", "Near-End PMA",
                           "Far-End PMA", "Far-End PCS"]

        loopback_dropdown = OptionMenu(self.frame,
                                       self.loopback,
                                       *loopback_options)
        loopback_dropdown.configure(width=15)
        loopback_dropdown['font'] = self.font

        loopback_label = Label(self.frame,
                           text="Loopback:",
                           font=self.font)

        loopback_label.grid(row=8, column=0, sticky=E)
        loopback_dropdown.grid(row=8, column=1, columnspan=2, sticky=W+E)




class ActionListGUI:

    def __init__(self, frame, parent, periodic = False):
        """
        Setup portion of GUI to be used to manage action list (used for setting
        up both single and periodic action lists)
        """
        self.font = parent.font

        if periodic:
            self.action_list = parent.periodic_action_list
            self.start_time = parent.period_start_time
            self.period_length = parent.period_length
            self.no_of_iterations = parent.no_of_iterations

        else:
            self.action_list = parent.single_action_list
            self.start_time = None
            self.period_length = None
            self.no_of_iterations = None

        self.listbox_frame = Frame(frame)
        self.options_frame = Frame(frame)
        self.listbox_frame.grid(row=0, column=0)
        self.options_frame.grid(row=0, column=1)
        
        self.action_listbox_setup()
        self.action_options_setup()

        if periodic:
            self.periodic_action_options_setup()

    def action_listbox_setup(self):

        self.action_listbox = ScrollableListbox(
            self.listbox_frame,
            listvariable = self.action_list,
            font=self.font,
            selectmode="multiple"
        )

        action_label = Label(self.listbox_frame,
                             text="Action List",
                             font=self.font)

        action_label.grid(row=0, column=0,
                          sticky="we")

        self.action_listbox.grid(row=1, column=0,
                                 sticky="we")

    def action_options_setup(self):

        delete_button = Button(
            self.options_frame,
            text = "Remove Selected",
            font=self.font,
            command = lambda : delete_multiple_select(self.action_listbox) 
        )

        possible_actions = g_action_map.keys()

        action_add_remove_label = Label(self.options_frame,
                                        text="Add or Remove Actions",
                                        font=self.font)

        add_action = StringVar()
        add_action.set("")

        action_dropdown = OptionMenu(self.options_frame,
                                     add_action,
                                     *possible_actions)

        action_dropdown.configure(width=20)
        action_dropdown['font'] = self.font

        add_button = Button(
            self.options_frame,
            text = "Add Action:",
            font=self.font,
            command = lambda : add_non_empty_item(self.action_listbox,
                                                  add_action)
        )


        action_add_remove_label.grid(row=0, column=5, columnspan=3)
        delete_button.grid(row=1, column=5, columnspan=2, sticky="we")
        add_button.grid(row=2, column=5, columnspan=2, sticky="we")
        action_dropdown.grid(row=3, column=5, columnspan=2, sticky="we")

    def periodic_action_options_setup(self):

        periodic_label = Label(self.options_frame,
                               text="Periodic Options",
                               font=self.font)

        start_time_label = Label(self.options_frame,
                                 text="Start Time:",
                                 font=self.font)

        start_time_spinbox = Spinbox(self.options_frame,
                                     textvariable=self.start_time,
                                     width=8,
                                     font=self.font,
                                     from_=0,
                                     to=604800)

        period_length_label = Label(self.options_frame,
                                    text="Period Length:",
                                    font=self.font)

        period_length_spinbox = Spinbox(self.options_frame,
                                        textvariable=self.period_length,
                                        width=8,
                                        font=self.font,
                                        from_=0,
                                        to=604800)

        no_of_iterations_label = Label(self.options_frame,
                                    text="# of Iterations:",
                                    font=self.font)

        no_of_iterations_spinbox = Spinbox(self.options_frame,
                                        textvariable=self.no_of_iterations,
                                        width=8,
                                        font=self.font,
                                        from_=0,
                                        to=604800)

        periodic_label.grid(row=4, column=5, columnspan=2, sticky="we")
        start_time_label.grid(row=5, column=5, sticky="e")
        start_time_spinbox.grid(row=5, column=6)
        period_length_label.grid(row=6, column=5, sticky="e")
        period_length_spinbox.grid(row=6, column=6)
        no_of_iterations_label.grid(row=7, column=5, sticky="e")
        no_of_iterations_spinbox.grid(row=7, column=6)


class LinkListGUI:

    def __init__(self, frame, parent):
        self.frame = frame
        self.parent = parent
        self.font = parent.font
        self.small_font = parent.small_font

        self.link_list = parent.link_list
        self.all_link_list = parent.all_link_list
        self.mgt_link_definitions = parent.mgt_link_definitions

        self.link_list_setup()
        self.link_list_management_setup()
        self.link_list_group_setup()
    
    def link_list_setup(self):
        """
        Setup portion of GUI to be used to manage the link list.
        """

        link_list = self.link_list

        self.link_listbox = ScrollableListbox(self.frame,
                                              listvariable = self.link_list,
                                              selectmode="multiple",
                                              font=self.small_font,
                                              width=36)

        link_list_label = Label(self.frame,
                                text="Link List",
                                font=self.font)

        link_list_label.grid(row=0, column=0, sticky="we")
        self.link_listbox.grid(row=1, rowspan=5, column=0, sticky="nsew")

    def link_list_management_setup(self):

        link_add_remove_label = Label(self.frame,
                                             text="Add or Remove Links",
                                             font=self.font)
        delete_sel_button = Button(
            self.frame,
            text = "Remove Selected",
            font=self.font,
            command = lambda : delete_multiple_select(self.link_listbox)
        )

        link_add_remove_by_name_label = Label(self.frame,
                                              text="Add/Remove By Name:",
                                              font=self.font)
        sel_link = StringVar()
        sel_link.set("")

        link_combobox = SearchableCombobox(self.frame,
                                           textvariable=sel_link,
                                           values=self.all_link_list)

        link_combobox.configure(width=35, font=self.small_font)

        add_name_button = Button(
            self.frame,
            text = "Add",
            font=self.font,
            command = lambda : self.add_link(sel_link)
        )

        delete_name_button = Button(
            self.frame,
            text = "Remove",
            font=self.font,
            command = lambda : self.remove_link(sel_link)
        )

        link_add_remove_label.grid(row=0, column=1, columnspan=2, padx=5)
        delete_sel_button.grid(row=1, column=1, columnspan=2, sticky="we", padx=5)
        link_add_remove_by_name_label.grid(row=2, column=1, columnspan=2, padx=5)
        link_combobox.grid(row=3, column=1, columnspan=2, sticky="we", padx=5)
        add_name_button.grid(row=4, column=1, columnspan=1, sticky="we", padx=5)
        delete_name_button.grid(row=4, column=2, columnspan=1, sticky="we", padx=5)

    def link_list_group_setup(self):
        """
        Setup portion of GUI to be used for managing link list by group/device,
        rather than individual links.
        """

        group_management_frame = Frame(self.frame)
        group_checkbox_frame = Frame(group_management_frame)

        self.device_use_dict = {}
        for i, device_name in enumerate(self.parent.device_name_list):
            device_name = device_name.lower()
            self.device_use_dict[device_name] = Variable()
            self.device_use_dict[device_name].set(1)
            self.add_device_checkbutton(group_checkbox_frame,
                                        device_name,
                                        self.device_use_dict[device_name],
                                        position=i)

        group_management_label = Label(group_management_frame,
                                       text="Add/Remove by Group:",
                                       font=self.font)

        add_group_button = Button(
            group_management_frame,
            text = "Add Group",
            font=self.font,
            command = lambda : self.add_group(sel_group)
        )

        delete_group_button = Button(
            group_management_frame,
            text = "Remove Group",
            font=self.font,
            command = lambda : self.remove_group(sel_group)
        )

        sel_group = StringVar()
        self.link_group_combobox = ttk.Combobox(group_management_frame,
                                                textvariable=sel_group)
        self.link_group_combobox['values'] = self.parent.yaml_def_file_list 

        group_management_label.grid(row=0, column=0, columnspan=2)
        group_checkbox_frame.grid(row=1, column=0, rowspan=10)
        self.link_group_combobox.grid(row=1, column=1, sticky="nwe")
        add_group_button.grid(row=2, column=1, sticky="nwe")
        delete_group_button.grid(row=3, column=1, sticky="nwe")

        group_management_frame.grid(row = 10, column=0, columnspan=3)



    def add_device_checkbutton(self, frame, device_id, device_intvar, position,
                               no_per_row=4):

        device_checkbutton = Checkbutton(frame,
                                         text = device_id,
                                         variable = device_intvar,
                                         onvalue = 1,
                                         offvalue= 0,
                                         height = 1,
                                         width = 6)

        row = int(position/no_per_row)
        column = int(position%no_per_row)
        device_checkbutton.grid(row=row, column=column)


    def add_group(self, yaml_file):
        # Convert yaml_file to string (may be a tkinter StringVar)
        try:
            yaml_file_str = yaml_file.get()
        except AttributeError:
            yaml_file_str = str(yaml_file)

        if yaml_file_str:
            group_def_yaml = get_yaml(MGTDEFPATH + yaml_file_str)
            group_def_dict = dict(group_def_yaml)
            for link_name in group_def_dict.keys():
                link_devices_present = True
                link_source =  group_def_dict[link_name]["source"]
                if link_source is not None and link_source != "None":
                    card_id = link_source["card"]
                    if card_id is not None and card_id != "None":
                        if not self.device_use_dict[card_id].get():
                            link_devices_present = False

                link_dest =  group_def_dict[link_name]["dest"]
                if link_dest is not None and link_dest != "None":
                    card_id = link_dest["card"]
                    if card_id is not None and card_id != "None":
                        if not self.device_use_dict[card_id].get():
                            link_devices_present = False

                if link_devices_present:
                    self.add_link(link_name)


    def remove_group(self, yaml_file):
        # Convert yaml_file to string (may be a tkinter StringVar)
        try:
            yaml_file_str = yaml_file.get()
        except AttributeError:
            yaml_file_str = str(yaml_file)

        if yaml_file_str:
            group_def_yaml = get_yaml(MGTDEFPATH + yaml_file_str)
            group_def_dict = dict(group_def_yaml)
            for link_name in group_def_dict.keys():
                tx_device_present = True
                rx_device_present = True

                link_source =  group_def_dict[link_name]["source"]
                if link_source is not None and link_source != "None":
                    card_id = link_source["card"]
                    if card_id is not None and card_id != "None":
                        if not self.device_use_dict[card_id].get():
                            tx_device_present = False

                link_dest =  group_def_dict[link_name]["dest"]
                if link_dest is not None and link_dest != "None":
                    card_id = link_dest["card"]
                    if card_id is not None and card_id != "None":
                        if not self.device_use_dict[card_id].get():
                            rx_device_present = False

                if tx_device_present or rx_device_present:
                    self.remove_link(link_name)


    def add_link(self, link_name):
        """
        Add link to link_listbox, checking that link is not already in
        link_listbox, that link is a valid link (i.e. defined in
        'mgt_link_definitions.yaml'), then update order of link_listbox.
        """
        # Convert link_name to string (may be a tkinter StringVar)
        try:
            link_name_str = link_name.get()
        except AttributeError:
            link_name_str = str(link_name)

        if link_name_str not in self.link_listbox.get(0, "end"):
            if link_name_str in self.all_link_list:
                self.link_listbox.insert("end", link_name_str)
                self.link_list_update(self.link_listbox)


    def remove_link(self, link_name):
        """
        Remove link_name from link_listbox, if link_name is in link_listbox
        """
        # Convert link_name to string (may be a tkinter StringVar)
        try:
            link_name_str = link_name.get()
        except AttributeError:
            link_name_str = str(link_name)

        selected_links = self.link_listbox.get(0, "end")
        if link_name_str in selected_links:
            i = selected_links.index(link_name_str)
            self.link_listbox.delete(i)


    def update_link_combobox(self, link_combobox, link_value):
        """
        Update link_combobox so that only options are those for which
        link_value is a substring
        """

        link_value_str = link_value.get().lower()

        if link_value_str == '':
            data = self.all_link_list 
        else:
            data = []
            for item in self.all_link_list:
                if link_value_str in item.lower():
                    data.append(item)                

        link_combobox['values'] = data


    def link_list_update(self, link_listbox):
        """
        Update link_listbox so that links are in same order as defined in
        'mgt_link_definitions.yaml'
        """

        # Order links in the same way as in 'mgt_link_definitions.yaml'
        # will need to be updated with change to how yaml files are handled
        data = link_listbox.get(0, "end")
        sorted_data = sorted(data,
                             key = self.list_index)
        link_listbox.delete(0, "end")
        for link in sorted_data:
            link_listbox.insert("end", link)

    def list_index(self, link):
        """
        Return index of link in the ordered list of all links, or the length of
        the list of all lengths if link is not present in the full link list
        (i.e., if not in the full link list, place at end)
        """
        if link in self.all_link_list: 
            return self.all_link_list.index(link)
        else:
            return len(self.all_link_list)


class SearchableCombobox(ttk.Combobox):
    """
    Class for creating and managing 'searchable' Comboboxes. Here 'searchable'
    means that the list of options available to select reduces to only those
    for which the current entry is a substring
    """

    def __init__(self, frame, textvariable, values, **kwd_args):

        self.all_values = values

        try:
            super().__init__(frame, textvariable=textvariable, **kwd_args)
        except: # Catch differences between python2.7 and python3
            ttk.Combobox.__init__(self, frame, textvariable=textvariable, **kwd_args)

        self['values'] = self.all_values 

        # Trace changes in combobox, update list
        textvariable.trace_variable(
            mode = ('wu'), # Trace on 'write' or 'unset'
            callback = lambda *args : self.update_combobox(textvariable)
        )


    def update_combobox(self, value):
        """
        Update combobox so that only options are those for which
        link_value is a substring
        """

        value_str = value.get().lower()

        if value_str == '':
            data = self.all_values 
        else:
            data = []
            for item in self.all_values:
                if item: # Make sure item is not None
                    if value_str in item.lower():
                        data.append(item)                

        self['values'] = data


def delete_multiple_select(listbox):
    """
    Delete selected items from listbox. Works also for multiple select mode
    listboxes.
    """
    selected_indices = listbox.curselection()

    # Delete items by index from greatest to least, so that removing one item
    # does not change the position of the others to be deleted
    for index in selected_indices[::-1]:
        listbox.delete(index)


def add_non_empty_item(listbox, item):
    
    """
    Add item to listbox if item is not empty. If an item in the listbox is
    selected, add item after selection (after the first item, for a multiple
    selection).
    """

    selected_indices = listbox.curselection()
    item_str = item.get()

    if item_str:

        if selected_indices:
            listbox.insert(selected_indices[0], item_str)

        else:
            listbox.insert("end", item_str)


def open_file(filename, initialdir = "../UserConfig"):
    """
    Allow user to set filename to that of an existing file (the file is not
    actually opened in this function)
    """

    filetypes = (("yaml Files", "*.yaml"), ("all files", "*.*"))

    filename_temp = filedialog.askopenfilename(initialdir = initialdir,
                                               title = "Select a File",
                                               filetypes = filetypes)

    filename.set(filename_temp)


def save_file(filename, initialdir = "../UserConfig"):
    """
    Allow user to set filename to that of an existing or new file (the file
    is not actually saved in this function)
    """

    filetypes = (("yaml Files", "*.yaml"), ("all files", "*.*"))

    filename_temp = filedialog.asksaveasfilename(
        initialdir = initialdir,
        title = "Select a File",
        filetypes = filetypes,
        defaultextension = filetypes,
    )

    filename.set(filename_temp)
    


def main():

    # Create top level widget - the main window
    root = Tk()
    root.title("IBERT IPBus GUI - %s"%version)
    #root.geometry("900x900+1000+1000")
    # First part (NNNxMMM) determines window size - If I do this, frame won't expand to fit widgets
    # Second part (+NNN+MMM) determines y and x offset position of window 

    # Establish Confiugration Parameters/Variables
    ibert_window = IbertConfigGUI(root)

    root.mainloop()

if __name__ == "__main__":

    main()
