import os
import sys
import time
IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
EYEPATH = IBERTPATH+"/EyeAnalysis/"
sys.path.append(IBERTPATH+'/PrivateModules/')
sys.path.append(IBERTPATH+'/Tools/')
sys.path.append(EYEPATH)
from cmd_line_args_class import ArgGetter
from log_class import Log
from ibert_management_classes import Devices
from yaml_functions import get_yaml, is_yaml_none
from gt_mgt_conversion import MGTProps

g_arg_tup_list = [
    ("-d", "--device"),
    ("-q", "--quad"),
    ("-c", "--channel"),
]

g_arg_tup_list_bool = [
    ("-h", "--help"),
]

def select_mgt_ch(card_dev, gt_ch_int, dev_type, log):
    # Step 1 - Select MGT Ch
    log.write("Setting mgtchselect to %s\n"%(gt_ch_int))
    log.flush()
    if dev_type == "rod":
        card_dev["versions"]["mgt_select"]["mgtchselect"].write(gt_ch_int)
    else:
        card_dev["csr"]["mgt_select"]["mgtchselect"].write(gt_ch_int)


def lower_eye_go(rx_mgt, log):
    # Step 2/7 - Set eye_go to 0
    log.write("Lowering eye_go\n")
    log.flush()
    rx_mgt["eye_control"]["eye_go"].lower_bits()


def raise_eye_go(rx_mgt, log):
    log.write("Raising eye_go.\n")
    log.flush()
    rx_mgt["eye_control"]["eye_go"].raise_bits()


def issue_eye_reset(rx_mgt, log, max_no_of_resets=5, wait_time=3):
    # Steps 3+4 - issue reset using eye_rstrtl, check if eye_datardy is low
    log.write("Issuing eye diagram reset, waiting for eye_datardy low ...\n")
    log.flush()
    i = 0
    while i < max_no_of_resets:
        rx_mgt["eye_control"]["eye_rstrtl"].blip(wait_time=wait_time)
        log.flush()
        if not rx_mgt["eye_status"]["eye_datardy"].read():
            break
        i += 1

    if i >= max_no_of_resets: 
        log.write("WARNING: Eye_datardy not low after %s reset attempts!\n"%
                           max_no_of_resets)

    log.write("Eye_datardy is low after %s reset attempts.\nn"%(i+1))
    log.flush()


def wait_for_eye_data_ready(rx_mgt, log, timeout=10):
    start_time = time.time()
    elapsed_time = time.time() - start_time
    log.write("Waiting for eye_datardy high (timeout of {:.2f} seconds) ...\n".format(timeout))
    log.flush()
    while not rx_mgt["eye_status"]["eye_datardy"].read():
        time.sleep(1)
        elapsed_time = time.time() - start_time
        if elapsed_time >= timeout:
            break
    if elapsed_time >= timeout:
        log.write("WARNING: Eye data not ready after %s seconds!\n"%(timeout))

    return elapsed_time


def eye_realign(gt_id, log, mgt_type=None):
    log.write("Performing realignment.\n")
    log.flush()

    devices = Devices()
    mgt = devices[gt_id]
    eye_ctrl = mgt["eye_control"]
    time.sleep(0.05)
    eye_ctrl["eye_drpaddrreg"].write(0x4f)
    time.sleep(0.05)
    eye_ctrl["eye_drpdireg"].write(0x8800)
    time.sleep(0.05)
    eye_ctrl["eye_drpwecmd"].write(0x1)
    time.sleep(0.05)
    eye_ctrl["eye_sofrs"].write(0x1)
    time.sleep(0.05)
    eye_ctrl["eye_drpwecmd"].write(0x0)
    time.sleep(0.05)
    eye_ctrl["eye_drpaddrreg"].write(0x4f)
    time.sleep(0.05)
    eye_ctrl["eye_drpdireg"].write(0x8000)
    time.sleep(0.05)
    eye_ctrl["eye_drpwecmd"].write(0x1)
    time.sleep(0.05)
    eye_ctrl["eye_sofrs"].write(0x0)
    time.sleep(0.05)
    eye_ctrl["eye_drpwecmd"].write(0x0)
    time.sleep(0.05)


def set_prescale(rx_mgt, log, value=0x0):
    log.write("Setting prescale to %s\n"%(value))
    rx_mgt["eye_pre_scale"].write(value)
    log.flush()


def get_timeout_value(prescale):
    """
    Get timeout value, in seconds, for the specified prescale value
    """
    return (prescale+1)*10


def eye_collection_process(mgt_props, log=None, prescale=0):
    if log is None:
        log = Log()

    gt_id = mgt_props.gt_id
    gt_ch_int = mgt_props.gt_ch
    card = mgt_props.device

    devices = Devices()
    card_dev = devices[card]
    rx_mgt = devices[gt_id]

    log.write("-"*70 + "\n")
    log.write("Attempting to get mgt_cdr_locked low for: %s\n"%(mgt_props))
    log.write("-"*70 + "\n")
    log.flush()


    log.write("Beginning collection process.\n\n")
    log.flush(timestamped=True)
    set_prescale(rx_mgt, log, value=prescale)
    select_mgt_ch(card_dev, gt_ch_int, mgt_props.device_type, log)

    max_no_of_realignments = 100
    no_of_realignments = 0
    while no_of_realignments < max_no_of_realignments:
        lower_eye_go(rx_mgt, log)
        issue_eye_reset(rx_mgt, log)
        raise_eye_go(rx_mgt, log)

        min_time = 1.5
        max_time = get_timeout_value(prescale)
        elapsed_time = wait_for_eye_data_ready(rx_mgt, log, timeout=max_time)

        if elapsed_time < min_time:
            log.write("Eye data ready in less than {:.2f} seconds. Will perform realignment.\n\n".format(min_time))
        elif elapsed_time >= max_time:
            log.write("Timeout of {:.2f} seconds reached waiting for eye data ready. Will perform realignment anyway.\n\n".format(max_time))
        else:
            log.write("Eye data ready in {:.2f} seconds. Will perform realignment anyway.\n\n".format(elapsed_time))

        if rx_mgt["common_status.mgt_cdr_locked"] == 0:
            log.write("[END]: MGT CDR Locked bit is low! Exiting")
            sys.exit()
        else:
            log.write("[CONTINUE]: MGT CDR Locked bit is not low. Will perform realignment then try again.\n\n")

        log.write("Lowering eye_go, then performing realignment.\n")
        log.flush()
        lower_eye_go(rx_mgt, log)
        eye_realign(gt_id, log)
        log.write("Realignment finished. Reattempting collection process.\n\n")
        log.flush(timestamped=True)

    log.write("WARNING: Maximum number of realignments (50) reached, but mgt_cdr_locked bit is still high.")


def main():
    cmd_line_args = ArgGetter(
        arg_tup_list = g_arg_tup_list,
        arg_tup_list_bool = g_arg_tup_list_bool,
    )

    # Get Device/MGT info
    device = cmd_line_args["--device"]
    channel = cmd_line_args["--channel"]
    quad = cmd_line_args["--quad"]
    try:
        mgt_props = MGTProps(device=device, channel=channel, quad=quad)
    except ValueError:
        raise ValueError("Error while getting MGT properties. Double check that necessary and correct information was specified (may be missing channel, or invalid quad/channel specified)")

    eye_collection_process(mgt_props)

if __name__ == "__main__":
    main()
