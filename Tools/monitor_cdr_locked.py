import time
import sys
import os
IBERTPATH = os.path.realpath(os.path.dirname(os.path.realpath(__file__))+"/../")
sys.path.append(IBERTPATH+'/PrivateModules/')
from ibert_test_suite import Manager, Devices
from cmd_line_args_class import ArgGetter
from log_class import Log
from yaml_functions import get_yaml
import gt_mgt_conversion as gmc
import pdb

if __name__ == "__main__":

    cmd_line_args = ArgGetter(
        arg_tup_list = [
            ("-l", "--log_name"),
            ("-c", "--user_config")
        ],
        arg_tup_list_bool = [
            ("-h", "--help"),
        ],
    )
    if cmd_line_args["--help"]:
        print("monitor_cdr_locked.py\n"
              "  -h | --help\n"
              "  -l | --log_name\n"
              )
        sys.exit()

    log = Log(cmd_line_args["--log_name"])

    if cmd_line_args["--user_config"] is not None:
        config = get_yaml(cmd_line_args["--user_config"])
    else:
        config = get_yaml(IBERTPATH+"/UserConfig/full_ibert_test.yaml")
    config.update(get_yaml(IBERTPATH+"/device_config.yaml"))

    manager = Manager(user_conf=config, dev_conf=config, used_links_only=True, used_devs_only=True)
    devices = Devices()

    cdr_locked_low_dict = {}
    for mgt in manager.gt_conf.get_rx_gts():
        cdr_locked_low_dict[mgt] = False
      
   
    wait_time = 10
    log.write("Beginning search for MGT CDR Locked low - scanning every %s seconds"%(wait_time))
    log.flush()
    while True:
        for gt_id in manager.gt_conf.get_rx_gts():
            cdr_locked = devices[gt_id]["common_status.mgt_cdr_locked"].read()
            if gmc.MGTProps(gt_id=gt_id).mgt_type == "Ultrascale GTY" and cdr_locked == 0:
				if not cdr_locked_low_dict[gt_id]:
					cdr_locked_low_dict[gt_id] = True
					log.write("GT %s - CDR Locked Low Found At Time %s\n"%(gt_id, time.strftime("%B %d, %H:%M")))
					log.flush()
        time.sleep(wait_time)



