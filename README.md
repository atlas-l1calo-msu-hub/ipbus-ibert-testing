
[[_TOC_]]
# Publishing Info

Project Rev: Release 6, Version 7 (R6V7)

README Rev: June 14th, 2021

Author: Spencer Lee (leespen1@msu.edu)

# Overview

The purpose of this project is to provide tools for conducting IBERT testing of
devices over ethernet using IPBus, and also to facilitate other functions which
require the use of IPBus.

# Important Files/Scripts

## ibert_ipbus_test.py

The main script used for conducting IBERT tests. User configuration files (which
will be described in further detail later on) control the behavior of the script
by specifying which links to include in the test, which settings to use on each
device/transceiver, and which 'actions' to perform.

```
Usage: python ibert_ipbus_test.py   
                                       -h|--help
                                       -c|--user_config_file
                                       -g|--gui
                                       -d|--device_config_file
                                       -l|--log_name
                                       -m|--message
                                       --hub1_SN
                                       --hub2_SN

Options:

 -h, --help                            Display this message and exit
 -c, --user_config_file [file name]    Specify test configuration file     
 -g, --gui                             Use GUI to manage user config settings
                                         (alternative to specifying file)
 -d, --device_config_file [file name]  Specify a device configuration file    
                                         (if not specified, defaults to
                                         <IBERTAREA>/device_config.yaml
 -l, --log_name [log name]             Log output to: 
                                         <IBERTAREA>/Results/[log name]
 -m, --message                         Message to include at beginning of log
 --hub1_SN [SN]                        For log header
 --hub2_SN [SN]                        For log header
```

## device_config.yaml

This is the default file used to determine which IBERT-accesible devices to use
in the test. For each generic device (e.g. hub1, hub2, htm3, htm4, etc), the
user specifies a 'ConnHub ID' for that device, which must be one of the
connection ids in `<IBERTAREA>/Xml/ConnHub.xml`, or 'None' (in the case that no
device is present, or a device is present but should not be included in the
test). This will determine the IP address used to talk to each device, as well
as the expected structure of the registers for each device.

Can be edited using a standard text editor, or using the GUI script at
`<IBERTAREA>/Tools/device_gui.py`. The GUI is especially useful in that it
allows the user to select ConnHub IDs from the actual IDs in
`<IBERTAREA>/Xml/ConnHub.xml`.

## User Configuration Files

Files used to determine settings for an IBERT test. Can be edited using a text
editor, but in order to make sure valid values are specified for each parameter,
it is recommended to manage configuration files using the GUI script at
`<IBERTAREA>/Tools/gui.py`. User configuration files include the following
parameters to be set:

`'device_init_parameters'`: Specifies settings which are set 'per-card', rather
than 'per-link/transceiver', to be used during an `'Initialize'` action.
- `'mgt equalizer'`: Specifies whether to turn on the mgt equalizer for a card.
  Valid options are `'ON'` and `'OFF'`

`'link_init_parameters'`: Specifies settings which are set 'per-link/transceiver',
rather than 'per-card', to be used during an `'Initialize'` action.
- `'prbs-select'`: Which prbs pattern to use for each transmitter/receiver. Valid
  values are `'PRBS-31/23/15/7'` and `'OFF'`
- `'DFE'`: Whether to enable DFE. Valid values are `'ON'` and `'OFF'`
- `'pre-emphasis'`: Value to use for TX pre-emphasis control, in dB. Typical
  values range from ~0.0-6.0
- `'post-emphasis'`: Value to use for TX post-emphasis contorl, in dB. Typical
  values range from ~0.0-14.0
- `'swing'`: Value to use for driver swing control, in mV (PPD). Typical values
  range from ~170-1120
- `'loopback'`: Specifies loopback behavior for MGT. Valid values are:
    - `Normal Operation''`
    - `'Near-End PCS'`
    - `'Near-End PMA'`
    - `'Far-End PMA'`
    - `'Far-End PCS'`


Note that although these parameters are expected for every user configuration
file, cards and transceivers will not be initialized with these settings unless
an `'Initialize'` action is performed.

Also note that although the user can specify any number for the `'pre-emphasis'`,
`'post-emphasis'`, and `'swing'` parameters, there are a limited number of discrete
values these parameters can actually take on at the hardware level. Moreover,
these discrete values differ between each type of MGT transceiver, so these
values are rounded to the nearest available value for each MGT transceiver type.
For example, a specified swing value of `'190'` mV will be translated to a swing
value of 191 mV for an Ultrascale GTY transceiver, and 269 mV for a 7 Series GTX
transceiver.

`'single_action_list'`: Specifies actions to perform once. Actions vary in
function, and will be described in more detail further on.

Valid actions are:
- `'Initialize'`
- `'Initialize All'`
- `'Reset Errors'`
- `'Reset Errors All'`
- `'Full Report'`
- `'Get Card Status'`
- `'Get DCDC Status'`
- `'Check Init Done'`
- `'Check RX Locked'`
- `'Check Common Status'`
- `'Check RX Status'`
- `'Check TX Status'`
- `'Report Errors'`
- `'Inject Errors'`
- `'Find Link Routes'`
- `'Get GT Registers'`
- `'Collect Eye Diagrams'`
- `'Get Optical Power'`

`'periodic_action_list'`: Specifies actions to perform once or more, periodically,
according to the parameters in `'periodic_action_timing'`.

`'periodic_action_timing'`: Specifies timing parameters determining periodic
action behavior.
- `'start time'`: Time in seconds to wait before performing the first iteration of
  periodic actions
- `'period'`: Time in seconds to wait between performing consecutive iterations of
  periodic actions
- `'no of iterations'`: Number of iterations of periodic actions to perform

`'link_list'`: Specifies which links to include in the test. Links are defined in
`<IBERTAREA>/LinkConfiguration/MGTDefinitions`, and specify the links source
transmitter MGT, and destination receiver MGT. Transceivers not included in any
of the specified links will not be affected by the test, except by some special
actions (at the moment, `'Initialize All'` and `'Full Report'` affect all
transceivers, even those not included in any link specified in the list). Since
there are many potential links to choose from, it is recommended that the user
only modifies this file be copy/pasting links from other configuration files, or
by using the GUI.

## SummaryGeneration/generate_summary.py

Script for generating a PDF summary from the raw output csv of an IPBus IBERT
test. Sections are generated according to the actions present in a test. That
is, there are no requirements on what actions must be performed in a test, the
report will include or omit that section of the report based on whether or not
the data is found. In the case of duplicate actions, a report is typically only
generated for the last action performed. There are some exceptions, including
the FullReport section, which counts on there being at least 2 FullReport
actions.

Requires pylatex python module (`pip install pylatex`), as well as texlive
software to compile the created LaTeX file.

```
Usage: python generate_summary_pdf.py -i [input_fname] (-t [title]) (-m [message])

                                    -h|--help                    
                                    -i|--unput
                                    -t|--title
                                    -m|--message


Options:

-h, --help                          Display this message and exit
-i, --input                         Specify input file (csv)
-t, --title                         Optional title (if not provided, title will
                                    be determined from input filename)
-m, --message                       Optional message to include in summary
```

## EyeAnalysis/eye_diagram_collector.py

Tool for collecting eye diagrams for a single MGT receiver on a device.
`ibert_ipbus_test.py` should be used to initalize links before using this script
(otherwise, you may be collecting an eye diagram for a broken link).

The `-f` specifies an identifier which will be used for naming the output files
of the script.
```
Usage: python eye_diagram_collector.py -d [device] -c [channel] (-q [quad])

                                            -h|--help
                                            -d|--device
                                            -q|--quad
                                            -c|--channel

Options:

 -h, --help                                  Display this message and exit
 -d, --device [name],                        Specify device
 -q, --quad [#]                              Specify quad number
 -c, --channel [#]                           Channel to collect eye diagram
                                             for. If quad number is provided,
                                             will be used as quad channel.
                                             Otherwise, will be used as GT
                                             channel.
 -f, --file  [file name]                     Identifier to use for output
                                             filenames 
 --raw_only                                  Only collect raw data.
 --show
 -p, --prescale [prescale value]
```

## EyeAnalysis/eye_diagram_plotter.py

Tool for plotting eye diagrams, given a raw data file (as created using
`eye_diagram_collector.py`) as input. 

```
Usage: python eye_diagram_plotter.py -i [input] -d [device] -c [channel] (-q [quad])

                                            -h|--help
                                            -d|--device
                                            -q|--quad
                                            -c|--channel

Options:

 -h, --help                                  Display this message and exit
 -i, --input, --input_file [filename]        Filename of raw input data
 -d, --device [name],                        Specify device
 -q, --quad [#]                              Specify quad number
 -c, --channel [#]                           Channel to collect eye diagram
                                             for. If quad number is provided,
                                             will be used as quad channel.
                                             Otherwise, will be used as GT
                                             channel.
 -f, --file_output  [file name]              Identifier to use for output
                                             filenames 
 -p, --prescale [prescale value]
 -s, --show                                  Whether to display results
                                             immediately.
```
## EyeAnalysis/full_card_collection.sh

Tool for collecting eye diagrams for a range of channels on the same device.

```
Usage: ./full_card_collection_test.sh -d [device] -s [start channel] -e [end channel] -f [file]

-h|--help                   Display this message and exit
-d|--device                 Device to use (hub1, htm3, etc)
-s|--start                  Lower end of GT channels to test
-e|--end                    Upper end of GT channels to test
-f|--file                   Filename to use (not including extension)
```


# Action Descriptions

Implemented Actions:
- "Initialize"
- "Initialize All"
- "Reset Errors"
- "Reset Errors All"
- "Full Report"
- "Get Card Status"
- "Get DCDC Status"
- "Get GBT Status"
- "Check Init Done"
- "Check RX Locked"
- "Check Common Status"
- "Check RX Status"
- "Check TX Status"
- "Report Errors"
- "Inject Errors"
- "Find Link Routes"
- "Get GT Registers"
- "Collect Eye Diagrams"
- "Get Optical Power"

Initialize: Initialize all in-use MGTs (endpoint of a link in the link list)
for present cards with the link parameters specified in the user configuration,
then reset them. Per-card parameters are also set before the MGT resets.

Initialize All: Initialize ALL MGTs on present cards, even if the MGT is not an
endpoint of a link in the link list, with the parameters specified in the user
configuration, and reset them. Per-card parameters are also set before the MGT
resets.

Reset Errors: Reset the error counters for each in-use mgt receiver by raising
and lowering `rx_reset.rxprbscntreset` bitfield.

Reset Errors All: Reset the error counters for all MGT receivers on present
cards by raising and lowering `rx_reset.rxprbscntreset` bitfield

Full Report: Action for obtaining a coomprehensive report of ALL links involving
present cards (including links not specified in user config; utilizes a special
FindLinkRoutes action with all MGT txs and rxs included), including error
counts, bit-error rates, and common/tx/rx status checks.

Get Card Status: Get/Report statuses for each card, including DNA number,
firmware version, etc.

Get DCDC Status: Get a table of all DCDC power supplies for Hub and ROD cards
(ROD data obtained using I2C via the corresponding Hub, but will only be
collected if Hub and ROD are present.)

Get GBT Status: Get/report GBT status (I'm still unsure of what exactly this
data corresponds to).

Check Init Done: Gets/reports `init_done` status for each in-use mgt receiver.
Inherits behavior from the CheckCommonStatus action, but only displays results
pertaining to the `init_done` bit.

Check RX Locked: Gets/reports rxprbslocked status for each in-use mgt receiver.
Inherits behavior from the CheckRXStatus action, but only displays results
pertaining to the rxprbslocked bit. At the moment, functionality is same to
CheckRXStatus, but may differ if CheckRXStatus is expanded to report more
bitfields.

Check Common Status: Get the values of the `common_status` register for all
in-use mgts, and report which MGTs have status bits indicating poor behavior
(e.g, `init_done` low). The report intelligently determines which status
bits should be checked for the different MGT types (not all status bits are
implemented/relevant on all MGT types).

Check RX Status: Get the values of the `rx_status` register for all in-use
mgts, and report which MGTs have status bits indicating poor behavior (e.g,
`rxprbslocked` low). The report intelligently determines which status bits
should be checked for the different MGT types (not all status bits are
implemented/relevant on all MGT types).

Check RX Status: Get the values of the `rx_status` register for all in-use
mgts, and report which MGTs have status bits indicating poor behavior (e.g,
`reset_tx_done` low). The report intelligently determines which status bits
should be checked for the different MGT types (not all status bits are
implemented/relevant on all MGT types).

Report Errors: Get/report the errors of all in-use mgt receivers.

Inject Errors: Inject errors for all in-use mgt transmitters. This is done by
changing the tx prbs pattern to a different one that is specified in the user
configuration (there is a bitfield for injecting errors, but it is not
implemented on all MGT types).

Find Link Routes: Determines which mgt transmitters are routed to which mgt
receivers. Injects errors using each individual in-use tx, then checks for new
errors on each in-use rx, to determine routes.

Get GT Registers: Get/display values of all mgt register/bitfield values, for
all in-use mgts.

Collect Eye Diagrams: Collect eye diagrams for the receivers of all in-use
links. Results are stored in `<IBERTAREA>/EyeAnalysis/Results/`, with each batch
of eye diagrams having their own directory. The directory name is based on the
ibert test's log name, and each individual filename is based on the log name, as
well as the MGT receiver and name of the link it is a part of. NOTE: producing
an eye diagram requires a display environment, even if the diagram is meant to
be created but not viewed immediately. Consequently, this action should not be
performed without a display environment (e.g. over ssh without the -X option, or
in a screen session - to collect them remotely, use x2go or ssh -X).

Get Optical Power: Report the optical power received by each I2C-accessible
minipod on the present cards.

# Directory Descriptions

## Results

Where the results of `ibert_ipbus_test.py` are stored by default.

The contents of this directory may be modified by the user.

## UserConfig

Directory containing user configuration files for tests.

The contents of this directory may be modified by the user.

## EyeAnalysis

Directory containing the scripts for collecting eye diagrams, and storing the
results. Results are stored according to the following structure:
```
EyeAnalysis/Results
|
|-- Test_Name
  |-- Logs
  |-- RawData
  |-- ProcessedData
  |-- EyeDiagrams
```

The contents of this directory shall not be modified by the user, except for the
the directories containing results files, which may be modified.

## Tools

Directory containing various tools that a user may find useful, but are not
essential. For example, `gui.py` and `device_gui.py` are scripts for using a GUI
to manage IBERT test and device configuration.

The contents of this directory may be run/executed but shall not be modified by
the user.

## LinkConfiguration

Directory where the definitions of links, as well as a file specifying the
ordering of links (for the purpose of orderly user configuration files) are
stored in yaml format.

The contents of this directory shall not be modified by the user.

## PrivateModules

Directory containing various python modules which are used by other scripts, but
not designed/intended to be run on their own by a user.

The contents of this directory shall not be used or modified by the user.

## Xml

Directory containing xml files used to specify devices and the
structure/addresses of their registers.

The contents of this directory shall not be modified by the user.

## SummaryGeneration

Directory containing script for converting raw IBERT test results csv files
into easily-readable PDF summaries. Results are stored in the `Results`
subdirectory. The script `generate_summary_pdf.py` may be ran by the user, but
not modified. Generated results in `SummaryGeneration/Results` may be freely
modified by the user.

# Other Notes

MGTs are written to according to GT channel, _NOT_ according to MGT Quad and
Quad Channel. Any displayed results referencing MGT Quad and Quad Channel are
only as correct as the developer's conversion between GT channel to MGT Quad and
Quad Channel, which is defined in
`<IBERTAREA>/PrivateModules/gt_mgt_conversion.py`.

As of the time of writing, we have for the Hubs: GT channels 0-39 correspond to
MGT quads 224-233, and GT chanels 40-79 correspond to MGT quads 124-133. For the
RODs: GT channels 0-39 correspond to MGT quads 210-219, and GT channels 40-79
correspond to MGT quads 110-119. For the HTMs: GT channels 0-15 correspond to
MGT quads 109-112.

